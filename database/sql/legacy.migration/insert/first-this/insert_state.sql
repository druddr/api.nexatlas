/* Data for the 'state' table  (Records 1 - 28) */

INSERT INTO `state`
  (`id`, `code`, `name`, `ibge_code`, `ibge_situation_id`)
VALUES 
  (1, 'RO', 'Rondônia', 11, 1),
  (2, 'AC', 'Acre', 12, 1),
  (3, 'AM', 'Amazonas', 13, 1),
  (4, 'RR', 'Roraima', 14, 1),
  (5, 'PA', 'Pará', 15, 1),
  (6, 'AP', 'Amapá', 16, 1),
  (7, 'TO', 'Tocantins', 17, 1),
  (8, 'MA', 'Maranhão', 21, 1),
  (9, 'PI', 'Piauí', 22, 1),
  (10, 'CE', 'Ceará', 23, 1),
  (11, 'RN', 'Rio Grande do Norte', 24, 1),
  (12, 'PB', 'Paraíba', 25, 1),
  (13, 'PE', 'Pernambuco', 26, 1),
  (14, 'AL', 'Alagoas', 27, 1),
  (15, 'SE', 'Sergipe', 28, 1),
  (16, 'BA', 'Bahia', 29, 1),
  (17, 'MG', 'Minas Gerais', 31, 1),
  (18, 'ES', 'Espírito Santo', 32, 1),
  (19, 'RJ', 'Rio de Janeiro', 33, 1),
  (20, 'SP', 'São Paulo', 35, 1),
  (21, 'PR', 'Paraná', 41, 1),
  (22, 'SC', 'Santa Catarina', 42, 1),
  (23, 'RS', 'Rio Grande do Sul', 43, 1),
  (24, 'MS', 'Mato Grosso do Sul', 50, 1),
  (25, 'MT', 'Mato Grosso', 51, 1),
  (26, 'GO', 'Goiás', 52, 1),
  (27, 'DF', 'Distrito Federal', 53, 1),
  (32, 'IG', 'Ignorado/exterior', 0, 1);