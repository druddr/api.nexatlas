SELECT
	CASE idtipoauxilio
		WHEN 7 THEN 7
		WHEN 8 THEN 8
		ELSE 6 -- those not VOR or VOR/DME
    END AS waypoint_type_id
	, codigo AS code
    , nome AS name
    , CAST(st_x(localizacao::geometry) as double precision) AS longitude
    , CAST(st_y(localizacao::geometry) as double precision) AS latitude
    , CURRENT_TIMESTAMP AS creation_date
FROM auxilio
WHERE ativo = true
ORDER BY
	code ASC

/*
Tipos auxílio
3 NDB
4 NDB (LOM)
5 --
6 NDB (LMM)
7 VOR
8 VOR/DME

*/

/* CREATE TABLE public.auxilio (
  id SERIAL,
  codigo VARCHAR(3) NOT NULL,
  idtipoauxilio INTEGER NOT NULL,
  nome VARCHAR(50) NOT NULL,
  localizacao public.geography NOT NULL,
  ativo BOOLEAN DEFAULT true NOT NULL,
  CONSTRAINT auxilio_codigo_tipo_nome_uidx UNIQUE(codigo, idtipoauxilio, nome),
  CONSTRAINT auxilio_pkey PRIMARY KEY(id),
  CONSTRAINT auxilio_tipoauxilio_fk FOREIGN KEY (idtipoauxilio)
    REFERENCES public.tipoauxilio(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    NOT DEFERRABLE
) */