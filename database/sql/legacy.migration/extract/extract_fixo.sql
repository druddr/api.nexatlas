SELECT
	4 AS waypoint_type_id
	, codigo AS code
    , codigo AS name
    , CAST(st_x(localizacao::geometry) as double precision) AS longitude
    , CAST(st_y(localizacao::geometry) as double precision) AS latitude
    , CURRENT_TIMESTAMP AS creation_date
FROM fixo
WHERE ativo = true
ORDER BY
	code ASC

/*
CREATE TABLE public.fixo (
  id SERIAL,
  codigo VARCHAR(20) NOT NULL,
  localizacao public.geography NOT NULL,
  ativo BOOLEAN DEFAULT true NOT NULL,
  CONSTRAINT fixo_pkey PRIMARY KEY(id)
) */