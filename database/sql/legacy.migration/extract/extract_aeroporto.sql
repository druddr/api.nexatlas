SELECT
	CASE 
		WHEN nome LIKE '%HELPN%' THEN 5
		ELSE 1
	END AS waypoint_type_id
	, codigo AS icao_code
    , nome AS name
    -- , st_astext(localizacao)
	, CAST(st_x(localizacao::geometry) as double precision) AS longitude
    , CAST(st_y(localizacao::geometry) as double precision) AS latitude
    , iduf AS state_id
    , CURRENT_TIMESTAMP AS creation_date
FROM aeroporto
WHERE ativo = true
	AND id > 3400
ORDER BY
	icao_code ASC