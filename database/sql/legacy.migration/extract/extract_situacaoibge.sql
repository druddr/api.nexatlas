SELECT
	id
    , sigla AS code
    , nome AS name
    , codigoibge AS ibge_code
    , idsituacaoibge AS ibge_situation_code
    FROM uf
    ORDER BY id ASC