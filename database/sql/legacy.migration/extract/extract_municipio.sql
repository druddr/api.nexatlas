SELECT
	2 AS waypoint_type_id
	, municipio.nome AS city_name
    , CONCAT(municipio.nome, ', ', uf.sigla) AS name
    , CAST(st_x(municipio.localizacao::geometry) AS double precision) AS longitude
    , CAST(st_y(municipio.localizacao::geometry) AS double precision) AS latitude
    , CAST(municipio.altitude AS float) AS altitude
    , municipio.codigoibge AS ibge_code
    , municipio.idsituacaoibge AS ibge_situation_id
    , municipio.iduf AS state_id
    , CURRENT_TIMESTAMP AS creation_date
FROM municipio
INNER JOIN uf ON municipio.iduf = uf.id
ORDER BY
	municipio.nome ASC

/*
CREATE TABLE public.municipio (
  id SERIAL,
  nome VARCHAR(50) NOT NULL,
  codigoibge INTEGER NOT NULL,
  iduf INTEGER NOT NULL,
  capital BOOLEAN NOT NULL,
  localizacao public.geography NOT NULL,
  altitude INTEGER,
  idsituacaoibge INTEGER NOT NULL,
  CONSTRAINT municipio_codigoibge_key UNIQUE(codigoibge),
  CONSTRAINT municipio_pkey PRIMARY KEY(id),
  CONSTRAINT municipio_iduf_fkey FOREIGN KEY (iduf)
    REFERENCES public.uf(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    NOT DEFERRABLE,
  CONSTRAINT municipio_situacaoibge_fk FOREIGN KEY (idsituacaoibge)
    REFERENCES public.situacaoibge(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    NOT DEFERRABLE
) */