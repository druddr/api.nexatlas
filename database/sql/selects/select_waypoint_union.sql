SELECT
	waypoint_type_id
	, id
	, code
	, name
	, NULL
	, latitude
	, longitude
	, CHAR_LENGTH(code)+1
	/*
	CASE
		WHEN waypoint_type_id IN (1, 5) THEN -- AERODROME OR HELIPORT
			CASE
				WHEN code LIKE 'SB%' THEN CHAR_LENGTH(name) -- INTL AERODROME
				ELSE 
					CASE 
						WHEN waypoint_type_id = 5 THEN CHAR_LENGTH(name) + 4 -- HELIPORT
						ELSE CHAR_LENGTH(name) + 3 -- AERODROME
					END
			END
		WHEN waypoint_type_id = 4 THEN -- FIX
			CHAR_LENGTH(code) + 1
		WHEN waypoint_type_id = 2 THEN -- CITY
			CHAR_LENGTH(name) +  2
		ELSE CHAR_LENGTH(code) + 1 -- OTHER WAYPOINT TYPES
	END AS importance_score
	*/
FROM waypoint
WHERE
	waypoint_type_id = 4
	AND (name LIKE '@text%')
UNION
SELECT
	waypoint_type_id
	, id
	, code
	, name
	, icao_code
	, latitude
	, longitude
	, CHAR_LENGTH(code)
WHERE
	waypoint_type_id IN (1, 5)
	AND (code LIKE '@text%')
	OR (icao_code LIKE '@text%')
UNION
SELECT
	waypoint_type_id
	, id
	, code
	, name
	, icao_code
	, latitude
	, longitude
	, CHAR_LENGTH(code)
WHERE
	waypoint_type_id IN (1, 5)
	AND (nome LIKE 'SB%')
	AND (code LIKE 'SB%')
WHERE
	waypoint_type_id IN (6, 7 ,8)
	waypoint_type_id IN (2)
	AND name LIKE 'SBP%'