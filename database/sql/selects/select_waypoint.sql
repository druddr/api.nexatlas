SELECT
	waypoint_type_id
	, id
	, code
	, name
	, city_name 
	, icao_code
	, latitude
	, longitude
	, 
	CASE
		-- AERODROME - waypoint pre-filter
		WHEN (waypoint_type_id = 1) THEN 
			CASE
				-- AERODROME - icao code partial matches query
				WHEN (icao_code LIKE '{$query}%')	 THEN CHAR_LENGTH(icao_code)
				-- AERODROME - icao_code IS NOT LIKE query
				WHEN (icao_code NOT LIKE '{$query}%') THEN
					CASE
						-- AERODROME - name IS LIKE query
						WHEN (name LIKE '{$query}%') THEN
							CASE 
								-- NON-INTL AERODROME - name partial match less priority
								WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(name)+3
								-- INTL AERODROME - name partial match
								ELSE CHAR_LENGTH(name) 
							END
						-- AERODROME - city name IS LIKE query
						WHEN (city_name LIKE '{$query}%') THEN
							CASE 
								-- NON-INTL AERODROME - city name partial match with less priority
								WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(city_name)+3
								-- INTL AERODROME - city name partial match
								ELSE CHAR_LENGTH(city_name) 
							END
						ELSE CHAR_LENGTH(icao_code)+10 -- AERODROME - no desired match at all
					END
				ELSE CHAR_LENGTH(icao_code)+10 -- AERODROME - no desired match at all
			END
		WHEN (waypoint_type_id = 5) THEN -- HELIPORT - pre-filter
			CASE
				-- AERODROME - icao code partial matches query
				WHEN (icao_code LIKE '{$query}%')	 THEN CHAR_LENGTH(icao_code)
				-- AERODROME - icao_code IS NOT LIKE query
				WHEN (icao_code NOT LIKE '{$query}%') THEN
					CASE
						-- AERODROME - name IS LIKE query
						WHEN (name LIKE '{$query}%') THEN
							CASE 
								-- NON-INTL AERODROME - name partial match less priority
								WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(name)+4
								-- INTL AERODROME - name partial match
								ELSE CHAR_LENGTH(name) 
							END
						-- AERODROME - city name IS LIKE query
						WHEN (city_name LIKE '{$query}%') THEN
							CASE 
								-- NON-INTL AERODROME - city name partial match with less priority
								WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(city_name)+4
								-- INTL AERODROME - city name partial match
								ELSE CHAR_LENGTH(city_name) 
							END						
						ELSE CHAR_LENGTH(icao_code)+10 -- HELIPORT - no desired match at all
					END
				ELSE CHAR_LENGTH(icao_code)+10 -- HELIPORT - no desired match at all
			END
		WHEN (waypoint_type_id = 4) THEN -- FIX
			CHAR_LENGTH(code)+1
		WHEN (waypoint_type_id = 2 AND ibge_situation_id = 1) THEN -- CITY
			CHAR_LENGTH(city_name)+2
		ELSE 
			-- NAVAID/OTHER WAYPOINT TYPES - can be type 6, 7, 8 or 3 (COORDS)
			CASE
				-- NAVAID/OTHER WAYPOINT TYPES - code partially matches
				WHEN (code LIKE '{$query}%') THEN CHAR_LENGTH(code)+1
				-- NAVAID/OTHER WAYPOINT TYPES - name partially matches
				WHEN (code NOT LIKE '{$query}%' AND name LIKE '{$query}%') THEN CHAR_LENGTH(name)+1
				
				ELSE CHAR_LENGTH(icao_code)+10 -- no desired match at all
			END
	END AS importance_score
FROM waypoint
WHERE
	name LIKE '{$query}%'
    OR city_name LIKE '{$query}%'
    OR icao_code LIKE '{$query}%'
    OR code LIKE '{$query}%'
ORDER BY importance_score, code
LIMIT 11