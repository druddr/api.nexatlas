CREATE VIEW `view_count_authentications_weekly` AS (
	SELECT
		COUNT(`id`) AS user_authentications_count,
		WEEK(`creation_date`) AS creation_week,
		DATE_FORMAT(`creation_date`, '%m') AS creation_month,
		DATE_FORMAT(`creation_date`, '%Y') AS creation_year
	FROM `authentication`
	GROUP BY
		creation_week,
		creation_month,
		creation_year
	ORDER BY
		creation_week,
		creation_month,
		creation_year
		ASC
);
