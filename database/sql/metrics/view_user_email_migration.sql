CREATE VIEW `view_user_email_migration` AS (
	SELECT
		count(user_email_migration_list.email) AS new_email_count,
		count(user_email_migration_list.legacy_email) AS migrated_email_count,
		user_email_migration_list.created_on,
		user_email_migration_list.original_creation_date,
		user_email_migration_list.total_legacy_accounts
	FROM
	(
		SELECT
			user_emails.email,
			user_emails.created_on,
			user_emails.original_creation_date,
			legacy_user_emails.email AS legacy_email,
			legacy_accounts.total_legacy_accounts
		FROM
		(
			SELECT
				email,
				DATE_FORMAT(`user`.`creation_date`, '%d/%m/%Y') AS created_on,
				DATE_FORMAT(`user`.`creation_date`, '%Y-%m-%d 00:00:00') AS original_creation_date
			FROM `user`
		) AS user_emails
		JOIN (SELECT COUNT(`id`) AS total_legacy_accounts FROM `legacy_user_email`) AS legacy_accounts
		LEFT JOIN
		(
			SELECT
				`id`,
				`email`
			FROM `legacy_user_email`
		) AS legacy_user_emails
			ON legacy_user_emails.email = user_emails.email
	) AS user_email_migration_list
	GROUP BY
		user_email_migration_list.created_on,
		user_email_migration_list.original_creation_date,
		user_email_migration_list.total_legacy_accounts
	ORDER BY user_email_migration_list.original_creation_date ASC
);
