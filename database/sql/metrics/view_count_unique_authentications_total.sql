CREATE VIEW `view_count_unique_authentications_total` AS (
	SELECT
		COUNT(`user_id`) AS unique_authentications_count
		FROM
		(
			SELECT
				COUNT(`id`) AS user_authentication_count,
				`user_id`
				FROM `authentication`
				GROUP BY `user_id`
				HAVING user_authentication_count = 1
		) AS unique_authentication_user_ids
);
