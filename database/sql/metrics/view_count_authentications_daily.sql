CREATE VIEW `view_count_authentications_daily` AS (
	SELECT
		COUNT(`id`) AS user_authentications_count,
		DATE_FORMAT(`creation_date`, '%d/%m/%Y') AS created_on,
		DATE_FORMAT(`creation_date`, '%Y-%m-%d 00:00:00') AS original_creation_date
	FROM `authentication`
	GROUP BY
		created_on,
		original_creation_date
	ORDER BY
		original_creation_date ASC
);
