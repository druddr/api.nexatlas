CREATE VIEW `view_count_new_users_monthly` AS (
	SELECT
		COUNT(`user_id`) AS users_count,
		creation_month,
		creation_year
	FROM
	(
		SELECT
			`id` AS user_id,
			DATE_FORMAT(`creation_date`, '%m') AS creation_month,
			DATE_FORMAT(`creation_date`, '%Y') AS creation_year
		FROM `user`
	) AS users_count
	GROUP BY users_count.creation_month
		, users_count.creation_year
);
