CREATE VIEW `view_count_users_which_authenticated_daily` AS (
	SELECT
		count(user_accesses.user_id) AS users_count,
		user_accesses.created_on,
		user_accesses.original_creation_date
	FROM
	(
		SELECT
			`authentication`.`user_id`
			, DATE_FORMAT(`authentication`.`creation_date`, '%d/%m/%Y') AS created_on
			, DATE_FORMAT(`authentication`.`creation_date`, '%Y-%m-%d 00:00:00') AS original_creation_date
		FROM `authentication`
		GROUP BY
			user_id
			, created_on
			, original_creation_date
		ORDER BY
			original_creation_date ASC
	) AS user_accesses
	GROUP BY
		user_accesses.created_on
		, user_accesses.original_creation_date
	ORDER BY
		user_accesses.original_creation_date ASC
);
