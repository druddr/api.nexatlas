CREATE VIEW `view_count_new_users_daily` AS (
	SELECT
		COUNT(`user_id`) AS users_count,
		creation_date,
		original_creation_date
	FROM
	(
		SELECT
			`id` AS user_id,
			DATE_FORMAT(`creation_date`, '%d/%m/%Y') AS creation_date,
			DATE_FORMAT(`creation_date`, '%Y-%m-%d 00:00:00') AS original_creation_date
		FROM `user`
	) AS users_count
	GROUP BY users_count.creation_date, users_count.original_creation_date
);
