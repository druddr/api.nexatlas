CREATE VIEW `view_count_total_users_daily` AS (
	SELECT
		COUNT(`user`.`id`) AS user_count,
		DATE_FORMAT(`user`.`creation_date`, '%d/%m/%Y') AS created_on,
		DATE_FORMAT(`user`.`creation_date`, '%Y-%m-%d 00:00:00') AS db_created_on
	FROM `user`
	GROUP BY
		created_on
		, db_created_on
	ORDER BY db_created_on ASC
);
