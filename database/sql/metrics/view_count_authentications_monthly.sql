CREATE VIEW `view_count_authentications_monthly` AS (
	SELECT
		COUNT(`id`) AS user_authentications_count,
		DATE_FORMAT(`creation_date`, '%m') AS creation_month,
		DATE_FORMAT(`creation_date`, '%Y') AS creation_year
	FROM `authentication`
	GROUP BY
		creation_month,
		creation_year
	ORDER BY
		creation_month,
		creation_year
		ASC
);
