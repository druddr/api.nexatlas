CREATE VIEW `view_count_users_which_authenticated_weekly` AS (
	SELECT
		count(user_accesses.user_id) AS users_count
		, user_accesses.creation_week
		, user_accesses.creation_month
		, user_accesses.creation_year
	FROM
	(
		SELECT
			`authentication`.`user_id`
			, WEEK(`authentication`.`creation_date`) AS creation_week
			, DATE_FORMAT(`authentication`.`creation_date`, '%m') AS creation_month
			, DATE_FORMAT(`authentication`.`creation_date`, '%Y') AS creation_year
		FROM `authentication`
		GROUP BY
			user_id
			, creation_week
			, creation_month
			, creation_year
		ORDER BY
			creation_week
			, creation_year ASC
	) AS user_accesses
	GROUP BY
		user_accesses.creation_week
		, user_accesses.creation_month
		, user_accesses.creation_year
	ORDER BY
		user_accesses.creation_week
		, user_accesses.creation_year ASC
);
