CREATE VIEW `view_count_total_users_per_authentications_grouping` AS (
	SELECT
		`user_authentications`.`user_authentication_count` AS user_authentication_frequency,
		COUNT(`user_authentications`.`user_id`) AS users_count
		FROM
		(
		SELECT
			COUNT(`authentication`.`id`) AS user_authentication_count,
			`authentication`.`user_id`
			FROM `authentication`
			GROUP BY `authentication`.`user_id`
		) AS user_authentications
		GROUP BY user_authentication_frequency
		ORDER BY user_authentication_frequency DESC
);
