CREATE VIEW `view_count_total_usage_per_aerodrome` AS (
	SELECT
		COUNT(`route`.`id`) AS aerodrome_usage_count,
		`route_waypoint`.`name` AS aerodrome_name,
		`route_waypoint`.`icao_code` AS aerodrome_icao,
		`route_waypoint`.`iata_code` AS aerodrome_iata,
		`route_waypoint`.`is_origin` AS aerodrome_is_origin,
		`route_waypoint`.`is_destination` AS aerodrome_is_destination
		FROM `route`
		INNER JOIN `route_waypoint`
			ON `route_waypoint`.`route_id` = `route`.`id`
		INNER JOIN `waypoint_type`
			ON `route_waypoint`.`waypoint_type_id` = `waypoint_type`.`id`
		WHERE `waypoint_type`.`code_name` = 'AD'
			AND ( `route_waypoint`.`is_origin` = 1 OR `route_waypoint`.`is_destination` = 1)
		GROUP BY `route_waypoint`.`name`
			, `route_waypoint`.`icao_code`
			, `route_waypoint`.`iata_code`
			, `route_waypoint`.`is_origin`
			, `route_waypoint`.`is_destination`
		ORDER BY aerodrome_usage_count DESC
		LIMIT 30
);
