CREATE VIEW `view_count_total_users_weekly` AS (
	SELECT
		COUNT(`user`.`id`) AS user_count,
		WEEK(`user`.`creation_date`) AS creation_week,
		DATE_FORMAT(`user`.`creation_date`, '%m') AS creation_month,
		DATE_FORMAT(`user`.`creation_date`, '%Y') AS creation_year
	FROM `user`
	GROUP BY
		creation_week
		, creation_month
		, creation_year
	ORDER BY creation_week, creation_month, creation_year ASC
);
