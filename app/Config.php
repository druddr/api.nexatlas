<?php

class config {
	public static $AngularJsJSON = true;
	public static $BasePath = "";
	public static $DefaultMeta = [];
	public static $DefaultJS = [];
	public static $DefaultCSS = [];
	public static $DefaultJSAssets = [];
	public static $DefaultCSSAssets = [];
	public static $DevMode = true; //should be false for deploy
	public static $UTF8EncodeResult = false;
	public static $requiredCCID = true;
	public static $UTF8DecodeResult = false;
	public static $UTF8EncodeSQL = false;
	public static $RESTMethodNames = array(
		'POST'=>'create',
		'GET'=>'get',
		'QUERY'=>'query',
		'DELETE'=>'delete',
		'PUT'=>'update'
	);
	public static $Database = array(
		'host'=>'',
		'db'=>'',
		'user'=>'',
		'pass'=>''
	);
	public static $SystemBasePath = "";
	public static $BaseDomain = "/";
	const SessionTimeout = 3600;
	/* Other configuration */
	public static $AppSendsEmail = true;
	public static $PasswordRequestValidityHours = 12;
	public static $AppUrl = "https://alfa.nexatlas.com/app/";
	public static $EmailConfirmationAppUrl = "https://alfa.nexatlas.com/app/#/app/user/confirmEmail/";
	public static $PasswordRequestAppUrl = "https://alfa.nexatlas.com/app/#/app/password-request/check/";
	public static $PasswordResetAppUrl = "/app/password-request/resetPassword/";
	public static $EmailLogoImageUri = "";
	public static $EmailLogoLinkUrl = "https://nexatlas.com/";
	public static $EmailInstagramImageUri = "";
	public static $EmailInstagramLinkUrl = "https://instagr.am/nexatlas/";
	public static $EmailFacebookImageUri = "";
	public static $EmailFacebookLinkUrl = "https://www.facebook.com/nexatlas";
	public static $EmailLinkedinImageUri = "";
	public static $EmailLinkedinLinkUrl = "https://linkedin.com/company/nexatlas";
	const AuthenticationSalt = null; //null will perform random and seek user's own salt
	const AuthorizationController = 'User';
	const AuthorizationMethod = 'authorize';
	const AuthorizationMark = 'authorized';
	const AuthorizationTokenLife = 30; //given in seconds
	const DefaultController = "Default";
	const DefaultAction = "index";
	const DefaultErrorController = "ErrorController";
	const DefaultErrorAction = "index";
	const DefaultDesign = "defaultDesign";
	const DefaultLayout = "default";
	const GoogleAPIKey = "907304989390-l34vlgsi6geummlj9v23ptte7ia1ba07.apps.googleusercontent.com";
	const SendGridAPIKey = "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";
	/* SMTP configuration (same as WordPress config) */
	const SmtpHeaderFromUser = "NexAtlas";
	const SmtpHeaderFromMail = "contato@nexatlas.com";
	const SmtpHeaderReplyToUser = "NexAtlas";
	const SmtpHeaderReplyToMail = "contato@nexatlas.com";
	const SmtpHeaderSubjectTag = ""; //"[NexAtlas App]"
	const SmtpHost = "email-smtp.us-east-1.amazonaws.com";
	const SmtpPort = "587";
	const SmtpUser = "AKIAIRNY6G6WGMBNQKCA";
	const SmtpPassword = "AnBeuTGmsvisoDBP+DQDbkkEAvJ0CjbYaOvd+NEcPEN8";
}
config::$SystemBasePath = realpath(dirname(dirname(__FILE__)))."/";
config::$BasePath = realpath(dirname(dirname(__FILE__)))."/";
//Environment selection
switch($_SERVER['HTTP_HOST']){
	case 'api.nexatlas.com':
		//Production environment
		config::$DevMode = false;
		config::$AppSendsEmail = true;
		config::$AppUrl = "https://nexatlas.com/app/";
		config::$EmailConfirmationAppUrl = "https://nexatlas.com/app/#/app/user/confirmEmail/";
		config::$PasswordRequestAppUrl = "https://nexatlas.com/app/#/app/password-request/check/";
		config::$PasswordResetAppUrl = "/app/password-request/resetPassword/";
		////TODO: Correct functioning
		//config::$EmailLogoImageUri = "http://alfa.nexatlas.com/images/mail/png/logo_email.png";
		//config::$EmailInstagramImageUri = "http://alfa.nexatlas.com/images/mail/png/instagram.png";
		//config::$EmailFacebookImageUri = "http://alfa.nexatlas.com/images/mail/png/facebook-01.png";
		//config::$EmailLinkedinImageUri = "http://alfa.nexatlas.com/images/mail/png/linkedin-01.png";
		config::$EmailLogoImageUri = "https://image.ibb.co/kM7VJF/logo_email.png";
		config::$EmailLogoLinkUrl = "https://nexatlas.com/";
		config::$EmailInstagramImageUri = "https://image.ibb.co/m6tgQv/instagram.png";
		config::$EmailFacebookImageUri = "https://image.ibb.co/j9NxyF/facebook_01.png";
		config::$EmailLinkedinImageUri = "https://image.ibb.co/f40jdF/linkedin_01.png";
		config::$UTF8EncodeResult=false;
		config::$UTF8DecodeResult=false;
		config::$UTF8EncodeSQL=false;
		config::$BaseDomain = "/";
		config::$Database = array(
			'host'=>'localhost',
			'db'=>'app_production',
			'user'=>'app_admin',
			'pass'=>'254289pp'
		);
		break;
	case 'alfa.nexatlas.com':
		//Stage environment
		config::$DevMode = false;
		config::$AppSendsEmail = true;
		config::$AppUrl = "https://alfa.nexatlas.com/app/";
		config::$EmailConfirmationAppUrl = "http://alfa.nexatlas.com/app/#/app/user/confirmEmail/";
		config::$PasswordRequestAppUrl = "http://alfa.nexatlas.com/app/#/app/password-request/check/";
		config::$PasswordResetAppUrl = "/app/password-request/resetPassword/";
		config::$EmailLogoImageUri = "https://image.ibb.co/kM7VJF/logo_email.png";
		config::$EmailLogoLinkUrl = "https://alfa.nexatlas.com/";
		config::$EmailInstagramImageUri = "https://image.ibb.co/m6tgQv/instagram.png";
		config::$EmailFacebookImageUri = "https://image.ibb.co/j9NxyF/facebook_01.png";
		config::$EmailLinkedinImageUri = "https://image.ibb.co/f40jdF/linkedin_01.png";
		config::$UTF8EncodeResult=false;
		config::$UTF8DecodeResult=false;
		config::$UTF8EncodeSQL=false;
		config::$BaseDomain = "/api/";
		config::$Database = array(
			'host'=>'localhost',
			'db'=>'app_stage',
			'user'=>'app_admin',
			'pass'=>'254289pp'
		);
		break;
	default:
		//Local environment
		config::$DevMode = true;
		//Won't send emails, anyway
		config::$AppSendsEmail = true;
		config::$AppUrl = "http://app.nexatlas/";
		config::$EmailConfirmationAppUrl = "http://app.nexatlas/#/app/user/confirmEmail/";
		config::$PasswordRequestAppUrl = "http://app.nexatlas/#/app/password-request/check/";
		config::$PasswordResetAppUrl = "/app/password-request/resetPassword/";
		config::$EmailLogoImageUri = "https://image.ibb.co/kM7VJF/logo_email.png";
		config::$EmailLogoLinkUrl = "http://app.nexatlas/";
		config::$EmailInstagramImageUri = "https://image.ibb.co/m6tgQv/instagram.png";
		config::$EmailFacebookImageUri = "https://image.ibb.co/j9NxyF/facebook_01.png";
		config::$EmailLinkedinImageUri = "https://image.ibb.co/f40jdF/linkedin_01.png";
		config::$UTF8EncodeResult=false;
		config::$UTF8DecodeResult=false;
		config::$UTF8EncodeSQL=false;
		config::$Database = array(
			'host'=>'localhost',
			'db'=>'nexatlas_local',
			'user'=>'nexatlas',
			'pass'=>'nexatlas'
		);
		break;
}
