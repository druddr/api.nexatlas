<?php

/**
 * ModelUserProfile provides user info.
 *
 * ModelUserProfile stands for a simple class which provides main user info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelUserProfile extends Model
{
	#region Properties

    public $primary_key = "id";
    public $table_name = "user_profile";

	public $field_config = array(
		'id' => array( 'type' => Model::type_int),
		'user_id' => array('type' => Model::type_int),
		'name' => array('type' => Model::type_varchar),
		'surname' => array('type' => Model::type_varchar),
		'complete' => array('type' => Model::type_boolean),
		'complete_on' => array('type' => Model::type_time),
		'pilot' => array('type' => Model::type_boolean),
		'aircraft_type' =>array('type' => Model::type_varchar),
		'aviation_segment' =>array('type' => Model::type_varchar),
		'self_classification' =>array('type' => Model::type_varchar),
		'picture' =>array('type' => Model::type_varchar),
		'creation_date' =>array('type' => Model::type_time),
	);
	public $id;
	public $user_id;
	public $name;
	public $surname;
	public $complete;
	public $complete_on;
	public $pilot;
	public $aircraft_type;
	public $aviation_segment;
	public $self_classification;
	public $picture;
	public $creation_date;
	public $hasOne =
	[
		'User' => [
			'model' => 'ModelUser',
			'where' => [
				['id','=','user_id']
			],
			'limit' => [1]
			]
	];

	#endregion

	#region Methods

	/**
	 * Gets a ["user_profile"] by its ID
	 */
	public function getById(int $profile_id) {
		return $this->records()->where('id',$profile_id)->getFirstModel();
	}

	/**
	 * Gets a ["user_profile"] by its user ID
	 */
	public function getByUserId(int $user_id) {
		return $this->records()->where('user_id',$user_id)->getFirstModel();
	}

	#endregion
}