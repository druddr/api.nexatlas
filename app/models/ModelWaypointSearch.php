<?php
/**
 * Counts total authentications, date/time based at the database
 *
 * Counts the total authentications daily on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountAuthenticationsDaily();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily accesses count
 *	 Starting from day 1.
 */
class ModelWaypointSearch extends Model
{
    public $primary_key = "creation_date";
	public $primary_key_is_string = true;
    public $table_name = "waypoint";
	public $field_config = [
		'waypoint_type_id' => ['type' => Model::type_int],
		'id' => ['type' => Model::type_int],
		'code' => ['type' => Model::type_varchar],
		'name' => ['type' => Model::type_varchar],
		'city_name' => ['type' => Model::type_varchar],
		'icao_code' => ['type' => Model::type_varchar],
		'iata_code' => ['type' => Model::type_varchar],
		'latitude' => ['type' => Model::type_double],
		'longitude' => ['type' => Model::type_double],
		'importance_score' => ['type' => Model::type_datetime],
	];

	public $waypoint_type_id;
	public $id;
	public $code;
	public $name;
	public $city_name;
	public $icao_code;
	public $iata_code;
	public $latitude;
	public $longitude;
	public $importance_score;
	public $hasOne = [
		'Type' => [
			'model' => 'ModelWaypointType',
			'where' => [
				['id','=','waypoint_type_id']
			],
			'limit' => [1]
		]
	];

	/**
	 * Gets waypoints ordered by its importance
	 * @return ActiveRecord
	 */
	public function get($query) {
		$records = $this->records();

		if (!isset($query))
			throw new Exception('queryParameterIsCompulsory');

		$records = $records->select(
				"waypoint_type_id" .
				", id			 " .
				", code			 " .
				", name			 " .
				", city_name	 " .
				", icao_code	 " .
				", iata_code	 " .
				", latitude		 " .
				", longitude	 " .
				",				 " .
				"CASE			 " .
				"	WHEN (waypoint_type_id = 1) THEN" .
				"		CASE" .
				"			WHEN (icao_code LIKE '{$query}%')	 THEN CHAR_LENGTH(icao_code)" .
				"			WHEN (icao_code NOT LIKE '{$query}%') THEN" .
				"				CASE" .
				"					WHEN (name LIKE '{$query}%') THEN" .
				"						CASE" .
				"							WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(name)+3" .
				"							ELSE CHAR_LENGTH(name)" .
				"						END" .
				"					WHEN (city_name LIKE '{$query}%') THEN" .
				"						CASE" .
				"							WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(city_name)+3" .
				"							ELSE CHAR_LENGTH(city_name)" .
				"						END" .
				"					ELSE CHAR_LENGTH(icao_code)+10" .
				"				END" .
				"			ELSE CHAR_LENGTH(icao_code)+10" .
				"		END" .
				"	WHEN (waypoint_type_id = 5) THEN" .
				"		CASE" .
				"			WHEN (icao_code LIKE '{$query}%')	 THEN CHAR_LENGTH(icao_code)" .
				"			WHEN (icao_code NOT LIKE '{$query}%') THEN" .
				"				CASE" .
				"					WHEN (name LIKE '{$query}%') THEN" .
				"						CASE" .
				"							WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(name)+4" .
				"							ELSE CHAR_LENGTH(name)" .
				"						END" .
				"					WHEN (city_name LIKE '{$query}%') THEN" .
				"						CASE" .
				"							WHEN (icao_code NOT LIKE 'SB%')	THEN CHAR_LENGTH(city_name)+4" .
				"							ELSE CHAR_LENGTH(city_name)" .
				"						END" .
				"					ELSE CHAR_LENGTH(icao_code)+10" .
				"				END" .
				"			ELSE CHAR_LENGTH(icao_code)+10" .
				"		END" .
				"	WHEN (waypoint_type_id = 4) THEN" .
				"		CHAR_LENGTH(code)+1" .
				"	WHEN (waypoint_type_id = 2 AND ibge_situation_id = 1) THEN" .
				"		CHAR_LENGTH(city_name)+2" .
				"	ELSE" .
				"		CASE" .
				"			WHEN (code LIKE '{$query}%') THEN CHAR_LENGTH(code)+1" .
				"			WHEN (code NOT LIKE '{$query}%' AND name LIKE '{$query}%') THEN CHAR_LENGTH(name)+1" .
				"			ELSE CHAR_LENGTH(icao_code)+10" .
				"		END" .
				" END AS importance_score");

		$records = $records
			->like('name', "{$query}%")
			->orThen()
			->like('city_name', "{$query}%")
			->orThen()
			->like('icao_code', "{$query}%")
			->orThen()
			->like('code', "{$query}%")
			->order(['importance_score, code'], 'ASC')
			->limit(11);

		//WHERE
		//	name LIKE 'por%'
		//	OR city_name LIKE 'por%'
		//	OR icao_code LIKE 'por%'
		//	OR code LIKE 'por%'
		//ORDER BY importance_score, code
		//LIMIT 11

		return $records;
	}
}
