<?php

/**
 * ModelUser provides user info.
 *
 * ModelUser stands for a simple class which provides main user info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelUser extends Model
{
	#region Properties

    public $primary_key = "id";
    public $table_name = "user";
	/**
	 * The subject to be attached to the email subject when
	 * an user creates an account
	 */
	const UserCreatedMailSubjectConfirmEmail = "Bem-vindo à NexAtlas: Confirme seu e-mail";
	const UserCreatedMailSubjectNoConfirmation = "Bem-vindo à NexAtlas";
	const UserConfirmedMailSubject = "Seu e-mail foi confirmado";
	const PasswordRequestedMailSubject = "Recupere sua senha";
	const PasswordUpdatedMailSubject = "Sua senha NexAtlas foi alterada";

	public $field_config = [
		'id' => ['type' => Model::type_int],
		'email' => ['type' => Model::type_varchar],
		'password' => ['type' => Model::type_varchar],
		'salt' => ['type' => Model::type_char],
		'read_terms' => ['type' => Model::type_boolean],
		'active' => ['type' => Model::type_boolean],
		'active_on' => ['type' => Model::type_time],
		'email_confirmation_token' => ['type' => Model::type_char],
		'confirmed_email' => ['type' => Model::type_boolean],
		'confirmed_email_on' => ['type' => Model::type_time],
		'facebook_connected' => ['type' => Model::type_boolean],
		'google_connected' => ['type' => Model::type_boolean],
		'creation_date' => ['type' => Model::type_time]
	];
	public $id;
	public $email;
	public $password;
	public $salt;
	public $read_terms;
	public $active;
	public $active_on;
	public $email_confirmation_token;
	public $confirmed_email;
	public $confirmed_email_on;
	public $facebook_connected;
	public $google_connected;
	public $creation_date;
	public $hasOne = [
		'Profile' => [
			'model' => 'ModelUserProfile',
			'where' => [
				['user_id','=','id']
			],
			'order' => ['creation_date'=>'DESC'],
			'limit' => [1]
		]
	];
	public $hasMany = [
		'Authentications' => [
			'model' => 'ModelAuthentication',
			'where' => [
				['user_id','=','id'],
				['dead', 'IS', 'NULL', false],
				['dead_on', 'IS', 'NULL', false]
			],
			'order' => ['creation_date'=>'DESC'],
			'limit' => []
		],
		'DeadAuthentications' => [
			'model' => 'ModelAuthentication',
			'where' => [
				['user_id','=','id'],
				['dead', '=', 1],
				['dead_on', 'IS NOT', 'NULL', false]
			],
			'order' => ['creation_date'=>'DESC'],
			'limit' => []
		],
		'Profiles' => [
			'model' => 'ModelUserProfile',
			'where' => [
				['user_id','=','id']
			],
			'order' => ['creation_date'=>'DESC'],
			'limit' => []
		],
		'PasswordRequests' => [
			'model' => 'ModelUserPasswordRequest',
			'where' => [
				['user_id','=','id'],
				['is_valid', '=', 1]
			],
			'order' => ['creation_date'=>'DESC'],
			'limit' => []
		],
		'InvalidPasswordRequests' => [
			'model' => 'ModelUserPasswordRequest',
			'where' => [
				['user_id', '=', 'id'],
				['is_valid', '=', 0]
			],
			'order' => ['creation_date'=>'DESC'],
			'limit' => []
		],
		'Routes' => [
			'model' => 'ModelRoute',
			'where' => [
				['user_id', '=', 'id'],
				['deleted', '=', 0]
			],
			'order' => ['creation_date'=>'ASC'],
			'limit' => []
		],
		'DeletedRoutes' => [
			'model' => 'ModelRoute',
			'where' => [
				['user_id', '=', 'id'],
				['deleted', '=', 1]
			],
			'order' => ['creation_date'=>'ASC'],
			'limit' => []
		]
	];

	#endregion

	#region Methods

	/**
	 * Gets a ["user"] by its ID
	 */
	public function getById(int $user_id) {
		return $this->records()->where('id',$user_id)->getFirstModel();
	}

	/**
	 * Gets a ["user"] by its Email
	 */
	public function getByEmail(string $user_email) {
		return $this->records()->where('email',$user_email)->getFirstModel();
	}

	/**
	 * Gets a ["user"] by its email confirmation token
	 */
	public function getByEmailConfirmationToken(string $token) {
		return $this->records()->where('email_confirmation_token', $token)->getFirstModel();
	}

	/**
	 * Generates a random salt for a user
	 * @return string the generated salt
	 */
	public function generateSalt() {
		return CustomUtilities::generateRandom(22);
	}

	/**
	 * Hashes a password based on a given salt
	 * @param $s string password salt
	 * @param $pwd string password to be used
	 */
	public function passwordHash($s, $pwd) {
		return password_hash($pwd, PASSWORD_BCRYPT, [ 'salt' => $s ]);
	}

	/**
	 * Updates the table defining active account
	 * @return ModelUser
	 */
	public function activate() {
		$this->active = true;
		$this->active_on = time();
		$this->save();

		return $this;
	}

	/**
	 * Updates the table defining inactive account
	 * @return ModelUser
	 */
	public function deactivate() {
		$this->active = false;
		//$this->active_on = null;
		$this->save();

		return $this;
	}

	/**
	 * Updates the table defining email was confirmed
	 * @return ModelUser
	 */
	public function confirmEmail() {
		$this->confirmed_email = true;
		$this->confirmed_email_on = time();
		$this->save();

		return $this;
	}

	/**
	 * Sends an account creation email
	 * @param string $OAuthMode The mode used to enter the app
	 * @return array the sent mail response object
	 */
	public function sendAccountCreationEmail(string $OAuthMode) {

		$userName = $this->Profile->name;
		$userSurname = $this->Profile->surname;

		if ($OAuthMode == OAuthDataProvider::AppMode) {
			$mailSubject = ModelUser::UserCreatedMailSubjectConfirmEmail;
			//Template for the app signup mode
			$template = new Template('app/designs/emails/pt-br/AccountCreationEmail.app.html');
			$template->confirmationLink = config::$EmailConfirmationAppUrl . 't=' . $this->email_confirmation_token;

			//Raw text version
			$rawTextTemplate =
				"Olá {$userName}, " .
				"obrigado por inscrever-se no App NexAtlas. " .
				"Confirme seu email clicando no link: " .
				config::$EmailConfirmationAppUrl . "t=" . $this->email_confirmation_token;
		} else {
			$mailSubject = ModelUser::UserCreatedMailSubjectNoConfirmation;
			//Template for the other signup modes
			$template = new Template('app/designs/emails/pt-br/AccountCreationEmail.other.html');
			$template->appUrl = config::$AppUrl;

			//Raw text version
			$rawTextTemplate =
				"Olá {$userName}, " .
				"seja bem-vindo e um muito obrigado por inscrever-se no App NexAtlas." .
				"Para acessar o aplicativo, clique no link: " . config::$AppUrl;
		}

		//replaces image: {@logoImageUri}
		$template->logoImageUri = config::$EmailLogoImageUri;
		//replaces link: {@nexatlasLink}
		$template->nexatlasLink = config::$EmailLogoLinkUrl;
		//replaces image: {@instagramImageUri}
		$template->instagramImageUri = config::$EmailInstagramImageUri;
		//replaces link: {@instagramLink}
		$template->instagramLink = config::$EmailInstagramLinkUrl;
		//replaces image: {@facebookImageUri}
		$template->facebookImageUri = config::$EmailFacebookImageUri;
		//replaces link: {@facebookLink}
		$template->facebookLink = config::$EmailFacebookLinkUrl;
		//replaces image: {@linkedinImageUri}
		$template->linkedinImageUri = config::$EmailLinkedinImageUri;
		//replaces link: {@linkedinLink}
		$template->linkedinLink = config::$EmailLinkedinLinkUrl;

		//Renders the HTML template version
		$renderedTemplate = $template->render();

		//Sends the email
		$mailSent =
			CustomUtilities::sendEmail(
				MailProvider::SendGrid,
				implode(' ', [$userName, $userSurname]),
				$this->email,
				$mailSubject,
				//HTML version
				$renderedTemplate,
				//Raw text version
				$rawTextTemplate
				);

		return $mailSent;
	}

	/**
	 * Sends a password request email
	 * @param string $request The password request object
	 * @return array the sent mail response object
	 */
	public function sendPasswordRequestEmail(ModelUserPasswordRequest $request) {
		$userName = $this->Profile->name;
		$userSurname = $this->Profile->surname;

		if (!isset($request)) throw new Exception('PasswordRequestInvalid');

		$mailSubject = ModelUser::PasswordRequestedMailSubject;
		//Template for the app pass request
		$template = new Template('app/designs/emails/pt-br/PasswordRequestEmail.html');
		$template->userName = $userName;
		$template->passwordRequestUrl = config::$PasswordRequestAppUrl . 't=' . $request->token;

		//replaces image: {@logoImageUri}
		$template->logoImageUri = config::$EmailLogoImageUri;
		//replaces link: {@nexatlasLink}
		$template->nexatlasLink = config::$EmailLogoLinkUrl;
		//replaces image: {@instagramImageUri}
		$template->instagramImageUri = config::$EmailInstagramImageUri;
		//replaces link: {@instagramLink}
		$template->instagramLink = config::$EmailInstagramLinkUrl;
		//replaces image: {@facebookImageUri}
		$template->facebookImageUri = config::$EmailFacebookImageUri;
		//replaces link: {@facebookLink}
		$template->facebookLink = config::$EmailFacebookLinkUrl;
		//replaces image: {@linkedinImageUri}
		$template->linkedinImageUri = config::$EmailLinkedinImageUri;
		//replaces link: {@linkedinLink}
		$template->linkedinLink = config::$EmailLinkedinLinkUrl;

		//Raw text version
		$rawTextTemplate =
			"Olá {$userName},\r\n" .
			"Recebemos seu pedido para recuperação de senha!\r\n" .
			"Clique neste link (" . config::$PasswordRequestAppUrl . 't=' . $request->token . ") para efetuar a troca de sua senha\r\n".
			"Se não fez este pedido, desconsidere esta mensagem. Nenhuma alteração será feita em sua conta.\r\n" .
			"Se desejar mais informações, fale conosco através do endereço contato@nexatlas.com\r\n" .
			"Equipe NexAtlas";

		//Renders the HTML template version
		$renderedTemplate = $template->render();

		//Sends the email
		$mailSent =
			CustomUtilities::sendEmail(
				MailProvider::SendGrid,
				implode(' ', [$userName, $userSurname]),
				$this->email,
				$mailSubject,
				//HTML version
				$renderedTemplate,
				//Raw text version
				$rawTextTemplate
				);

		return $mailSent;
	}

	#endregion
}
