<?php

/**
 * ModelAuthentication provides user authentication info.
 *
 * ModelAuthentication stands for a simple class which provides main user auth info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelAuthentication extends Model
{
	#region Properties

    public $primary_key = "id";
    public $table_name = "authentication";

	public $field_config = [
		'id' => [ 'type' => Model::type_time ],
		'user_id' => [ 'type' => Model::type_int ],
		'creation_date' => [ 'type' => Model::type_time ],
		'valid_to' => [ 'type' => Model::type_time ],
		'mode' => [ 'type' => Model::type_varchar ],
		'dead' => [ 'type' => Model::type_boolean ],
		'dead_on' => [ 'type' => Model::type_time ],
		'token' => [ 'type' => Model::type_varchar ],
		'device' => [ 'type' => Model::type_varchar ]
	];
	public $id;
	public $user_id;
	public $creation_date;
	public $valid_to;
	public $mode;
	public $dead;
	public $dead_on;
	public $token;
	public $device;
	public $hasOne = [
		'User' => [
			'model' => 'ModelUser',
			'where' => [
				['id','=','user_id']
			]
		]
	];

	#endregion

	#region Methods

	/**
	 * Gets an ["authentication"] by its (timestamp) ID
	 * @param int $timestamp_id
	 */
	public function getById(int $timestamp_id) {
		return $this->records()->where('id',$timestamp_id)->getFirstModel();
	}

	/**
	 * Gets an ["authentication"] by its user ID
	 * @param int $user_id
	 */
	public function getByUserId(int $user_id) {
		return $this->records()->where('user_id',$user_id)->getFirstModel();
	}

	/**
	 * Gets an ["authentication"] by its token
	 * @param int $user_id
	 */
	public function getByToken($authToken) {
		return $this->records()->where('token',$authToken)->getFirstModel();
	}

	/**
	 * Sets the current logged user and register the authentication
	 * @param mixed $user The user to register a sign in
	 * @param string $mode The authentication method used to sign in
	 * @return ModelAuthentication The just created authentication object
	 */
	public function setSession($user, $mode) {
		//This object is used for front-end mapping of info
		CurrentUser::set($user, $mode);

		//Registers the entrance of the user
		$this->register($user, $mode);

		return $this;
	}

	/**
	 * Summary of register
	 * @param ModelUser $user the user to have its authentication registered
	 * @param string $mode the authentication method used
	 */
	public function register(ModelUser $user, $mode) {
		//The id of the auth object is the current time
		//$this->id = time();
		$this->user_id = $user->id;
		//Adds 3 months to the auth date
		$this->creation_date = time();
		$this->valid_to = strtotime('+3 months', $this->creation_date);
    /*
    Generates JWT to use the auth method
    $key = "secret";
    $payload = array(
      // Start: RFC 7519: https://tools.ietf.org/html/rfc7519#section-4.1.1
      "iss" => "http://example.org",
      "aud" => "http://example.com",
      "iat" => 1356999524,
      "nbf" => 1357000000,
      // End: RFC 7519: https://tools.ietf.org/html/rfc7519#section-4.1.1
      "user_id" => ... ,
    );

    */
		$this->token = md5($this->creation_date.$user->generateSalt());
		$this->mode = $mode;
		//TODO: Implement device identification
		//$this->device = not used yet cause of browser limitations
		$this->records()->save();

		return $this;
	}

	/**
	 * Invalidates by killing an user authentication
	 */
	public function kill($time = null) {

		//Invalidates the authentication object by puttin a dead label on it :)
		$this->dead = true;
		$this->dead_on = isset($time) ? $time : time();
		$this->update();

		return $this;
	}

	#endregion
}
