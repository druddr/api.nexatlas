<?php

/**
 * ModelAiracCycle provides available AIRAC cycles info.
 *
 * ModelAiracCycle stands for a class which provides AIRAC cycles info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelAiracCycle extends Model
{
	#region Properties

    public $primary_key = "id";
	public $primary_key_is_string = true;
	public $primary_key_is_auto_increment = false;
    public $table_name = "airac_cycle";

	public $field_config = [
		'id' => ['type' => Model::type_int],
		'ident' => ['type' => Model::type_varchar],
		'date_from' => ['type' => Model::type_time],
		'date_to' => ['type' => Model::type_time],
		'creation_date' => ['type' => Model::type_time],
		'active' => ['type' => Model::type_boolean]
	];
	public $id;
	public $ident;
	public $date_from;
	public $date_to;
	public $creation_date;
	public $active;
	public $hasMany = [
		'Waypoints' => [
			'model' => 'ModelWaypoint',
			'where' => [
				['id','=','airac_cycle_id'],
			],
			'order' => ['id'=>'ASC'],
			'limit' => []
		],
		'RouteWaypoints' => [
			'model' => 'ModelRouteWaypoint',
			'where' => [
				['id','=','airac_cycle_id'],
			],
			'order' => ['id'=>'ASC'],
			'limit' => []
		]
	];

	#endregion

	#region Methods

	/**
	 * Gets an ["airac_cycle"] by its ID
	 */
	public function getById(int $airacCycleId) {
		return $this->records()->where('id',$airacCycleId)->getFirstModel();
	}

	#endregion
}
