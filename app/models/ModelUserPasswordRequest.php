<?php

/**
 * ModelUserPasswordRequest provides user password request access.
 *
 * ModelUserPasswordRequest stands for a simple class which provides info upon
 * new password requests.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelUserPasswordRequest extends Model
{
	#region Properties

    public $primary_key = "id";
	public $primary_key_is_string = false;
    public $table_name = "user_password_request";
	public $primary_key_is_auto_increment = true;

	public $field_config = [
		'id' => [ 'type' => Model::type_int ],
		'token' => [ 'type' => Model::type_varchar ],
		'user_id' => [ 'type' => Model::type_int ],
		'is_valid' => [ 'type' => Model::type_boolean ],
		'used_on' => [ 'type' => Model::type_time ],
		'creation_date' => [ 'type' => Model::type_time ]
	];
	public $id;
	public $token;
	public $user_id;
	public $is_valid;
	public $used_on;
	public $creation_date;
	public $hasOne =
	[
		'User' => [
			'model' => 'ModelUser',
			'where' => [
				['id','=','user_id']
			],
			'limit' => [1]
			]
	];

	#endregion

	#region Methods

	/**
	 * Gets a ["user_password_request"] by its ID
	 */
	public function getById(int $request_id) {
		return $this->records()->where('id', $request_id)->getFirstModel();
	}

	/**
	 * Gets a ["user_password_request"] by its token
	 */
	public function getByToken(string $request_token) {
		return $this->records()->where('token', $request_token)->getFirstModel();
	}

	/**
	 * Gets a ["user_password_request"] by its user ID
	 */
	public function getByUserId(int $user_id) {
		return $this->records()->where('user_id', $user_id)->getFirstModel();
	}

	/**
	 * Kills this password request object by setting it valid = false
	 * @return ModelUserPasswordRequest
	 */
	public function invalidate() {
		//if (!$this->isValid()) return $this;
		if (!$this->is_valid) return $this;

		//Invalidates the password request
		$this->is_valid = false;

		//TODO: Here we forcedly update the model, cause save would call insert();
		$this->update();

		return $this;
	}

	/**
	 * Burns this password request object by setting it valid = false and used
	 * @return ModelUserPasswordRequest
	 */
	public function burn() {
		if (!$this->isValid()) return $this;

		//Invalidates the password request
		$this->is_valid = false;
		$this->used_on = time();

		//TODO: Here we forcedly update the model, cause save would call insert();
		$this->update();

		return $this;
	}

	/**
	 * Checks whether the current instance of password request is valid and not expired
	 * @return boolean whether it is valid or not
	 */
	public function isValid() {
		return
			((date_diff(new DateTime($this->creation_date), new DateTime(), true)->h <=
			Config::$PasswordRequestValidityHours)
			&&
			(!isset($this->used_on) && $this->is_valid));
	}

	#endregion
}