<?php
/**
 * Counts total accounts at the database, daily basis
 *
 * Counts the cumulative total users on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountTotalUsersWeekly();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily grouping of total accounts
 *	 Starting from day 1.
 */
class ViewCountTotalUsersWeekly extends Model
{
    public $primary_key = "creation_week";
	public $primary_key_is_string = true;
    public $table_name = "view_count_total_users_weekly";
	public $field_config = [
		'user_cumulative_count' => ['type' => Model::type_int],
		'creation_week' => ['type' => Model::type_varchar],
		'creation_month' => ['type' => Model::type_varchar],
		'creation_year' => ['type' => Model::type_varchar]
	];

	public $user_cumulative_count;
	public $creation_week;
	public $creation_month;
	public $creation_year;

	/**
	 * Gets a daily basis of total users
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountTotalUsersWeekly[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		$records = $records->select(
			[
				"(@total_weekly := @total_weekly + user_count) AS user_cumulative_count",
				"creation_week",
				"creation_month",
				"creation_year"
			]);

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_week', date('W', strtotime($intervalFrom)), date('W', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('creation_week', date('W', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('creation_week', date('W', strtotime($intervalTo)), "<=");

		$records = $records->joinSql("(SELECT @total_weekly := 0) AS r");

		$records = $records->group(
			[
				"creation_week",
				"creation_month",
				"creation_year"
			]);

		return $records->order([
			"creation_week",
			"creation_month",
			"creation_year"
		], 'ASC')->toModelArray();
	}
}
