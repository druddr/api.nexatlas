<?php


class ViewRotas extends Model
{
    public $primary_key = "id_action";
    public $table_name = "view_rotas";
	public $field_config = [
		'id_sistema' => ['type' => Model::type_int],
		'nome_sistema' => ['type' => Model::type_varchar],
		'descricao_sistema' => ['type' => Model::type_varchar],
		'tipo_sistema' => ['type' => Model::type_varchar],
		'ativo_sistema' => ['type' => Model::type_boolean],
		'id_modulo' => ['type' => Model::type_int],
		'nome_modulo' => ['type' => Model::type_varchar],
		'diretorio_modulo' => ['type' => Model::type_varchar],
		'id_controller' => ['type' => Model::type_int],
		'nome_controller' => ['type' => Model::type_varchar],
		'diretorio_controller' => ['type' => Model::type_varchar],
		'id_action' => ['type' => Model::type_int],
		'nome_action' => ['type' => Model::type_varchar],
		'metodo_action' => ['type' => Model::type_varchar],
		'prefixo_rota' => ['type' => Model::type_varchar],
		'rota' => ['type' => Model::type_varchar]
	];
	public $id_sistema;
	public $nome_sistema;
	public $descricao_sistema;
	public $tipo_sistema;
	public $ativo_sistema;
	public $id_modulo;
	public $nome_modulo;
	public $diretorio_modulo;
	public $id_controller;
	public $nome_controller;
	public $diretorio_controller;
	public $id_action;
	public $nome_action;
	public $metodo_action;
	public $prefixo_rota;
	public $rota;
}