<?php
/**
 * Counts total authentications, date/time based at the database
 *
 * Counts the total authentications weekly on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountAuthenticationsWeekly();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the weekly accesses count
 *	 Starting from day 1.
 */
class ViewCountAuthenticationsWeekly extends Model
{
    public $primary_key = "creation_week";
	public $primary_key_is_string = true;
    public $table_name = "view_count_authentications_weekly";
	public $field_config = [
		'user_authentications_count' => ['type' => Model::type_int],
		'creation_week' => ['type' => Model::type_varchar],
		'creation_month' => ['type' => Model::type_varchar],
		'creation_year' => ['type' => Model::type_varchar]
	];

	public $user_authentications_count;
	public $creation_week;
	public $creation_month;
	public $creation_year;

	/**
	 * Gets a weekly count of authentications
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountAuthenticationsWeekly[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_week', date('W', strtotime($intervalFrom)), date('W', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('creation_week', date('W', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('creation_week', date('W', strtotime($intervalTo)), "<=");

		return $records->order(['creation_week', 'creation_month', 'creation_year'], 'ASC')->toModelArray();
	}
}
