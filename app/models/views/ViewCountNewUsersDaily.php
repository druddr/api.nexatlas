<?php
/**
 * Counts total new accounts, date/time based at the database
 *
 * Counts the differential total new users on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountNewUsersDaily();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily grouping of total accounts
 *	 Starting from day 1.
 */
class ViewCountNewUsersDaily extends Model
{
    public $primary_key = "creation_date";
	public $primary_key_is_string = true;
    public $table_name = "view_count_new_users_daily";
	public $field_config = [
		'users_count' => ['type' => Model::type_int],
		'creation_date' => ['type' => Model::type_date],
		'original_creation_date' => ['type' => Model::type_datetime]
	];

	public $users_count;
	public $creation_date;
	public $original_creation_date;

	/**
	 * Gets a daily basis of total users
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountNewUsersDaily[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalFrom)), date('Y-m-d 00:00:00', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalTo)), "<=");

		return $records->order('original_creation_date', 'ASC')->toModelArray();
	}

	/**
	 * Gets a weekly basis of total users
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountNewUsersDaily[]
	 */
	public function getWeekly($intervalFrom = null, $intervalTo = null) {
		$weeklyResult = $this->records();

		if (isset($intervalFrom) && isset($intervalTo))
			$weeklyResult = $weeklyResult->between('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalFrom)), date('Y-m-d 00:00:00', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$weeklyResult = $weeklyResult->where('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$weeklyResult = $weeklyResult->where('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalTo)), "<=");

		$weeklyResult = $weeklyResult->group(); // ('original_creation_date', 'ASC')->toModelArray();

		return $weeklyResult;
	}

	public function getMonthly($intervalFrom = null, $intervalTo = null) {

	}

	public function getYearly($intervalFrom = null, $intervalTo = null) {

	}
}
