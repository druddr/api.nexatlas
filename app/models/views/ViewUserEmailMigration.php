<?php
/**
 * Counts total accounts at the database, daily basis
 *
 * Counts the cumulative total users on the database
 * - To use it, create an instance of this class:
 * $count = new ViewUserEmailMigration();
 * - Then, invoke the method:
 * $count = $migration->get();
 * - This will default to the daily grouping of migrated accounts
 *	 Starting from day 1.
 */
class ViewUserEmailMigration extends Model
{
    public $primary_key = "created_on";
	public $primary_key_is_string = true;
    public $table_name = "view_user_email_migration";
	public $field_config = [
		'new_email_count' => ['type' => Model::type_int],
		'migrated_email_count' => ['type' => Model::type_int],
		'created_on' => ['type' => Model::type_varchar],
		'original_creation_date' => ['type' => Model::type_varchar],
		'total_legacy_accounts' => ['type' => Model::type_int],
		'migration_percentage' => ['type' => Model::type_float]
	];

	public $new_email_count;
	public $migrated_email_count;
	public $created_on;
	public $original_creation_date;
	public $migration_percentage;

	/**
	 * Gets a daily basis of total migrated users
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewUserEmailMigration[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		$records = $records->select(
			[
				"new_email_count",
				"migrated_email_count",
				"created_on",
				"original_creation_date",
				"total_legacy_accounts",
				"(@total_percentage := @total_percentage + ((migrated_email_count / total_legacy_accounts) * 100)) AS migration_percentage"
			]);

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_week', date('Y-m-d 00:00:00', strtotime($intervalFrom)), date('Y-m-d 00:00:00', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('creation_week', date('Y-m-d 00:00:00', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('creation_week', date('Y-m-d 00:00:00', strtotime($intervalTo)), "<=");

		$records = $records->joinSql("(SELECT @total_percentage := 0) AS r");

		$records = $records->group(
			[
				"new_email_count",
				"migrated_email_count",
				"created_on",
				"original_creation_date",
				"total_legacy_accounts"
			]);

		return $records->order([
			"original_creation_date"
		], 'ASC')->toModelArray();
	}
}
