<?php
/**
 * Counts users which authenticated on the app, date/time based at the database
 *
 * Counts the users which visited the app weekly on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountUsersWhichAuthenticatedWeekly();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the weekly user single accesses count
 *	 Starting from day 1.
 */
class ViewCountUsersWhichAuthenticatedWeekly extends Model
{
    public $primary_key = "creation_week";
	public $primary_key_is_string = true;
    public $table_name = "view_count_users_which_authenticated_weekly";
	public $field_config = [
		'users_count' => ['type' => Model::type_int],
		'creation_week' => ['type' => Model::type_varchar],
		'creation_month' => ['type' => Model::type_varchar],
		'creation_year' => ['type' => Model::type_varchar]
	];

	public $users_count;
	public $creation_week;
	public $creation_month;
	public $creation_year;

	/**
	 * Gets a weekly count of users which authenticated on the app
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountUsersWhichAuthenticatedWeekly[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_week', date('W', strtotime($intervalFrom)), date('W', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('creation_week', date('W', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('creation_week', date('W', strtotime($intervalTo)), "<=");

		return $records->order(['creation_week', 'creation_year'], 'ASC')->toModelArray();
	}
}
