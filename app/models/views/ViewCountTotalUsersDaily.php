<?php
/**
 * Counts total accounts at the database, daily basis
 *
 * Counts the cumulative total users on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountTotalUsersDaily();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily grouping of total accounts
 *	 Starting from day 1.
 */
class ViewCountTotalUsersDaily extends Model
{
    public $primary_key = "db_created_on";
	public $primary_key_is_string = true;
    public $table_name = "view_count_total_users_daily";
	public $field_config = [
		'user_cumulative_count' => ['type' => Model::type_int],
		'created_on' => ['type' => Model::type_varchar],
		'db_created_on' => ['type' => Model::type_datetime]
	];

	public $user_cumulative_count;
	public $created_on;
	public $db_created_on;

	/**
	 * Gets a daily basis of total users
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountTotalUsersDaily[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		$records = $records->select(
			[
				"(@total_daily := @total_daily + user_count) AS user_cumulative_count",
				"created_on",
				"db_created_on"
			]);

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('db_created_on', date('Y-m-d 00:00:00', strtotime($intervalFrom)), date('Y-m-d 00:00:00', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('db_created_on', date('Y-m-d 00:00:00', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('db_created_on', date('Y-m-d 00:00:00', strtotime($intervalTo)), "<=");

		$records = $records->joinSql("(SELECT @total_daily := 0) AS r");

		$records = $records->group(
			[
				"created_on",
				"db_created_on"
			]);

		return $records->order([
			"db_created_on"
		], 'ASC')->toModelArray();
	}
}
