<?php
/**
 * Counts total new accounts, date/time monthly based at the database
 *
 * Counts the differential total new users on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountNewUsersMonthly();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily grouping of total accounts
 *	 Starting from month 1.
 */
class ViewCountNewUsersMonthly extends Model
{
    public $primary_key = "creation_month";
	public $primary_key_is_string = true;
    public $table_name = "view_count_new_users_monthly";
	public $field_config = [
		'users_count' => ['type' => Model::type_int],
		'creation_month' => ['type' => Model::type_varchar],
		'creation_year' => ['type' => Model::type_varchar]
	];

	public $users_count;
	public $creation_month;
	public $creation_year;

	/**
	 * Gets a monthly basis of new users
	 * @param mixed $intervalFrom Date containing begin limit month to consider
	 * @param mixed $intervalTo Date containing end limit month to consider
	 * @return ViewCountNewUsersMonthly[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		//Sets interval if defined
		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_month', date('m', strtotime($intervalFrom)), date('m', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('creation_month', date('m', strtotime($intervalFrom)), ">=", false);
		else if (isset($intervalTo))
			$records = $records->where('creation_month', date('m', strtotime($intervalTo)), "<=", false);

		//Returns results ordering
		return $records->order('creation_month', 'ASC')->toModelArray();
	}
}
