<?php
/**
 *
 */
class ViewCountUniqueAuthenticationsPerUser extends Model
{
    public $primary_key = "user_id";
    public $table_name = "view_count_unique_authentications_per_user";
	public $field_config = [
		'user_authentication_count' => ['type' => Model::type_int],
		'user_id' => ['type' => Model::type_int],
		'user_email' => ['type' => Model::type_int]
	];
	public $user_authentication_count;
	public $user_id;
	public $user_email;

	/**
	 * Summary of getByUserId
	 * @param mixed $user_id
	 * @return array
	 */
	public function getByUserId($user_id){
		return $this->records()->where('user_id', $user_id)->toStandardArray();
	}
}
