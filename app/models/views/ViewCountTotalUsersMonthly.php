<?php
/**
 * Counts total accounts at the database, daily basis
 *
 * Counts the cumulative total users on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountTotalUsersMonthly();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily grouping of total accounts
 *	 Starting from day 1.
 */
class ViewCountTotalUsersMonthly extends Model
{
    public $primary_key = "creation_date";
	public $primary_key_is_string = true;
    public $table_name = "view_count_new_users_monthly";
	public $field_config = [
		'user_cumulative_count' => ['type' => Model::type_int],
		'creation_month' => ['type' => Model::type_varchar],
		'creation_year' => ['type' => Model::type_varchar]
	];

	public $user_cumulative_count;
	public $creation_month;
	public $creation_year;

	/**
	 * Gets a daily basis of total users
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountTotalUsersMonthly[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		$records = $records->select(
			[
				"(@total_monthly := @total_monthly + users_count) AS user_cumulative_count",
				"creation_month",
				"creation_year"
			]);

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_month', date('m', strtotime($intervalFrom)), date('m', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('creation_month', date('m', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('creation_month', date('m', strtotime($intervalTo)), "<=");

		$records = $records->joinSql("(SELECT @total_monthly := 0) AS r");

		$records = $records->group(
			[
				"creation_month",
				"creation_year"
			]);

		return $records->order(['creation_month', 'creation_year'], 'ASC')->toModelArray();
	}
}
