<?php
/**
 * View for aerodrome usage report
 */
class ViewCountTotalUsagePerAerodrome extends Model
{
    public $primary_key = "aerodrome_icao";
	public $primary_key_is_string = true;
    public $table_name = "view_count_total_usage_per_aerodrome";
	public $field_config = [
		'aerodrome_usage_count' => ['type' => Model::type_int],
		'aerodrome_name' => ['type' => Model::type_varchar],
		'aerodrome_icao' => ['type' => Model::type_varchar],
		'aerodrome_iata' => ['type' => Model::type_varchar],
		'aerodrome_is_origin' => ['type' => Model::type_boolean],
		'aerodrome_is_destination' => ['type' => Model::type_boolean]
	];

	public $aerodrome_usage_count;
	public $aerodrome_name;
	public $aerodrome_icao;
	public $aerodrome_iata;
	public $aerodrome_is_origin;
	public $aerodrome_is_destination;

	/**
	 * Gets the total usage per aerdrome in the system
	 * @return ViewCountTotalUsagePerAerodrome[]
	 */
	public function getAll() {
		return $this->records()->toModelArray();
	}

	/**
	 * Gets the total usage per aerodrome, as route origin
	 * @return ViewCountTotalUsagePerAerodrome[]
	 */
	public function getOrigins() {
		return $this
			->records()
			->where('aerodrome_is_origin', 1)
			->order('aerodrome_usage_count', 'DESC')
			->toModelArray();
	}

	/**
	 * Gets the total usage per aerodrome, as route destination
	 * @return ViewCountTotalUsagePerAerodrome[]
	 */
	public function getDestinations() {
		return $this
			->records()
			->where('aerodrome_is_destination', 1)
			->order('aerodrome_usage_count', 'DESC')
			->toModelArray();
	}
}
