<?php
/**
 *
 */
class ViewCountTotalUsersPerAuthenticationsGrouping extends Model
{
    public $primary_key = "user_authentication_frequency";
    public $table_name = "view_count_total_users_per_authentications_grouping";
	public $field_config = [
		'user_authentication_frequency' => ['type' => Model::type_int],
		'users_count' => ['type' => Model::type_int]
	];

	public $user_authentication_frequency;
	public $users_count;

	/**
	 * Gets the total count of user grouped by user authentications count
	 * @return ViewCountTotalUsersPerAuthenticationsGrouping[]
	 */
	public function get() {
		return $this->records()->toModelArray();
	}
}
