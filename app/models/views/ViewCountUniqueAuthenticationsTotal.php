<?php
/**
 *
 */
class ViewCountUniqueAuthenticationsTotal extends Model
{
    public $primary_key = "unique_authentications_count";
    public $table_name = "view_count_unique_authentications_per_user";
	public $field_config = [
		'unique_authentications_count' => ['type' => Model::type_int]
	];
	public $unique_authentications_count;

	/**
	 * Summary of getByUserId
	 * @param mixed $user_id
	 * @return array
	 */
	public function get(){
		return $this->records()->getFirstModel();
	}
}
