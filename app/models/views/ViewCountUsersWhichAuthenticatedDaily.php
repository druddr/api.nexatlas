<?php
/**
 * Counts users which authenticated on the app, date/time based at the database
 *
 * Counts the users which visited the app daily on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountUsersWhichAuthenticatedDaily();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily user single accesses count
 *	 Starting from day 1.
 */
class ViewCountUsersWhichAuthenticatedDaily extends Model
{
    public $primary_key = "created_on";
	public $primary_key_is_string = true;
    public $table_name = "view_count_users_which_authenticated_daily";
	public $field_config = [
		'users_count' => ['type' => Model::type_int],
		'created_on' => ['type' => Model::type_varchar],
		'original_creation_date' => ['type' => Model::type_datetime]
	];

	public $users_count;
	public $created_on;
	public $original_creation_date;

	/**
	 * Gets a daily count of users which authenticated on the app
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountUsersWhichAuthenticatedDaily[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalFrom)), date('Y-m-d 00:00:00', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->where('original_creation_date', date('Y-m-d 00:00:00', strtotime($intervalTo)), "<=");

		return $records->order('original_creation_date', 'ASC')->toModelArray();
	}
}
