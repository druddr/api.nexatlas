<?php
/**
 * Counts total new accounts, date/time weekly based at the database
 *
 * Counts the differential total new users on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountNewUsersWeekly();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the daily grouping of total accounts
 *	 Starting from week 1.
 */
class ViewCountNewUsersWeekly extends Model
{
    public $primary_key = "creation_week";
	public $primary_key_is_string = true;
    public $table_name = "view_count_new_users_weekly";
	public $field_config = [
		'users_count' => ['type' => Model::type_int],
		'creation_week' => ['type' => Model::type_varchar],
		'creation_month' => ['type' => Model::type_varchar],
		'creation_year' => ['type' => Model::type_varchar]
	];

	public $users_count;
	public $creation_week;
	public $creation_month;
	public $creation_year;

	/**
	 * Gets a weekly basis of new users
	 * @param mixed $intervalFrom Date containing begin limit week to consider
	 * @param mixed $intervalTo Date containing end limit week to consider
	 * @return ViewCountNewUsersWeekly[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		//Sets interval if defined
		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_week', date('W', strtotime($intervalFrom)), date('W', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->where('creation_week', date('W', strtotime($intervalFrom)), ">=", false);
		else if (isset($intervalTo))
			$records = $records->where('creation_week', date('W', strtotime($intervalTo)), "<=", false);

		//Returns results ordering
		return $records->order('creation_week', 'ASC')->toModelArray();
	}
}
