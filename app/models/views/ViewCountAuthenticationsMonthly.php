<?php
/**
 * Counts total authentications, date/time based at the database
 *
 * Counts the total authentications monthly on the database
 * - To use it, create an instance of this class:
 * $count = new ViewCountAuthenticationsMonthly();
 * - Then, invoke the method:
 * $count = $count->get();
 * - This will default to the monthly accesses count
 *	 Starting from day 1.
 */
class ViewCountAuthenticationsMonthly extends Model
{
    public $primary_key = "creation_month";
	public $primary_key_is_string = true;
    public $table_name = "view_count_authentications_monthly";
	public $field_config = [
		'user_authentications_count' => ['type' => Model::type_int],
		'creation_month' => ['type' => Model::type_varchar],
		'creation_year' => ['type' => Model::type_varchar]
	];

	public $user_authentications_count;
	public $creation_month;
	public $creation_year;

	/**
	 * Gets a monthly count of authentications
	 * @param mixed $intervalFrom
	 * @param mixed $intervalTo
	 * @return ViewCountAuthenticationsMonthly[]
	 */
	public function get($intervalFrom = null, $intervalTo = null) {
		$records = $this->records();

		if (isset($intervalFrom) && isset($intervalTo))
			$records = $records->between('creation_month', date('m', strtotime($intervalFrom)), date('m', strtotime($intervalTo)));
		else if (isset($intervalFrom))
			$records = $records->having('creation_month', date('m', strtotime($intervalFrom)), ">=");
		else if (isset($intervalTo))
			$records = $records->having('creation_month', date('m', strtotime($intervalTo)), "<=");

		return $records->order(['creation_month', 'creation_year'], 'ASC')->toModelArray();
	}
}
