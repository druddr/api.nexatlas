<?php

/**
 * ModelWaypointType provides types of waypoint info.
 *
 * ModelWaypointType stands for a class which provides a typification of waypoints info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelWaypointType extends Model
{
	#region Properties

	/**
	 * Aerodrome waypoint type
	 */
	const TypeAerodrome = 'AD';
	/**
	 * City waypoint type
	 */
	const TypeCity = 'CITY';
	/**
	 * Geo Coords waypoint type
	 */
	const TypeGeoCoordinates = 'COORDS';
	/**
	 * Fix waypoint type
	 */
	const TypeFix = 'FIX';
	/**
	 * Heliport waypoint type
	 */
	const TypeHeliport = 'HELPN';
	/**
	 * NDB waypoint type
	 */
	const TypeNDB = 'NDB';
	/**
	 * VOR waypoint type
	 */
	const TypeVOR = 'VOR';
	/**
	 * VOR/DME waypoint type
	 */
	const TypeVORDME = 'VOR/DME';

    public $primary_key = "id";
    public $table_name = "waypoint_type";

	public $field_config = [
		'id' => ['type' => Model::type_int],
		'name' => ['type' => Model::type_varchar],
		'code_name' => ['type' => Model::type_varchar],
		'icon' => ['type' => Model::type_varchar],
		'creation_date' => ['type' => Model::type_time]
	];
	public $id;
	public $name;
	public $code_name;
	public $icon;
	public $creation_date;
	public $hasMany = [
		'RouteWaypoints' => [
			'model' => 'ModelRouteWaypoint',
			'where' => [
				['id','=','waypoint_type_id'],
			],
			'limit' => []
		],
		'Waypoints' => [
			'model' => 'ModelWaypoint',
			'where' => [
				['id','=','waypoint_type_id'],
			],
			'limit' => []
		]
	];

	#endregion

	#region Methods

	/**
	 * Gets a ["waypoint_type"] by its ID
	 */
	public function getById(int $waypointTypeId) {
		return $this->records()->where('id',$waypointTypeId)->getFirstModel();
	}

	/**
	 * Gets a ["waypoint_type"] by its name
	 */
	public function getByName($waypointTypeName) {
		return $this->records()->where('name',$waypointTypeName)->getFirstModel();
	}

	/**
	 * Gets a ["waypoint_type"] by its code_name
	 */
	public function getByCodeName($waypointTypeCodeName) {
		return $this->records()->where('code_name',$waypointTypeCodeName)->getFirstModel();
	}

	#endregion
}
