<?php

/**
 * ModelRoute provides navigational route info.
 *
 * ModelRoute stands for a simple class which provides main air nav route info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelRoute extends Model implements LogicallyDeletable, CustomExtractable
{
	#region Properties

    public $primary_key = "id";
    public $table_name = "route";

	public $field_config = [
		'id' => ['type' => Model::type_int],
		'user_id' => ['type' => Model::type_int],
		'name' => ['type' => Model::type_varchar],
		'ground_speed' => ['type' => Model::type_float],
		'is_simple' => ['type' => Model::type_boolean],
		'favourite' => ['type' => Model::type_boolean],
		'deleted' => ['type' => Model::type_boolean],
		'deleted_on' => ['type' => Model::type_time],
		'creation_date' => ['type' => Model::type_time],
		'updated_on' => ['type' => Model::type_time]
	];
	public $id;
	public $user_id;
	public $name;
	public $ground_speed;
	public $is_simple;
	public $favourite;
	public $deleted;
	public $deleted_on;
	public $creation_date;
	public $updated_on;
	public $hasOne = [
		'User' => [
			'model' => 'ModelUser',
			'where' => [
				['user_id','=','id']
			],
			'limit' => [1]
		],
		'Origin' => [
			'model' => 'ModelRouteWaypoint',
			'where' => [
				['user_id','=','id'],
				['is_origin','=', 1]
			],
			'limit' => [1]
		],
		'Destination' => [
			'model' => 'ModelRouteWaypoint',
			'where' => [
				['user_id','=','id'],
				['is_destination','=', 1]
			],
			'limit' => [1]
		]
	];
	public $hasMany = [
		'Waypoints' => [
			'model' => 'ModelRouteWaypoint',
			'where' => [
				['route_id','=','id'],
				['is_origin', '=', 0],
				['is_destination', '=', 0]
			],
			'order' => ['sequence'=>'ASC'],
			'limit' => []
		],
		'AllWaypoints' => [
			'model' => 'ModelRouteWaypoint',
			'where' => [
				['route_id','=','id'],
			],
			'order' => ['sequence'=>'ASC'],
			'limit' => []
		]
	];

	#endregion

	#region Methods

	/**
	 * Overrides the base
	 * @return StdClass
	 */
	public function extract() {
		//Corrects the boolean value of favourite
		$this->favourite = (bool)(intval($this->favourite) == 1);
		$this->is_simple = (bool)(intval($this->is_simple) == 1);
		$this->deleted = (bool)(intval($this->deleted) == 1);

		//Corrects float values
		$this->ground_speed = (float)floatval($this->ground_speed);

		//Corrects time values
		$this->creation_date = is_int($this->creation_date) ? (int)$this->creation_date : (int)strtotime($this->creation_date);
		$this->updated_on = is_int($this->updated_on) ? (int)$this->updated_on : (int)strtotime($this->updated_on);

		return parent::extract();
	}

	/**
	 * Logically deletes a route object
	 * @return ModelRoute
	 */
	public function delete() {
		//Deletes it logically
		$this->deleted = true;
		$this->deleted_on = time();
		$this->save();

		return $this;
	}

	/**
	 * Gets a ["route"] by its ID
	 * @param int $routeId the route ID to look for
	 * @return ModelRoute based on the route id passed on
	 */
	public function getById(int $routeId) {
		return $this->records()->where('id', $routeId)->getFirstModel();
	}

	/**
	 * Gets a ["route"] by its user_id
	 * @param int $userId the user ID to look for routes
	 * @return array of ModelRoute
	 */
	public function getByUserId(int $userId) {
		return $this->records()->where('user_id', $userId)->toModelArray();
	}

	/**
	 * Updates route info based on an stdClass object
	 * @param mixed $data stdClass object containing route info
	 * @return ModelRoute The invoker object of this method
	 */
	public function updateInfo($data) {
		//Updates info
		$this->ground_speed = $data->ground_speed;
		$this->favourite = $data->favourite;
		$this->updated_on = isset($data->updated_on) ? strtotime(date('Y-m-d H:i:s', $data->updated_on)) : time();

		//TODO: Define metrics for deciding
		//		When a route is simple or complex
		//$this->is_simple = to be defined;

		$this->save();

		return $this;
	}

	/**
	 * Checks current route waypoints versus the passed waypoints
	 * @param mixed $waypoints
	 */
	public function checkWaypoints(array $waypoints) {
		//No waypoints to check
		if (count($waypoints) == 0) return $this->AllWaypoints;

		$currentWaypoints = $this->AllWaypoints;

		foreach ($waypoints as $key => $waypoint) {
			//If the current waypoint has already been inserted on the route
			if (count($currentWaypoints->filter(
				function ($item, $index){
					return isset($waypoint->id) && $item->route_id == $waypoint->route_id;
				})->toArray()) > 0) {
				continue;
			}


		}

		return $this->AllWaypoints;
	}

	/**
	 * Adds waypoints to a route object
	 * @param array $waypoints Collection of waypoints to be added to the route
	 * @param boolean $clone If it is to be cloning waypoints
	 * @return ModelRoute
	 */
	public function addWaypoints(array $waypoints, $clone = false) {
		$counter = 1;

		//Each waypoint is added and sequenced to this route
		foreach ($waypoints as $key => $waypoint) {
			//Initializes the added waypoint as an empty array
			$addedRouteWaypoint = (new ModelRouteWaypoint());

			//If the route already exists, we are manipulating it and the waypoint may already exist
			if ($waypoint->route_id
				&& $waypoint->route_id == $this->id) {
				//Tries to find the waypoint
				$addedRouteWaypoint =
					$addedRouteWaypoint
						->records()
						->where('latitude', $waypoint->latitude, "=", false)
						->where('longitude', $waypoint->longitude, "=", false)
						->where('route_id', $waypoint->route_id)
						->getFirstModel();
			}
			//else if (!isset($this->id)
			//            && $clone) {
			//            //&& isset($waypoint->route_id)
			//            //&& isset($waypoint->id)) {
			//    //This case is when the route is new, even though, the waypoint is a copy from
			//    //another route waypoint item at the db
			//    //Tries to find the waypoint, then
			//    $addedRouteWaypoint =
			//        $addedRouteWaypoint
			//            ->records()
			//            ->where('id', $waypoint->id)
			//            ->getFirstModel();

			//    //Removes the waypoint id (will reinsert)
			//    $addedRouteWaypoint->id = null;
			//}

			//If the current waypoint is new
			if (!isset($addedRouteWaypoint->id)) {
				//print_r($waypoint);
				//Adds the waypoint to the route
				$addedRouteWaypoint = $this->addNewRouteWaypoint($waypoint, $counter, $clone);
			}

			//Sets the sequence indication
			$addedRouteWaypoint->sequence = $counter;

			//Defines destination and origin
			if ($counter == 1)
				$addedRouteWaypoint->setAsOrigin();
			else if ($counter == count($waypoints))
				$addedRouteWaypoint->setAsDestination();

			//Saves any changes
			$addedRouteWaypoint->save();

			//Next sequence item
			$counter++;
		}

		return $this;
	}

	/**
	 * Adds a NEW route waypoint
	 * Where a copy of an existing waypoint is done
	 * Or a fake route wapoint is created for defined geo coordinates
	 * @param mixed $waypoint The waypoint containing info to be added
	 * @param mixed $sequence The sequence counter of this waypoint
	 * @param mixed $clone If the waypoint is a clone from another route waypoint
	 * @throws Exception
	 * @return ModelRouteWaypoint The added route waypoint
	 */
	public function addNewRouteWaypoint($waypoint, $sequence, $clone = false) {
		$originalWaypoint = null;
		$isCoordinate =
			(isset($waypoint->waypoint_type_code_name)
			&& $waypoint->waypoint_type_code_name == ModelWaypointType::TypeGeoCoordinates);
		//isset($waypoint->id)
		//&& !isset($this->id)
		//&& isset($waypoint->route_id) ? (new ModelRouteWaypoint()) : (new ModelWaypoint()) ;

		//print_r($waypoint);
		//print_r($this);

		if ($clone && isset($waypoint->id)) {
			//print_r('clone!');
			//The waypoint is ready for insertion cause it should be cloned
			$originalWaypoint = (new ModelRouteWaypoint());
			$originalWaypoint = $originalWaypoint->getModelById($waypoint->id);
		} else if (isset($waypoint->id)) {
			//print_r('waypoint has an ID');
			//If the route exists and the waypoint has an ID
			$originalWaypoint = (new ModelWaypoint());
			$originalWaypoint = $originalWaypoint->getModelById($waypoint->id);
		}

		//print_r($waypoint);
		//print_r($originalWaypoint);

		//Here we have a geo coordinates point
		if ($isCoordinate && !isset($waypoint->id)) {
			$addedRouteWaypoint =
				$this->addWaypoint($waypoint, $sequence);
			//die($originalWaypoint);
			//$originalWaypoint->name =
			//	WaypointUtility::decimalDegreesToDegreeMinutesSeconds($waypoint->latitude) . " / " .
			//	WaypointUtility::decimalDegreesToDegreeMinutesSeconds($waypoint->longitude);
		} else if (!isset($originalWaypoint->id) && !$isCoordinate) {
			//The waypoint was not found by its defined ID
			//And it's not a valid geo coordinate point
			throw new Exception('invalidWaypointCode');
		} else {
			//Waypoint is valid and exists at waypoints ref
			//Adds the waypoint to the route
			$addedRouteWaypoint =
				$this->addWaypoint($originalWaypoint, $sequence);
		}

		return $addedRouteWaypoint;
	}

	/**
	 * Updates the route waypoints list
	 * Adding a waypoint
	 * @param mixed $waypoint The waypoint object
	 * @param mixed $sequence The waypoint sequence integer
	 * @return ModelRouteWaypoint
	 */
	public function addWaypoint($waypoint, $sequence) {

		//Crates a route waypoint to the user route
		//This filling will do the commented lines below
		$addedWaypoint = (new ModelRouteWaypoint())->fill($waypoint);
		//$addedWaypoint = $addedWaypoint->fill($waypoint);
		//$addedWaypoint->waypoint_type_id = $waypoint->waypoint_type_id;
		//$addedWaypoint->airac = $waypoint->airac;
		//$addedWaypoint->code = $waypoint->code;
		//Due to encoding failure, we reset it here
		//$addedWaypoint->name = utf8_encode($waypoint->name);
		//$addedWaypoint->name = $waypoint->name;
		//$addedWaypoint->city_name = isset($waypoint->city_name) ? utf8_encode($waypoint->city_name) : null;
		//$addedWaypoint->city_name = isset($waypoint->city_name) ? $waypoint->city_name : null;
		//$addedWaypoint->icao_code = $waypoint->icao_code;
		//$addedWaypoint->iata_code = $waypoint->iata_code;
		//$addedWaypoint->latitude = $waypoint->latitude;
		//$addedWaypoint->longitude = $waypoint->longitude;

		//Other fields which should be specified for this item forcedly
		$addedWaypoint->id = null; //This field Should be generated, not copied
		$addedWaypoint->route_id = $this->id;
		$addedWaypoint->sequence = $sequence;
		$addedWaypoint->is_origin = false;
		$addedWaypoint->is_destination = false;
		$addedWaypoint->creation_date = time();

		//Saves the operation
		$addedWaypoint->records()->save();

		return $addedWaypoint;
	}

	/**
	 * Redefines a waypoint sequence
	 * Including resetting origin and destination
	 * @return ModelRoute The invoker ModelRoute object
	 */
	public function redefineWaypointSequence() {
		//If this route has no waypoint at all
		if (count($this->Waypoints) == 0)
			return $this;

		$counter = 1;

		foreach ($this->Waypoints as $key => $waypoint) {
			//Defines destination and origin
			if ($counter == 1)
				$waypoint->setAsOrigin();
			else if ($counter == count($this->Waypoints))
				$waypoint->setAsDestination();

			//Next sequence item
			$counter++;
		}

		return $this;
	}

	/**
	 * Updates the route waypoints list
	 * Removing a waypoint
	 * @param mixed $waypointId The waypoint ID
	 * @return ModelRoute
	 */
	public function removeWaypoint($waypointId) {

		$removedWaypoint = new ModelRouteWaypoint();
		$removedWaypoint = $removedWaypoint->getById($waypointId);

		if (!isset($removedWaypoint->id))
			throw new Exception('routeWaypointIdNotFound');

		$removedWaypoint->records()->delete($removedWaypoint->id);

		//Redefines the waypoint sequence of this route
		return $this->redefineWaypointSequence();
	}

	#endregion
}
