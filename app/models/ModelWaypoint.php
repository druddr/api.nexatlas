<?php

/**
 * ModelWaypoint provides query-able waypoint info.
 *
 * ModelWaypoint stands for a class which provides an air nav planning waypoint info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelWaypoint extends Model
{
	#region Properties

    public $primary_key = "id";
    public $table_name = "waypoint";

	public $field_config = [
		'id' => ['type' => Model::type_int],
		'waypoint_type_id' => ['type' => Model::type_int],
		'airac_cycle_id' => ['type' => Model::type_int],
		'ibge_situation_id' => ['type' => Model::type_int],
		'ibge_code' => ['type' => Model::type_int],
		'code' => ['type' => Model::type_varchar],
		'name' => ['type' => Model::type_varchar],
		'state_id' => ['type' => Model::type_int],
		'city_name' => ['type' => Model::type_varchar],
		'icao_code' => ['type' => Model::type_char],
		'iata_code' => ['type' => Model::type_char],
		'latitude' => ['type' => Model::type_double],
		'longitude' => ['type' => Model::type_double],
		'altitude' => ['type' => Model::type_float],
		'frequency' => ['type' => Model::type_varchar],
		'frequency_type' => ['type' => Model::type_varchar],
		'creation_date' => ['type' => Model::type_time]
	];
	public $id;
	public $waypoint_type_id;
	public $airac_cycle_id;
	public $ibge_situation_id;
	public $ibge_code;
	public $code;
	public $name;
	public $state_id;
	public $city_name;
	public $icao_code;
	public $iata_code;
	public $latitude;
	public $longitude;
	public $altitude;
	public $frequency;
	public $frequency_type;
	public $creation_date;
	public $hasOne = [
		'Type' => [
			'model' => 'ModelWaypointType',
			'where' => [
				['id','=','waypoint_type_id']
			],
			'limit' => [1]
		],
		'AiracCycle' => [
			'model' => 'ModelAiracCycle',
			'where' => [
				['id','=','airac_cycle_id']
			],
			'limit' => [1]
		]
	];

	#endregion

	#region Methods

	/**
	 * Gets a ["waypoint"] by its ID
	 */
	public function getById(int $waypointId) {
		return $this->records()->where('id',$waypointId)->getFirstModel();
	}

	/**
	 * Gets a ["waypoint"] by its code
	 */
	public function getByCode($waypointCode) {
		return $this->records()->where('code',$waypointCode)->getFirstModel();
	}

	/**
	 * Gets a ["waypoint"] by its name
	 */
	public function getByName($waypointName) {
		return $this->records()->where('name',$waypointName)->getFirstModel();
	}

	/**
	 * Gets a ["waypoint"] by its ICAO code
	 */
	public function getByIcao($waypointICAO) {
		return $this->records()->where('icao_code',$waypointICAO)->getFirstModel();
	}

	/**
	 * Gets a ["waypoint"] by its IATA code
	 */
	public function getByIata($waypointIATA) {
		return $this->records()->where('iata_code',$waypointIATA)->getFirstModel();
	}

	/**
	 * Gets a ["waypoint"] by its geolocation
	 */
	public function getByGeolocation($waypointLatitude, $waypointLongitude) {
		return $this->records()
			->where('latitude', $waypointLatitude)
			->where('longitude', $waypointLongitude)
			->getFirstModel();
	}

	/**
	 * Creates a Spherical Law of Cosines application for distance calculation from this waypoint
	 *
	 * Builds a SQL approximately like this one:
	 *
	 * //Spherical Law of Cosines Formula
	 * (37 and -122 are the lat and lon of your radius center)
	 *
	 * "SELECT id, ( 3440 * acos( cos( radians( {$latitude} ) ) * cos( radians( latitude ) )
	 * 	* cos( radians( longitude ) - radians( {$longitude} ) ) + sin( radians( {$latitude} ) ) * sin( radians( latitude ) ) ) ) AS distance
	 * FROM myTable
	 * HAVING distance < {$radius}
	 * ORDER BY distance ASC"
	 *
	 * @param mixed $lat
	 * @param mixed $long
	 * @param mixed $radius
	 * @return ActiveRecord
	 */
	public function sphericalLawOfCosines($lat, $long, $radius) {
		$records = $this->records();

		$records->addSelect(
			"waypoint_type_id" .
			", id			 " .
			", code			 " .
			", name			 " .
			", city_name	 " .
			", icao_code	 " .
			", iata_code	 " .
			", latitude		 " .
			", longitude	 " .
			", ( 3440 * acos( cos( radians( {$lat} ) ) * cos( radians( latitude ) )".
			" * cos( radians( longitude ) - radians( {$long} ) ) + sin( radians( {$lat} ) ) * sin( radians( latitude ) ) ) ) AS distance"
			);

		//Adds the having (comparing an alias to an external value)
		$records->having('distance', $radius, "<", false);
		$records->order(['distance'], 'ASC');
		$records->limit(11);

		return $records;
	}

	#endregion
}
