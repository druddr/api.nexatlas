<?php

/**
 * ModelPessoaPerfil short summary.
 *
 * ModelPessoaPerfil description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 * 
 * @version 1.1
 * @author bruno.teixeira.silva
 */
class ModelPessoaPerfilSessao extends Model
{
    public $primary_key = "id";
    public $table_name = "pessoa_perfil_sessao";
	public $field_config = array(
		'id' => array( 'type' => Model::type_int),
		'id_pessoa_perfil' => array('type' => Model::type_int),
		'id_pessoa_perfil_dispositivo'=> array('type' => Model::type_int),		
		'sessao' => array('type' => Model::type_varchar),
		'ativa' => array('type' => Model::type_boolean)
	);
	public $id;
	public $id_pessoa;
	public $id_agenda_padrao;
	public $login;
}
