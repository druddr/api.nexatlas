<?php

/**
 * ModelSistemaModulos (table: sistema_modulos).
 *
 * ModelSistemaModulos description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelSistemaModulos extends Model
{
    public $primary_key = "id";
    public $table_name = "sistema_modulos";
	public $field_config = [
		'id' => ['type' => Model::type_int],
		'nome' => ['type' => Model::type_varchar],
		'descricao' => ['type' => Model::type_text],
		'diretorio' => ['type' => Model::type_text],
		'id_sistema' => ['type' => Model::type_int]
	];
	/**
	 * @var int $id ID
	 */
	public $id;
	/**
     * @var string $nome Module's name
	 */
	public $nome;
	/**
     * @var string $descricao Module's description
     */
	public $descricao;
	/**
     * @var string $diretorio Module's directory
     */
	public $diretorio;
    /**
     * @var int $id_sistema Module's related system ID
     */
	public $id_sistema;
}
