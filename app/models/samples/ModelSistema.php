<?php

/**
 * ModelSistema (table: sistema).
 *
 * ModelSistema description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelSistema extends Model
{
    public $primary_key = "id";
    public $table_name = "sistema";
	public $field_config = [
		'id' => ['type' => Model::type_int],
		'nome' => ['type' => Model::type_varchar],
		'descricao' => ['type' => Model::type_text],
		'tipo' => ['type' => Model::type_varchar],
		'ativo' => ['type' => Model::type_boolean],
		'id_redirect_to' => ['type' => Model::type_int]
	];
	/**
     * @var int $id ID
	 */
	public $id;
	/**
     * @var string $nome Name
	 */
	public $nome;
	/**
     * @var string $descricao Description
	 */
	public $descricao;
	/**
     * @var string $tipo Active or not
	 */
	public $tipo;
	/**
     * @var boolean $ativo System is active or not
     */
	public $ativo;
    /**
     * @var int $id_redirect_to Used when the current system needs to redirect to another system
     */
	public $id_redirect_to;
}