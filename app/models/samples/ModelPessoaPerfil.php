<?php

/**
 * ModelPessoaPerfil short summary.
 *
 * ModelPessoaPerfil description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 * 
 * @version 1.1
 * @author bruno.teixeira.silva
 */
class ModelPessoaPerfil extends Model
{
    public $primary_key = "id";
    public $table_name = "pessoa_perfil";
	public $field_config = array(
		'id' => array( 'type' => Model::type_int),
		'id_pessoa' => array('type' => Model::type_int),
		'id_agenda_padrao'=> array('type' => Model::type_int),		
		'login' => array('type' => Model::type_varchar),
		'password' => array('type' => Model::type_varchar),
		'ultima_atualizacao' => array('type' => Model::type_datetime),
		'bebida_preferida' => ['type' => Model::type_varchar],
		'revista_preferida' => ['type' => Model::type_varchar],
		'usuario_google' => array('type' => Model::type_varchar),
		'usuario_facebook' => array('type' => Model::type_varchar),
		'foto_perfil' => array('type' => Model::type_text),		
		'nome' => array('type' => Model::type_varchar, 'belongsTo'=>array('table'=>'pessoa_fisica','on'=>array('pessoa_fisica.id_pessoa'=>'pessoa_perfil.id_pessoa' ))),
		'sobrenome' => array('type' => Model::type_varchar, 'belongsTo'=>array('table'=>'pessoa_fisica','on'=>array('pessoa_fisica.id_pessoa'=>'pessoa_perfil.id_pessoa' ))),
		'cpf' => array('type' => Model::type_varchar, 'belongsTo'=>array('table'=>'pessoa_fisica','on'=>array('pessoa_fisica.id_pessoa'=>'pessoa_perfil.id_pessoa' ))),
		'data_nascimento' => array('type' => Model::type_varchar, 'belongsTo'=>array('table'=>'pessoa_fisica','on'=>array('pessoa_fisica.id_pessoa'=>'pessoa_perfil.id_pessoa' ))),
	    'login_criado_pela_agenda' => ['type' => Model::type_boolean],
	    'houve_primeiro_acesso' => ['type' => Model::type_boolean],
	    'token_autorizacao' => ['type' => Model::type_varchar]
	);
	public $id;
	public $id_pessoa;
	public $id_agenda_padrao;
	public $login;
	public $password;
	public $ultima_atualizacao;
	public $bebida_preferida;
	public $revista_preferida;
	public $usuario_google;
	public $usuario_facebook;
	public $foto_perfil;	
	public $nome;
	public $sobrenome;
	public $cpf;
	public $data_nascimento;
    public $login_criado_pela_agenda;
    public $houve_primeiro_acesso;
    public $token_autorizacao;

	/**
	 * Summary of getByIdPessoa
	 * @param int $id_pessoa 
	 * @return ModelPessoaPerfil|bool
	 */
	public function getByIdPessoa(/*int*/ $id_pessoa){
        //Checks whether the param is numeric or not
        if(!is_numeric($id_pessoa)) $id_pessoa = intval($id_pessoa);

		return $this->records()->where('id_pessoa',$id_pessoa)->getFirstModel();
	}
	public function getByIdProfissional(/*int*/ $id_profissional){
		//Checks whether the param is numeric or not
        if(!is_numeric($id_profissional)) $id_profissional = intval($id_profissional);
        
        return $this->records()
			->join('profissional',['pessoa_perfil.id_pessoa'=>'profissional.id_pessoa'])
			->where('profissional.id',$id_profissional)
			->getFirstModel();
	}
	public function getByLogin(string $login){
		return $this->records()->where('login',$login)->getFirstModel();
	}

    /**
     * No parameters won't generate a true setting
     * @param boolean $data if null, will generate 'false' value
     * @return ModelPessoaPerfil it's own instance
     */
    public function setPerfilCriadoPelaAgenda($data=null) {
        $this->login_criado_pela_agenda = is_bool($data) ? $data : false;
        $this->houve_primeiro_acesso = false;

		print_r($data);

        return $this;
    }

    public function passwordHash($password) {
        return md5(config::AuthenticationSalt.$password);
    }
}
