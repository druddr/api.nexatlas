<?php

/**
 * ModelSistemaController (table: sistema_controller).
 *
 * ModelSistemaController description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelSistemaController extends Model
{
    public $primary_key = "id";
    public $table_name = "sistema_controller";
	public $field_config = [
		'id' => ['type' => Model::type_int],
		'nome' => ['type' => Model::type_varchar],
		'classe' => ['type' => Model::type_varchar],
		'diretorio' => ['type' => Model::type_varchar],
		'id_sistema' => ['type' => Model::type_int]
	];
	/**
	 * @var int $id ID
	 */
	public $id;
	/**
     * @var string $nome Name of the controller
	 */
	public $nome;
	/**
     * @var string $classe Class' name
	 */
	public $classe;
	/**
     * @var string $diretorio Directory of location
	 */
	public $diretorio;
    /**
     * @var int $id_sistema Controller's related system ID
     */
	public $id_sistema;
}
