<?php

class ModelPessoaEndereco extends Model
{

    public $primary_key = "id";
    public $table_name = "pessoa_endereco";
	public $field_config = [
		'id' => [  'type' => Model::type_int ],
		'id_pessoa' => [ 'type' => Model::type_int ],
		'logradouro' => [ 'type' => Model::type_int ],
		'bairro' => [ 'type' => Model::type_varchar ],
		'id_cidade' => [ 'type' => Model::type_int ],
	];
	public $id;
    public $id_pessoa;
	public $logradour;
	public $bairro;
	public $id_cidade;
}