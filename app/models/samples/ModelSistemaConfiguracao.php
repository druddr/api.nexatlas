<?php

/**
 * ModelSistemaConfiguracao illustrates a (table: sistema_configuracao).
 *
 * ModelSistemaConfiguracao is used for data travelling from a point to the other inside the system.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelSistemaConfiguracao extends Model
{
    #region Properties

    public $primary_key = "chave";
    public $primary_key_is_string = true;
    public $table_name = "sistema_configuracoes";
	public $field_config = [
		'chave' => ['type' => Model::type_varchar],
		'valor' => ['type' => Model::type_text],
		'data_criacao' => ['type' => Model::type_datetime],
		'ativo' => ['type' => Model::type_boolean],
		'fixa' => ['type' => Model::type_boolean],
		'data_ultima_alteracao' => ['type' => Model::type_datetime],
		'id_sistema' => ['type' => Model::type_int],
		'id_empresa_filial' => ['type' => Model::type_int]
	];
	/**
	 * @var string $chave Unique configuration identifier
	 */
	public $chave;
	/**
     * @var string $valor Setting value
	 */
	public $valor;
	/**
     * @var datetime $data_criacao Creation date
	 */
	public $data_criacao;
	/**
     * @var boolean $ativo Active or not
	 */
	public $ativo;
	/**
     * @var boolean $fixa Fixed config or not
     */
	public $fixa;
    /**
     * @var datetime $data_ultima_alteracao Last change date
     */
	public $data_ultima_alteracao;
    /**
     * @var int $id_sistema Config's related system ID
     */
	public $id_sistema;
    /**
     * @var int $id_empresa_filial Config's related branch ID
     */
	public $id_empresa_filial;

    #endregion

    #region Methods

    /**
     * Gets a configuration setting by it's (string) key
     * @param string $key
     */
    protected static function get($key) {

        if (empty($key) || is_null($key)) {
            throw new LogicException('This method requires a parameter in order to work properly');
        }

        //Creates the query object
		$config = 
            (new ModelSistemaConfiguracao())
                ->records()
			    ->where('chave', $key, '=', true)
			    ->getFirstModel();

		return $config;
    }

    #endregion
}