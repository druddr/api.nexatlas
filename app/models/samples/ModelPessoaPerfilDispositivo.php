<?php

/**
 * ModelPessoaPerfilDispositivo short summary.
 *
 * ModelPessoaPerfilDispositivo description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelPessoaPerfilDispositivo extends Model
{
    public $primary_key = "id";
    public $table_name = "pessoa_perfil_dispositivo";
	public $field_config = array(
		'id' => array( 'type' => Model::type_int),
		'id_pessoa_perfil' => array('type' => Model::type_int),
		'id_dispositivo' => array('type' => Model::type_int),
		'modelo' => array('type' => Model::type_varchar),
		'marca' => array('type' => Model::type_varchar),
		'plataforma' => array('type' => Model::type_varchar),
		'versao' => array('type' => Model::type_varchar),
		'autorizado'=> array('type' => Model::type_boolean)
	);
	public $id;
	public $id_pessoa_perfil;
	public $id_dispositivo;
	public $modelo;
	public $marca;
	public $plataforma;
	public $versao;
	public $autorizado;
}	
	
	
	
	
	
	