<?php

/**
 * ModelPessoaContato (table: pessoa_contatos).
 *
 * ModelPessoaContato description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 *
 * @version 1.1
 * @author bruno.teixeira.silva
 */
class ModelPessoaContato extends Model
{
	#region Properties

    public $primary_key = "id";
    public $table_name = "pessoa_contato";
	public $field_config = [
		'id' => [  'type' => Model::type_int ],
		'id_pessoa' => [ 'type' => Model::type_int ],
		'id_empresa' => [ 'type' => Model::type_int ],
		'tipo_contato' => [ 'type' => Model::type_varchar ],
		'contato' => [ 'type' => Model::type_varchar ],
		'data_criacao' => [ 'type' => Model::type_datetime ],
		'publico' => [ 'type' => Model::type_boolean ]
	];

    /**
     * tipo_contato_email = E
     */
	const tipo_contato_email = "E";
    /**
     * tipo_contato_telefone = T
     */
	const tipo_contato_telefone = "T";

	public $id;
	public $id_pessoa;
	public $id_empresa;
	public $tipo_contato;
	public $contato;
	public $data_criacao;
	public $publico;

	#endregion

	#region Methods

    /**
     * Creates many items in the database
     * @param mixed $data_set the data set containing multiple items to be created
     * @return array of created objects
     */
    public function saveMany($data_set) {
        //Gets the records for bulk operations
        $records = $this->records();

        //Keeps the created objects' collection
        $return = [];

        //Iterates thru each object in the array for saving
        foreach ($data_set as $key => $contact) {
            //Executes the saving operation
            $this->fill($contact);

			//If this is the insertion of a new contact
			if (!$contact->id) {
				//Gets the current date plus the owner company ID
				$this->data_criacao = date('Y-m-d H:i:s');
				$this->id_empresa = intval(AuthorizedSession::get()->companies[0]);
			}

			//Saves the data
            $this->save();

            //Adds the item to the return variable
            $return[] = $this;

            //Sets the primary key null for reusage
            $records->reuse();
        }

        return $return;
    }

	///**
	// * Updates a single item in the database
	// * @param mixed $data the standard object with the properties to be updated
	// * @return mixed the update object or false, in case the operation has failed
	// */
	//public function update($data) {
	//    //Gets the records for the update operation
	//    $records = $this->records();

	//    //Queries for the item to apply the updates
	//    $contact = $records->get($data->id)->getFirstModel();

	//    //The contact wasn't found, get outta here
	//    if ($contact == null) return false;

	//    //Manipulates the object and applies the changes
	//    $contact->fill($data);
	//    $contact->save();

	//    return $contact;
	//}

    /**
     * Seeks a person contacts by it's system ID
     * @param mixed $id_pessoa Owner ID pessoa
     * @return array of stdClass
     */
    public function getByIdPessoa($id_pessoa) {
        $return = [];

        //Gets the records for the search
        $records = $this->records();

        //Executes the search
        $return =
			$records
				->where('id_pessoa', $id_pessoa, '=', false)
				->whereIn('id_empresa', AuthorizedSession::get()->companies)
				->order('data_criacao', 'DESC')
				->get();

        //Returns or an empty array or the standard list
        return count($return) == 0 ? [] : $return->toStandardArray();
    }

	#endregion
}