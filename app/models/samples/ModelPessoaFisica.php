<?php

/**
 * ModelPessoaFisica short summary.
 *
 * ModelPessoaFisica description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelPessoaFisica extends Model
{
    public $primary_key = "id_pessoa";
    public $table_name = "pessoa_fisica";

	public $field_config = array(
		'id_pessoa' => array( 'type' => Model::type_int),
		'id_pessoa_perfil' => array('type' => Model::type_varchar, 'belongsTo'=>array('field'=>'id','table'=>'pessoa_perfil','on'=>array('pessoa_perfil.id_pessoa' => 'pessoa_fisica.id_pessoa'))),
		'cpf' =>array('type' => Model::type_varchar),
		'nome' =>array('type' => Model::type_varchar),
		'sobrenome' =>array('type' => Model::type_varchar),
		'sexo' =>array('type' => Model::type_varchar),
		'data_nascimento' =>array('type' => Model::type_varchar),
		'login' => array('type' => Model::type_varchar, 'belongsTo'=>array('table'=>'pessoa_perfil','on'=>array('pessoa_perfil.id_pessoa' => 'pessoa_fisica.id_pessoa'))),
	);
	public $id_pessoa;
	public $id_pessoa_perfil;
	public $cpf;
	public $nome;
	public $sobrenome;
	public $sexo;
	public $data_nascimento;
	public $login;

	/**
	 * Queries a physical person by it's given CPF (national person code)
	 * @param mixed $cpf
	 * @return ModelPessoaFisica|bool|stdClass|Model
	 */
	public function getByCPF($cpf){
		if(empty($cpf)) return false;

		//Builds the query to find a physical person
		$physicalPerson = $this->records()->like('cpf', '%' . $cpf . '%')->getFirstModel();

		return $physicalPerson;
	}
	public function getByIdPessoa($id_pessoa){
		return $this->records()->where( 'id_pessoa', $id_pessoa )->getFirstModel();
	}
}
