<?php

/**
 * ModelSistemaModuloControllers (table: sistema_modulo_controllers).
 *
 * ModelSistemaModuloControllers description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelSistemaModuloControllers extends Model
{
    public $primary_key = "id";
    public $table_name = "sistema_modulo_controllers";
	public $field_config = [
		'id' => ['type' => Model::type_int],
		'id_modulo' => ['type' => Model::type_int],
		'id_controller' => ['type' => Model::type_int]
	];
	/**
	 * @var int $id ID
	 */
	public $id;
	/**
     * @var int $id_modulo Module's ID
	 */
	public $id_modulo;
	/**
     * @var int $id_controller Controller's ID
     */
	public $id_controller;
}
