<?php

/**
 * ModelSistemaControllerActions (table: sistema_controller_actions).
 *
 * ModelSistemaControllerActions description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelSistemaControllerActions extends Model
{
    public $primary_key = "id";
    public $table_name = "sistema_controller_actions";
	public $field_config = [
		'id' => ['type' => Model::type_int],
		'id_controller' => ['type' => Model::type_int],
		'nome' => ['type' => Model::type_varchar],
		'metodo' => ['type' => Model::type_varchar],
		'quantidade_parametros_obrigatorios' => ['type' => Model::type_tinyint],
		'id_sistema' => ['type' => Model::type_int]
	];
	/**
	 * @var int $id ID
	 */
	public $id;
	/**
     * @var int $id_controller Controller's ID
	 */
	public $id_controller;
	/**
     * @var string $nome Name of the action
	 */
	public $nome;
	/**
     * @var string $metodo Method's name
	 */
	public $metodo;
	/**
     * @var int $quantidade_parametros_obrigatorios Quantity of compulsory parameters
	 */
	public $quantidade_parametros_obrigatorios;
    /**
     * @var int $id_sistema Action's related system ID
     */
	public $id_sistema;

	public function getAllActionsRoutes(){
		
	}
}
