<?php

/**
 * ModelRouteWaypoint provides route waypoint info.
 *
 * ModelRouteWaypoint stands for a class which provides an air nav route waypoint info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ModelRouteWaypoint extends Model implements CustomExtractable
{
	#region Properties

    public $primary_key = "id";
    public $table_name = "route_waypoint";

	public $field_config = [
		'id' => ['type' => Model::type_int],
		'route_id' => ['type' => Model::type_int],
		'sequence' => ['type' => Model::type_int],
		'waypoint_type_id' => ['type' => Model::type_int],
		'airac_cycle_id' => ['type' => Model::type_char],
		'ibge_situation_id' => ['type' => Model::type_int],
		'ibge_code' => ['type' => Model::type_int],
		'code' => ['type' => Model::type_varchar],
		'name' => ['type' => Model::type_varchar],
		'state_id' => ['type' => Model::type_int],
		'city_name' => ['type' => Model::type_varchar],
		'icao_code' => ['type' => Model::type_char],
		'iata_code' => ['type' => Model::type_char],
		'latitude' => ['type' => Model::type_double],
		'longitude' => ['type' => Model::type_double],
		'altitude' => ['type' => Model::type_float],
		'is_origin' => ['type' => Model::type_boolean],
		'is_destination' => ['type' => Model::type_boolean],
		'is_change_over' => ['type' => Model::type_boolean],
		'frequency' => ['type' => Model::type_varchar],
		'frequency_type' => ['type' => Model::type_varchar],
		'creation_date' => ['type' => Model::type_time]
	];
	public $id;
	public $route_id;
	public $sequence;
	public $waypoint_type_id;
	public $airac_cycle_id;
	public $ibge_situation_id;
	public $ibge_code;
	public $code;
	public $name;
	public $state_id;
	public $city_name;
	public $icao_code;
	public $iata_code;
	public $latitude;
	public $longitude;
	public $altitude;
	public $is_origin;
	public $is_destination;
	public $is_change_over;
	public $frequency;
	public $frequency_type;
	public $creation_date;
	public $hasOne = [
		'Route' => [
			'model' => 'ModelRoute',
			'where' => [
				['route_id','=','id']
			],
			'limit' => [1]
		],
		'Type' => [
			'model' => 'ModelWaypointType',
			'where' => [
				['id','=','waypoint_type_id']
			],
			'limit' => [1]
		],
		'AiracCycle' => [
			'model' => 'ModelAiracCycle',
			'where' => [
				['airac_cycle_id','=','id']
			],
			'limit' => [1]
		]
	];

	#endregion

	#region Methods

	/**
	 * Applies value corrections
	 *
	 * @return StdClass
	 */
	public function extract() {
		//Corrects double values and boolean values
		$this->latitude = (double)(doubleval($this->latitude));
		$this->longitude = (double)(doubleval($this->longitude));
		$this->is_origin = (bool)(intval($this->is_origin) == 1);
		$this->is_destination = (bool)(intval($this->is_destination) == 1);
		$this->is_change_over = (bool)(intval($this->is_change_over) == 1);

		return parent::extract();
	}

	/**
	 * Gets a ["route_waypoint"] by its ID
	 */
	public function getById(int $routeWaypointId) {
		return $this->records()->where('id',$routeWaypointId)->getFirstModel();
	}

	/**
	 * Gets a ["route_waypoint"] by its code
	 */
	public function getByCode($waypointCode) {
		return $this->records()->where('code',$waypointCode)->getFirstModel();
	}

	/**
	 * Gets a ["route_waypoint"] by its name
	 */
	public function getByName($waypointName) {
		return $this->records()->where('name',$waypointName)->getFirstModel();
	}

	/**
	 * Gets a ["route_waypoint"] collection by their city_name
	 * @return ModelRouteWaypoint[]
	 */
	public function getByCityName($waypointCityName) {
		return $this->records()->where('city_name',$waypointCityName)->toModelArray();
	}

	/**
	 * Gets a ["route_waypoint"] by its ICAO code
	 */
	public function getByIcao($waypointICAO) {
		return $this->records()->where('icao_code',$waypointICAO)->getFirstModel();
	}

	/**
	 * Gets a ["route_waypoint"] by its IATA code
	 */
	public function getByIata($waypointIATA) {
		return $this->records()->where('iata_code',$waypointIATA)->getFirstModel();
	}

	/**
	 * Gets a ["route_waypoint"] by its geolocation
	 */
	public function getByGeolocation($waypointLatitude, $waypointLongitude) {
		return $this->records()
			->where('latitude', $waypointLatitude, '=', false)
			->where('longitude', $waypointLongitude, '=', false)
			->getFirstModel();
	}

	/**
	 * Updates the current route waypoint setting it as "origin"
	 * @return ModelRouteWaypoint
	 */
	public function setAsOrigin() {

		//Crates a route waypoint to the user route
		$this->sequence = 1;
		$this->is_origin = true;
		$this->is_destination = false;
		$this->save();

		return $this;
	}

	/**
	 * Updates the current route waypoint setting it as "destination"
	 * @return ModelRouteWaypoint
	 */
	public function setAsDestination() {

		//Crates a route waypoint to the user route
		$this->is_origin = false;
		$this->is_destination = true;
		$this->save();

		return $this;
	}

	#endregion
}
