<?php
/**
 * User controller class.
 *
 * User controller handles main user functions such as:
 * query
 * get
 * create
 * authenticate
 * googleAuthenticate
 * facebookAuthenticate
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class User extends Controller
{
	/**
	 * @ajax
	 */
	public function get($id=null){
		$user = $this->models->User;
		$result = $user->records()->get($id,null)->limit(1)->toStandardArray();
		return reset($result);
	}

    /**
	 * @ajax
	 */
	public function query($query=null){
		$result = $this->model->User->records()->get(null,$query)->toStandardArray();
		return array_merge($result,$result);
	}

    /**
	 * Creates user based on form provided info
	 * @authorized
	 * @ajax
	 */
    public function create($data){
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		//Validates password length
		if (strlen($data->password) < 4 || strlen($data->password) > 16)
			throw new Exception('invalidPasswordFormat');

		//Checks whether the user can be found by email
		$user = $this->models->User->records()
			->where('email', $data->email)
			->getFirstModel();

		//Checks whether the email is already used
		if ($user->id) throw new Exception('emailAlreadyUsed');

		//Begins a transactioned process
		$userRecord = $this->models->User->records();
		$userRecord->beginTransaction();

		try {

			//Fills the user object
			$user = $this->models->User->fill($data);

			//Gets a salt for the user and hashes the password
			$user->salt = $user->generateSalt();
			$user->read_terms = $data->readTerms;
			//User accounts enter the system inactive because they need to fill in profile info
			$user->active = false;
			$user->email_confirmation_token = md5($user->salt);
			$user->password = $user->passwordHash($user->salt, $user->password);
			$user->confirmed_email = false;
			//$user->facebook_connected = false;
			//$user->google_connected = false;
			//Auto-specified by DB
			$user->creation_date = time(); //time();

			//Saves the user and gives it back
			$user->records()->save();

			//TODO: User is not authenticated by being created anymore
			//This object is used for front-end mapping of info
			//Registers the entrance of the user
			//$this->setSession($user, OAuthDataProvider::AppMode);
		}
		catch (Exception $exception) {
			//Cancels every created register
			$userRecord->rollBack();

			//Rethrows the exception
			throw $exception;
		}

		//Does the commit of the data to the DB
		$userRecord->commit();

        return [
			'user' => $this->models->User->extract(),
			'userProfile' => new stdClass(),
			'remember' => true
			];
	}

	/**
	 * Internal: Creates user based on OAuth provided info
	 * @param $data stdClass with user data to be saved
	 * @param $mode string Containing the type of OAuth service used
	 * @return ModelUser The created user with Facebook/Google (OAuth) provided info
	 */
	public function createBasedOnOAuth($data, $mode) {

		//Begins a transactioned process
		$userRecord = $this->models->User->records();
		$userRecord->beginTransaction();

		try {

			//Creates an user ID to this email
			$user = $this->models->User->fill($data);

			//Switches between different OAuth methods
			switch ($mode) {
				case OAuthDataProvider::FacebookMode:
					$user->facebook_connected = true;
					break;

				case OAuthDataProvider::GoogleMode:
					$user->google_connected = true;
					break;

				default:
					//Invalid OAuth method breaks the process
					throw new Exception("invalidProviderMode");
				//break;
			}

			//this information if useful for sending the user a confirmation email
			$user->salt = ModelUser::generateSalt();
			//Entering this way made the user jump the read terms process
			$user->read_terms = false;
			$user->confirmed_email = true;
			$user->confirmed_email_on = time();
			$user->email_confirmation_token = md5($user->salt);
			$user->creation_date = date("Y-m-d H:i:s", time()); //time();
			//User accounts enter the system active because OAuth provided info
			$user->active = true;
			$user->active_on = time();
			$user->save();

			//Creates a profile completing with information provided by facebook
			$userProfile = $this->models->UserProfile->fill($data);
			//User profile enter the system incomplete because
			//OAuth provided info is still lacking specific info
			$userProfile->complete = false;
			$userProfile->user_id = $user->id;
			$userProfile->creation_date = time();
			$userProfile->save();
		}
		catch (Exception $exception) {
			//Cancels every created register
			$userRecord->rollBack();

			//Rethrows the exception
			throw $exception;
		}

		//Does the commit of the data to the DB
		$userRecord->commit();

		return $user;
	}

	/**
	 * Confirms an user email
	 * @param mixed $data
	 * @authorized
	 * @ajax
	 */
	public function confirmEmail($data) {
		if(!isset($data) || !is_object($data) || !isset($data->token))
            throw new Exception('dataNotAnObject');

		//Tries to find the user
		$user = $this->models->User->getByEmailConfirmationToken($data->token);

		//Invalid mail confirmation token
		if(!isset($user->id) || (isset($data->userId) && !($user->id == $data->userId)))
            throw new Exception('invalidEmailConfirmationToken');

		//If the email was already confirmed
		if ($user->confirmed_email)
			throw new Exception("emailAlreadyConfirmed");

		//Executes its saving
		$user->confirmEmail();

		////TODO: REMOVED FOR LAUNCH
		////Sends an email to notify the user
		//$mailSent =
		//    !config::$AppSendsEmail ||
		//    CustomUtilities::sendEmail(
		//		  MailProvider::SendGrid
		//        implode(' ', [$user->Profile->name, $user->Profile->surname]),
		//        $user->email,
		//        ModelUser::UserConfirmedMailSubject,
		//        //HTML version
		//        "Olá <b>{$user->Profile->name}</b>, <br><br>Seu email da conta App NexAtlas foi confirmado. Obrigado.",
		//        //Raw text version
		//        "Olá {$user->Profile->name}, seu email da conta App NexAtlas foi confirmado. Obrigado.");

		return [
			'userId' => $user->id,
			'status' => 200
			];
	}

	/**
	 * Authenticates an user based on email, password
	 * @param $query stdClass containing the info to use as authentication
	 * @authorized
	 * @ajax
	 */
	public function authenticate($query){
		if(!isset($query) || !is_object($query))
            throw new Exception('dataNotAnObject');

		//Active record instance
		$record = $this->models->User->records();

		//Tries to get the user password salt
		$user = $record
			->where('email', $query->email)
			->getFirstModel();

		//User was not found by email
		if (!$user->id || !($user->password === $user->passwordHash($user->salt, $query->password)))
			throw new Exception('invalidUserOrPassword');

		//User did not complete profile creation
		if (!$user->active)
			throw new Exception('userAccountInactive');

		//User did not complete profile creation
		if (!$user->Profile->id || !$user->Profile->complete)
			throw new Exception('userProfileIncomplete');

		//Kills each previous open authentication from this user
		if (count($user->Authentications) > 0)
			$user->Authentications->each(function(ModelAuthentication $item, $k){ $item->kill(); });

		//This object is used for front-end mapping of info
		//Registers the entrance of the user
		$auth = $this->models->Authentication->setSession($user, OAuthDataProvider::AppMode);

		return [
			'user' => CurrentUser::get()->user->extract(),
			'userProfile' => CurrentUser::get()->userProfile->extract(),
			'remember' => $query->rememberMe,
			'token' => $auth->token
		];
	}

	/**
	 * Authenticates the user using Facebook data
	 * @param mixed $data
	 * @authorized
	 * @ajax
	 */
	public function facebookAuthenticate($data) {
		if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$user = $this->models->User->getByEmail($data->email);

		//TODO: Add step/phase validation of facebook connection
		//Using a PHP API

		//First user access through Facebook
		if (!$user->id) {
			//Creates an user ID to this email
			$user = $this->createBasedOnOAuth($data, OAuthDataProvider::FacebookMode);
		}

		//User did not complete profile creation
		if (!$user->active)
			return [ 'user' => $user->extract(), 'remember' => true ];

		//Used for profile form completion when
		//User did not complete profile creation
		if (!$user->Profile->id || !$user->Profile->complete)
			return [ 'user' => $user->extract(), 'userProfile' => $user->Profile->extract(), 'remember' => true ];

		//This object is used for front-end mapping of info
		$auth = $this->models->Authentication->setSession($user, OAuthDataProvider::FacebookMode);

		//We forcedly remember the user, here
		return [
			'user' => CurrentUser::get()->user->extract(),
			'userProfile' => CurrentUser::get()->userProfile->extract(),
			'remember' => true,
			'token' => $auth->token
			];
	}

	/**
	 * Authenticates the user with Google data
	 * @param mixed $data
	 * @authorized
	 * @ajax
	 */
	public function googleAuthenticate($data) {
		if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$user = $this->models->User->getByEmail($data->email);

		//TODO: Add step/phase validation of Google connection
		//Using a PHP API

		//First user access through Google
		if (!$user->id) {
			//Creates an user ID to this email
			$user = $this->createBasedOnOAuth($data, OAuthDataProvider::GoogleMode);
		}

		//User did not complete profile creation
		if (!$user->active)
			return [ 'user' => $user->extract(), 'remember' => true ];

		//Used for profile form completion when
		//User did not complete profile creation
		if (!$user->Profile->id || !$user->Profile->complete)
			return [ 'user' => $user->extract(), 'userProfile' => $user->Profile->extract(), 'remember' => true ];

		//This object is used for front-end mapping of info
		$auth = $this->models->Authentication->setSession($user, OAuthDataProvider::GoogleMode);

		//We forcedly remember the user, here
		return [
			'user' => CurrentUser::get()->user->toStandard(),
			'userProfile' => CurrentUser::get()->userProfile->toStandard(),
			'remember' => true,
			'token' => $auth->token
			];
	}

	/**
	 * Authorizes an user through a route or not
	 * @authorized
	 * @ajax
	 */
	public function authorize() {
		$headers = getallheaders();

		//// $auth = $headers['Authorization'];
		//error_log($headers, 3, '/home/gomes/dev/workspaces/nexatlas.com/api.nexatlas/log.log');

		return "HW";
	}

	/**
	 * Renews an authentication token based on user_id and token death time
	 * @param mixed $data
	 * @authorized
	 * @ajax
	 */
	public function renewActivity($data) {
		$newAuth = null;

		//Checks the validity of the data
        if (!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		if (!isset($data->t)) throw new Exception('authenticationTokenInvalid');

		$auth = $this->models->Authentication->getByToken($data->t);

		//The token have no association with auth
		if (!$auth->id || $auth->user_id != $data->user_id) throw new Exception('authenticationTokenInvalid');

		$records = $auth->records();
		$records->beginTransaction();

		try {
			//Kills the old auth object
			$auth->kill(strtotime(date('Y-m-d H:i:s', $data->time)));

			//Creates a new authentication object
			$newAuth = new ModelAuthentication();
			$newAuth = $newAuth->register($auth->User, $auth->mode);

		} catch (Exception $exception) {
			$records->rollBack();
			throw $exception;
		}

		//Finishes the process
		$records->commit();

		return [
			'token' => $newAuth->token
			];
	}

	/**
	 * Requests an user password change by executing a few tasks
	 * @param mixed $data containing user email
	 * @authorized
	 * @ajax
	 */
	public function lostPasswordRequest($data) {
		$mailSent = [ 'status' => 200 ];

		if (!$data || !isset($data->email))
			throw new Exception('invalidPasswordRequest');

		//Tries to find the user
		$user = $this->models->User->records()
			->where('email', $data->email)
			->getFirstModel();

		//No user found, should stop the process
		if (!$user->id) throw new Exception('invalidUser');

		//Catch a password request object for transaction purposes
		$passwordRequestRecords = $this->models->UserPasswordRequest->records();
		$passwordRequestRecords->beginTransaction();

		try {
			//$previousRequests = $user->PasswordRequests;

			//Any previous password request made
			if (count($user->PasswordRequests) > 0) {

				//Kills each of them requests before creating a new one
				$user->PasswordRequests->each(function($item, $k) {
					$item->invalidate();
				});
			}

			//Generates a new password switch request
			$passwordRequest = $this->models->UserPasswordRequest;
			$passwordRequest->token = md5($user->generateSalt());
			$passwordRequest->is_valid = 1;
			$passwordRequest->user_id = $user->id;
			$passwordRequest->creation_date = time();

			//Saves the request
			$passwordRequest->insert();

			//Now if the mail should be sent
			if (config::$AppSendsEmail) {
				//$userName = $user->Profile->name;
				//$userSurname = $user->Profile->surname;

				//Sends an email to notify the user
				$mailSent = $user->sendPasswordRequestEmail($passwordRequest);
			}
		}
		catch (Exception $exception) {
			//Cancels the creation of a password request
			$passwordRequestRecords->rollBack();

			//Rethrows the error for front-end
			throw $exception;
		}

		//Confirms the process
		$passwordRequestRecords->commit();

		return $mailSent;
	}

	/**
	 * Checks whether the current request was found and is valid
	 * @param mixed $data object containing the pass request id
	 * @return string[] object with redirect http code and url or error
	 * @authorized
	 * @ajax
	 */
	public function checkPasswordRequest($data) {
		if (!$data || !isset($data->token))
			throw new Exception('invalidPasswordRequest');

		//Finding the password request by its token
		$passwordRequest =
			$this->models->UserPasswordRequest->getByToken($data->token);

		//Checking whether the request is valid
		if (!$passwordRequest->user_id || !$passwordRequest->isValid()) {
			//Applies the invalidation to this password request
			$passwordRequest->invalidate();
			throw new Exception('invalidPasswordRequest');
		}

		//Notifies redirect status
		return [ 'status' => 302, 'url' => Config::$PasswordResetAppUrl . "t=" . $passwordRequest->token ];
	}

	/**
	 * Resets a password
	 * @param mixed $data object containing the new password and the password request ID
	 * @return string[] object with redirect http code and url or error
	 * @authorized
	 * @ajax
	 */
	public function resetPassword($data) {
		if (!$data || !is_object($data))
            throw new Exception('dataNotAnObject');

		if (!$data->password === $data->passwordConfirmation)
			throw new Exception('passwordConfirmationMismatch');

		$user = $this->models->User->records()
			->select(['user.*'])
			->where('user_password_request.token', $data->token)
			->join('user_password_request', [ 'user_password_request.user_id' => 'user.id' ])
			->getFirstModel();

		if (!$user->id) throw new Exception('invalidPasswordRequest');

		//Hashes a new password for the user and saves it
		$user->salt = $user->generateSalt();
		$user->password = $user->passwordHash($user->salt, $data->password);
		$user->records()->save();

		//Kills the password request, cannot be user anymore
		$passwordRequest = $this->models->UserPasswordRequest->getByToken($data->token);

		//Invalidates the request by burning it (it's used!)
		$passwordRequest->burn();

		//TODO: Cancelled by now
		////Sends an email to notify the user
		//$mailSent =
		//    !config::$AppSendsEmail ||
		//    CustomUtilities::sendEmail(
		//		  MailProvider::SendGrid
		//        implode(' ', [$user->Profile->name, $user->Profile->surname]),
		//        $user->email,
		//        ModelUser::PasswordUpdatedMailSubject,
		//        //HTML version
		//        "Ol� <b>{$user->Profile->name}</b>, sua senha foi alterada com sucesso.",
		//        //Raw text version
		//        "Ol� {$user->Profile->name}, sua senha foi alterada com sucesso.");

		return [
			'status' => 200
			];
	}

	/**
	 * Logs the user out of the server
	 * @param mixed $data the user data to have it logged out
	 * @authorized
	 * @ajax
	 */
	public function logout($data) {
		if (!$data || !is_object($data))
			throw new Exception('dataNotAnObject');

		//Tries to find the latest user authentication object
		//TODO: Add device identification here
		$authentication = $this->models->Authentication->records()
			//->where('user_id', $data->id)
			->where('token', $data->token)
			->order('id', 'DESC')
			->getFirstModel();

		if (!$authentication->id) throw new Exception('authenticationObjectInvalid');

		//Invalidates the authentication object by literally killing it
		$authentication->kill();

		return [ "status" => 200 ];
	}

	/**
	* Send contact mail from this user
	* @authorized
	* @ajax
	*/
	public function contact($data){
		if (!$data || !is_object($data)){
			throw new Exception('dataNotAnObject');
		}

		if (config::$AppSendsEmail) {
			$emailTimestamp = (new DateTime($data->timestamp))->format('d/m/Y H:i');

			$mailSubject = "{$data->motive} NexAtlas";
			//Template for the app pass request
			$template = new Template('app/designs/emails/pt-br/ContactForm.html');
			$template->name = $data->name;
			$template->email = $data->email;
			$template->phone = $data->phone;
			$template->motive = $data->motive;
			$template->timestamp = $emailTimestamp;
			$template->message = $data->message;

			//Raw text version
			$rawTextTemplate =
				"{$data->motive} NexAtlas\n" .
				"{$data->message}\n" .
				"Nome: {$data->name}\n" .
				"Email: {$data->email}\n" .
				(isset($data->phone)) ? "Telefone fornecido: {$data->phone} \n" : "".
				"Data: {$emailTimestamp} \n";

			// Renders the HTML template version
			$renderedTemplate = $template->render();

			// Sends the email
			$mailSent =
				CustomUtilities::sendEmail(
					MailProvider::SendGrid,
					Config::SmtpHeaderFromUser,
					Config::SmtpHeaderFromMail,
					$mailSubject,
					//HTML version
					$renderedTemplate,
					//Raw text version
					$rawTextTemplate,
					$data->name,
					$data->email
					);

			//Checking the returning mail sent info
			if ($mailSent['status'] == 200)
				return [ "status" => 200 ];
			else
				return [ "status" => 500 , "error" => true, "message" => $mailSent['error']];
		}
		else {
			return [ "status" => 500, "error" => true, "message" => "appSendsEmailDisabled" ];
		}
	}
}
