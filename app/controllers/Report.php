<?php
/*
 * Report Controller
 *
 * Report controller handles main report functions such as:
 * - rendering a available reports page
 * - converting an array to csv?
 * - rendering reports like:
 * 	- total_users
 * 	- new_users
 */

class Report extends Controller{
    // a list with all available reports to be rendered
    private $available_reports = [
       'total_users_daily'=> ["link"=>"total_users_daily","name"=>"Total users (daily)"],
       'total_users_weekly'=> ["link"=>"total_users_weekly","name"=>"Total users (weekly)"],
       'total_users_monthly'=> ["link"=>"total_users_monthly","name"=>"Total users (monthly)"],
       'new_users_daily'=> ["link"=>"new_users_daily","name"=>"New users (daily)"],
       'new_users_weekly'=> ["link"=>"new_users_weekly","name"=>"New users (weekly)"],
       'new_users_monthly'=> ["link"=>"new_users_monthly","name"=>"New users (monthly)"],
       'account_migration_factor_daily'=> ["link"=>"account_migration_factor_daily","name"=>"Account migration factor (daily)"],
       'authentications_daily'=> ["link"=>"authentications_daily","name"=>"Accesses count (daily)"],
       'authentications_weekly'=> ["link"=>"authentications_weekly","name"=>"Accesses count (weekly)"],
       'authentications_monthly'=> ["link"=>"authentications_monthly","name"=>"Accesses count (monthly)"],
       'users_which_authenticated_daily'=> ["link"=>"users_which_authenticated_daily","name"=>"Single user accesses count (daily)"],
       'users_which_authenticated_weekly'=> ["link"=>"users_which_authenticated_weekly","name"=>"Single user accesses count (weekly)"],
       'users_which_authenticated_monthly'=> ["link"=>"users_which_authenticated_monthly","name"=>"Single user accesses count (monthly)"],
       'user_access_frequency_grouped_by_user'=> ["link"=>"user_access_frequency_grouped_by_user","name"=>"Total user accesses grouped by frequency"],
       'origin_aerodrome_usage_count'=> ["link"=>"origin_aerodrome_usage_count","name"=>"Per aerodrome usage as origin (top 30)"],
       'destination_aerodrome_usage_count'=> ["link"=>"destination_aerodrome_usage_count","name"=>"Per aerodrome usage as destination (top 30)"],
    ];

    /**
     * Summary of index
     * @param mixed $query
     */
    public function index($query=null){
        // check if is requesting a report
        if ($query !== null && array_key_exists('report_type', $query)) {
            // generates the name timestamp
            $date = new DateTime();
            $datestamp = $date->format('Y-m-d');
            // get the report type by GET query
            $report_type = $query->report_type;

            // get the report method in this controller
            $callMethodName = "generate_".$report_type."_report";

            if (is_callable(array($this, $callMethodName)) && method_exists($this, $callMethodName)) {
                $this->return_csv($this->$callMethodName(), $datestamp."_".$report_type);
            } else {
                echo "REPORT NOT FOUND";
            }
        } else {
            // renders an available reports page
            echo $this->render_html_header();
            foreach ($this->available_reports as $report) {
                echo "<a href=\"report?report_type=$report[link]\">$report[name]</a><br/>";
            }
            echo $this->render_html_footer();
        }
    }

    private function render_html_header(){
        return <<<HTML
			<!DOCTYPE html>
			<html>

			<head>
			<meta charset="utf-8">
			<title>Available Reports</title>
			</head>
			<style>body{margin: 0; padding: 0; line-height: 100%; text-align: center;} a{line-height: 160%; font-size: 1.6em;}</style>
			<body>
			<h1>Available Reports</h1>
HTML;
    }
    private function render_html_footer(){
        return <<<HTML
			</body>
			</html>
HTML;
    }

    /**
	 * method to set the csv headers and return the file string
	 * @param mixed $data
     * @param mixed $report_name
     */
    private function return_csv($data, $report_name){
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$report_name.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $data;
    }

	/**
	 * Generates a CSV report with daily new users
	 * @return string for CSV file
	 */
	public function generate_new_users_daily_report() {
		//Gets the data
		$userCount = (new ViewCountNewUsersDaily())->get();

		//Builds the header
		$strOutput = "User count;Day date";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->users_count};".
				"{$count->creation_date}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV report with weekly new users
	 * @return string for CSV file
	 */
	public function generate_new_users_weekly_report() {
		//Gets the data
		$userCount = (new ViewCountNewUsersWeekly())->get();

		//Builds the header
		$strOutput = "User count;Week (of year) number;Month;Year";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->users_count};".
				"{$count->creation_week};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV report with monthly new users
	 * @return string for CSV file
	 */
	public function generate_new_users_monthly_report() {
		//Gets the data
		$userCount = (new ViewCountNewUsersMonthly())->get();

		//Builds the header
		$strOutput = "User count;Month;Year";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->users_count};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		return $strOutput;
	}

	/**
	 * All users cumulative count created daily basis
	 * @return string for CSV file
	 */
	private function generate_total_users_daily_report() {
		// get the data
		$usersTotal = (new ViewCountTotalUsersDaily())->get();

		//Builds the header
		$strOutput = "Cumulative user count;Date;Database date";

		//Builds each line for the response
		foreach ($usersTotal as $key => $count) {
			$strOutput .=
				"\r\n{$count->user_cumulative_count};".
				"{$count->created_on};".
				"{$count->db_created_on}";
		}

		// return the str
        return $strOutput;
    }

	/**
	 * All users cumulative count created weekly basis
	 * @return string for CSV file
	 */
	private function generate_total_users_weekly_report() {
		// get the data
		$usersTotal = (new ViewCountTotalUsersWeekly())->get();

		//Builds the header
		$strOutput = "Cumulative user count;Week;Month;Year";

		//Builds each line for the response
		foreach ($usersTotal as $key => $count) {
			$strOutput .=
				"\r\n{$count->user_cumulative_count};".
				"{$count->creation_week};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		// return the str
        return $strOutput;
    }

	/**
	 * All users cumulative count created monthly basis
	 * @return string for CSV file
	 */
	private function generate_total_users_monthly_report() {
		// get the data
		$usersTotal = (new ViewCountTotalUsersMonthly())->get();

		//Builds the header
		$strOutput = "Cumulative user count;Month;Year";

		//Builds each line for the response
		foreach ($usersTotal as $key => $count) {
			$strOutput .=
				"\r\n{$count->user_cumulative_count};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		// return the str
        return $strOutput;
    }

	/**
	 * All users cumulative count created monthly basis
	 * @return string for CSV file
	 */
	private function generate_account_migration_factor_daily_report() {
		// get the data
		$migrationFactor = (new ViewUserEmailMigration())->get();

		//Builds the header
		$strOutput = "Date;Migration percentage";

		//Builds each line for the response
		foreach ($migrationFactor as $key => $sequence) {
			$strOutput .=
				"\r\n{$sequence->original_creation_date};".
				"{$sequence->migration_percentage}";
		}

		// return the str
        return $strOutput;
    }

	/**
	 * Generates a CSV with total count of user authentications frequency grouped by user quantity
	 * @return string for CSV file
	 */
	private function generate_user_access_frequency_grouped_by_user_report() {
		//Gets the data
		$accessCount = (new ViewCountTotalUsersPerAuthenticationsGrouping())->get();

		//Builds the header
		$strOutput = "User access frequency;Users count";

		//Builds each line for the response
		foreach ($accessCount as $key => $group) {
			$strOutput .= "\r\n{$group->user_authentication_frequency};{$group->users_count}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV report with daily authentications count
	 * @return string for CSV file
	 */
	public function generate_authentications_daily_report() {
		//Gets the data
		$userCount = (new ViewCountAuthenticationsDaily())->get();

		//Builds the header
		$strOutput = "Authentications count;Day date";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->user_authentications_count};".
				"{$count->created_on}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV report with weekly authentications count
	 * @return string for CSV file
	 */
	public function generate_authentications_weekly_report() {
		//Gets the data
		$userCount = (new ViewCountAuthenticationsWeekly())->get();

		//Builds the header
		$strOutput = "Authentications count;Week;Month;Year";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->user_authentications_count};".
				"{$count->creation_week};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV report with monthly authentications count
	 * @return string for CSV file
	 */
	public function generate_authentications_monthly_report() {
		//Gets the data
		$userCount = (new ViewCountAuthenticationsMonthly())->get();

		//Builds the header
		$strOutput = "Authentications count;Month;Year";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->user_authentications_count};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV string report with users which authenticated daily on the app
	 * @return string for CSV file
	 */
	public function generate_users_which_authenticated_daily_report() {
		//Gets the data
		$userCount = (new ViewCountUsersWhichAuthenticatedDaily())->get();

		//Builds the header
		$strOutput = "Single-user count;Day date";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->users_count};".
				"{$count->created_on}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV string report with users which authenticated weekly on the app
	 * @return string for CSV file
	 */
	public function generate_users_which_authenticated_weekly_report() {
		//Gets the data
		$userCount = (new ViewCountUsersWhichAuthenticatedWeekly())->get();

		//Builds the header
		$strOutput = "Single-user count;Week;Month;Year";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->users_count};".
				"{$count->creation_week};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		return $strOutput;
	}

	/**
	 * Generates a CSV string report with users which authenticated monthly on the app
	 * @return string for CSV file
	 */
	public function generate_users_which_authenticated_monthly_report() {
		//Gets the data
		$userCount = (new ViewCountUsersWhichAuthenticatedMonthly())->get();

		//Builds the header
		$strOutput = "Single-user count;Month;Year";

		//Builds each line for the response
		foreach ($userCount as $key => $count) {
			$strOutput .=
				"\r\n{$count->users_count};".
				"{$count->creation_month};".
				"{$count->creation_year}";
		}

		return $strOutput;
	}

	// TODO: planodevoo convertion report
	// TODO: single user access report
	// TODO: frequency single user access report
	// TODO: nex-pilots report

	// TODO: 30+ airports as start waypoint
	/**
	 * Generates a CSV report with top 30 origin aerodromes
	 * @return string for CSV file
	 */
	public function generate_origin_aerodrome_usage_count_report() {
		//Gets the data
		$perAerodromeUsageList = (new ViewCountTotalUsagePerAerodrome())->getOrigins();

		//Builds the header
		$strOutput = "Usage frequency;Aerodrome name;Aerodrome ICAO;Aerodrome IATA";

		//Builds each line for the response
		foreach ($perAerodromeUsageList as $key => $aerodrome) {
			$strOutput .=
				"\r\n{$aerodrome->aerodrome_usage_count};".
				"{$aerodrome->aerodrome_name};".
				"{$aerodrome->aerodrome_icao};".
				"{$aerodrome->aerodrome_iata}";
		}

		return $strOutput;
	}

	// TODO: 30+ airports as end waypoint
	/**
	 * Generates a CSV report with top 30 destination aerodromes
	 * @return string for CSV file
	 */
	public function generate_destination_aerodrome_usage_count_report() {
		//Gets the data
		$perAerodromeUsageList = (new ViewCountTotalUsagePerAerodrome())->getDestinations();

		//Builds the header
		$strOutput = "Usage frequency;Aerodrome name;Aerodrome ICAO;Aerodrome IATA";

		//Builds each line for the response
		foreach ($perAerodromeUsageList as $key => $aerodrome) {
			$strOutput .=
				"\r\n{$aerodrome->aerodrome_usage_count};".
				"{$aerodrome->aerodrome_name};".
				"{$aerodrome->aerodrome_icao};".
				"{$aerodrome->aerodrome_iata}";
		}

		return $strOutput;
	}
}
