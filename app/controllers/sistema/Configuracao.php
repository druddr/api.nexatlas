<?php

/**
 * Configuracao maintains all the configuration for the invoker system.
 *
 * Configuracao might not have much to say, though, use with caution.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Configuracao extends Controller
{
    #region Instance models


    #endregion

    #region Actions/methods

    /**
     * Queries for a config by it's property 'chave'.
     * Note 1: Address the method by Configuracao.query({ chave: '<string>' })
     * @param mixed $query
	 * @ajax
     */
    public function query($query){
		$configs = $this->models->SistemaConfiguracao->records()
			->whereIf(!is_null($query) && !empty($query->text), 'chave', $query->text, '=', true)
			->get();

		return $configs->toStandardArray();
	}

    /**
     * Gets a configuration setting by it's (string) key
     * @param string $key
     */
    public static function get($key) {

        //Static access to the model
		$config = ModelSistemaConfiguracao::get($key);

		return $config;
    }

	/**
	 * Summary of create
	 * @param ModelSistemaConfiguracao $object
     * @return stdClass
     * @ajax
	 */
	public function create($object){
		//print_r($object);
		//	exit;
		$config = $this->models->SistemaConfiguracao->fill($object);
		$config->records()->save();
		return $config->getStandardObject();
	}

    /**
     * Summary of update
     * @param ModelSistemaConfiguracao $object
     * @return stdClass
     */
    public function update($object){
        //Checks the possibility of an update to succeed
        if (is_null($object) || empty($object->chave)) return [ 'error' => 1 ];

        //Creates the query object
        $config_records = $this->models->SistemaConfiguracao->records();
        $config_records->where('chave', $object->chave);

        //Fills the configuration of this request
        $this->models->SistemaConfiguracao->fill($config_records->getFirstModel());

        //If the key wasn't found to be configured
        if (empty($this->models->SistemaConfiguracao->chave)) {
            return [ 'error' => 1 ];
        }

        //If this configuration is fixed, doesn't allow any changes to it
        if ($this->models->SistemaConfiguracao->fixa) {
            return [ 'error' => 1 ];
        }

        //Fills with the modified propertie's values
        $this->models->SistemaConfiguracao->fill($object);

        //Sets the date of the last update
        $this->models->SistemaConfiguracao->data_ultima_alteracao = date('Y-m-d H:i:s');

        //Executes a save operation
        $config_records->save();

        return $this->models->SistemaConfiguracao;
    }

    #endregion
}
