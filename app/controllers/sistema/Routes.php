<?php

/**
 * Routes short summary.
 *
 * Routes description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Routes extends Controller
{
	/**
	 * @ajax
	 */
    public function query($data=null){
		return $this->views->Rotas->records()->get(null,['ativo_sistema'=>'1'])->toStandardArray();
	}
	
}
