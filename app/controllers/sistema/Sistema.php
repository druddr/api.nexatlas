<?php

/**
 * Sistema short summary.
 *
 * Sistema description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Sistema extends Controller
{
    /**
     * Queries for a system by it's property [id] or returns a full list
     * @param mixed $query 
	 * @ajax
     */
    public function query($query){
		$systems = $this->models->Sistema->records()
			->whereIf(!is_null($query) && !empty($query->text), 'id', $query->text, '=', true)
			->get();

		return $systems->toStandardArray();
	}
}
