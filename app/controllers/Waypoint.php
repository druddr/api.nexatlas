<?php
/**
 * Waypoint Provides data access for querying navigation waypoint.
 *
 * Waypoint Navigational info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Waypoint extends Controller
{
    /**
	 * Adds a waypoint from user navigation route
	 * @authorized
	 * @ajax
	 */
	public function query($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data) || !isset($data->text))
            throw new Exception('dataNotAnObject');

		//Used vars
		$results = [];
		$waypoints = new ModelWaypoint();

		//Preparing the feature collection for receiving objects
		$featureCollection = [
			'type' => GeoJSONTypeProxy::FeatureCollection,
			'features' => []
			];

		//In this case, we have a coordinate system match and a radius passed on
		if (preg_match(WaypointUtility::RegExDegreesMinutesSeconds, $data->text) && isset($data->r)) {
			$latitudeArray = explode(' ', trim(explode('/', $data->text)[0]));
			$longitudeArray = explode(' ', trim(explode('/', $data->text)[1]));

			$latitude =
				WaypointUtility::degreeMinutesSecondsToDecimalDegrees(
					$latitudeArray[0],
					$latitudeArray[1],
					$latitudeArray[2],
					$latitudeArray[3]
				);

			$longitude =
				WaypointUtility::degreeMinutesSecondsToDecimalDegrees(
					$longitudeArray[0],
					$longitudeArray[1],
					$longitudeArray[2],
					$longitudeArray[3]
				);

			//Adds the distance calculation alias
			$re = $waypoints->sphericalLawOfCosines($latitude, $longitude, $data->r);

			$coordsType = (new ModelWaypointType());
			$coordsType = $coordsType->getByCodeName('COORDS');

			//Maps the current feature
			array_push($featureCollection['features'], [
				'type' => GeoJSONTypeProxy::Feature,
				'geometry' => [
					'type' => GeoJSONTypeProxy::Point,
					'coordinates' => [ $longitude, $latitude ]
					//'coordinates' => [ [ $latitude, $longitude ] ]
				],
				'properties' => [
					'id' => null,
					'code' => null,
					'name' => strtoupper($data->text),
					'latitude' => $latitude,
					'longitude' => $longitude,
					'waypoint_type_id' => $coordsType->id,
					'waypoint_type_name' => $coordsType->name,
					'waypoint_type_code_name' => $coordsType->code_name,
					'waypoint_type_icon' => $coordsType->icon,
					'icao' => null,
					'iata' => null //,
					//'frequency' => null,
					//'frequency_type' => null
				]
			]);

		} else {
			//In this case, it is a text search
			$textExact = trim($data->text);

			//General search
			$re = (new ModelWaypointSearch())->get($textExact);
		}

		//Gets the model array (evaluate the query)
		$waypoints = $re->toModelArray();

		//print_r($re->last_sql);

		//if (!isset($waypoints) || !is_array($waypoints) || count($waypoints) == 0)
		//    throw new Exception('noWaypointsFound');

		//We have to map each waypoint as a feature in order for
		//The front-end tool MapBox to understand it
		foreach ($waypoints as $waypoint) {

			//Maps the current feature
			$feature = [
				'type' => GeoJSONTypeProxy::Feature,
				'geometry' => [
					'type' => GeoJSONTypeProxy::Point,
					'coordinates' => [ $waypoint->longitude, $waypoint->latitude ]
				],
				'properties' => [
					'id' => $waypoint->id,
					'code' => $waypoint->code,
					'name' => $waypoint->name,
					'latitude' => $waypoint->latitude,
					'longitude' => $waypoint->longitude,
					'waypoint_type_id' => $waypoint->Type->id,
					'waypoint_type_name' => $waypoint->Type->name,
					'waypoint_type_code_name' => $waypoint->Type->code_name,
					'waypoint_type_icon' => $waypoint->Type->icon,
					'icao' => $waypoint->icao_code,
					'iata' => $waypoint->iata_code //,
					//'frequency' => $waypoint->frequency,
					//'frequency_type' => $waypoint->frequency_type
				]
			];

			//Adds the feature to the feature collection
			array_push($featureCollection['features'], $feature);
		}

		return [ 'waypoints' => $waypoints, 'featureCollection' => $featureCollection ];
	}
}
