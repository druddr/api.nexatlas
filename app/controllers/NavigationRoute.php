<?php

/**
 * NavigationRoute Provides data access for navigational routes.
 *
 * NavigationRoute Navigational routes info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class NavigationRoute extends Controller
{
	/**
	 * Gets a list of recent routes of a given user
	 * @param mixed $data Input data with id/user_id
	 * @authorized
	 * @ajax
	 */
	public function get($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		if (!isset($data->user_id) && !isset($data->id)) throw new Exception('nullOrInvalidUserOrRouteId');

		//Starts building the query
		$routes = (new ModelRoute())
			->records()
			->where('deleted', 1, "!=");

		if (isset($data->id)) {
			//Searches for routes
			$routes = $routes->where('id', $data->id);
		} else if (isset($data->user_id)) {
			$routes = $routes->where('user_id', $data->user_id);
		}

		//Orders by favorite then creation date
		$routes = $routes->order('updated_on', 'DESC');

		//Gets the route (EntityCollection) collection for mapping features
		$routeCollection = $routes->toEntityCollection();

		//For each route, maps its waypoints
		$routeCollection = $routeCollection->map(function($route, $index) {
			$features = $route->AllWaypoints->map(
				function($waypoint, $i){
					$waypoint->WaypointType;
					return $waypoint;
				})->toArray();
			$route = $route->extract();
			$route->features = WaypointUtility::convertWaypointCollectionToGeoJson($features);
			return $route;
		});

		//Transforms it to a readable json
		$routes = $routeCollection->toArray();

		return [
			'routes' => $routes
			];
	}

	/**
	 * Gets a list of recent routes of a given user
	 * @param mixed $data Input data with route_id
	 * @authorized
	 * @ajax
	 */
	public function getWaypoints($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		if (!isset($data->route_id)) throw new Exception('invalidRouteId');

		//Queries this route waypoints
		$records = (new ModelRouteWaypoint())
			->records()
			->where('route_id', $data->route_id);

		//Gets the model list
		$routeWaypoints = $records->toModelArray();

		//Preparing the feature collection for receiving objects
		$featureCollection = [
			'type' => GeoJSONTypeProxy::FeatureCollection,
			'features' => []
			];

		//We have to map each waypoint as a feature in order for
		//The front-end tool MapBox to understand it
		foreach ($routeWaypoints as $waypoint) {

			//Maps the current feature
			$feature = [
				'type' => GeoJSONTypeProxy::Feature,
				'geometry' => [
					'type' => GeoJSONTypeProxy::Point,
					'coordinates' => [ $waypoint->longitude, $waypoint->latitude ]
					//'coordinates' => [ [ $waypoint->latitude, $waypoint->longitude ] ]
				],
				'properties' => [
					'id' => $waypoint->id,
					'code' => $waypoint->code,
					'name' => $waypoint->name,
					'latitude' => $waypoint->latitude,
					'longitude' => $waypoint->longitude,
					'waypoint_type_id' => $waypoint->Type->id,
					'waypoint_type_name' => $waypoint->Type->name,
					'waypoint_type_code_name' => $waypoint->Type->code_name,
					'waypoint_type_icon' => $waypoint->Type->icon,
					'icao' => $waypoint->icao_code,
					'iata' => $waypoint->iata_code,
					'frequency' => $waypoint->frequency,
					'frequency_type' => $waypoint->frequency_type
				]
			];

			//Adds the feature to the feature collection
			array_push($featureCollection['features'], $feature);
		}


		return [
			'features' => $featureCollection,
			'waypoints' => $records->toStandardArray()
			];
	}

    /**
	 * Creates a new user navigation route
	 * @authorized
	 * @ajax
	 */
    public function create($data){
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$userRecords = $this->models->User->records();
		$user = $this->models->User->getById($data->user_id);

		if (empty($user->id)) throw new Exception('invalidUser');

		if (isset($data->id)) {
			$existingRoute = $this->models->Route->getById($data->id);

			if (isset($existingRoute->id)) {
				throw new Exception('routeAlreadyExists');
			}
		}

		//Transaction scope
		$userRecords->beginTransaction();

		try {
			//Creates the route, then
			$newRoute = $this->models->Route->fill($data);
			//If the route will be copied, then, a new time should be set
			$newRoute->creation_date =
				isset($data->creation_date)
				&& (isset($data->clone) && !$data->clone) ? strtotime(date('Y-m-d H:i:s', $data->creation_date)) : time();
			$newRoute->updated_on =
				isset($data->updated_on)
				&& $newRoute->creation_date <= $newRoute->updated_on ? strtotime(date('Y-m-d H:i:s', $data->updated_on)) : time();
			$newRoute->deleted_on = null;
			$newRoute->deleted = false;
			$newRoute->is_simple = false;
			$newRoute->favourite = false;
			$newRoute->records()->save();

			//If the route already has at least a waypoint
			if (isset($data->waypoints) && is_array($data->waypoints) && count($data->waypoints) > 0)
				$newRoute->addWaypoints($data->waypoints, (isset($data->clone) && $data->clone));

		}
		catch (Exception $exception) {
			//Rolls back the process of profile filling
			$userRecords->rollBack();

			throw $exception;
		}

		//Commits the process of profile filling
		$userRecords->commit();

		////Corrects the value of favourite
		//$newRoute->favourite = (bool)(intval($newRoute->favourite) == 1);

		return [
			'route' => $newRoute->extract(),
			'waypoints' => $newRoute->AllWaypoints->map(function($item, $index){ return $item->extract(); })->toArray()
		];
	}

	/**
	 * Updates a ground speed of the route
	 * @authorized
	 * @ajax
	 */
    public function updateGroundSpeed($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$route = (new ModelRoute())->getById($data->id);

		if (!isset($route->id)) throw new Exception('invalidRouteId');

		//Updates the route setting it to favourite
		$route = $route->updateInfo($data);

		return [
			'route' => $route->extract()
			];
	}

	/**
	 * Makes a route favourite
	 * @authorized
	 * @ajax
	 */
    public function setFavourite($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$route = (new ModelRoute())->getById($data->id);

		if (!isset($route->id)) throw new Exception('invalidRouteId');

		//Updates the route setting it to favourite
		$route = $route->updateInfo($data);

		return [
			'route' => $route->extract()
			];
	}

	/**
	 * Creates a new user navigation route
	 * @authorized
	 * @ajax
	 */
    public function checkWaypoints($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$route = (new ModelRoute())->getById($data->route_id);

		//Invalid route object
		if (!isset($route->id)) throw new Exception('invalidRouteId');

		//Transaction
		$routeRecords = $route->records();
		$routeRecords->beginTransaction();

		try {
			//Updates route info in general
			$route
				->updateInfo($data)
				->checkWaypoints($data);
		}
		catch (Exception $exception) {
			//Cancels updates/removes/inserts
			$routeRecords->rollBack();
			throw $exception;
		}

		//Releases changes
		$routeRecords->commit();

		return [
			'route' => $route->extract(),
			'waypoints' => $route->AllWaypoints->map(function($item, $index) { return $item->extract(); })->toArray()
			];
	}

    /**
	 * Adds a waypoint from user navigation route
	 * @authorized
	 * @ajax
	 */
	public function addWaypoint($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$route = $this->models->Route->getById($data->route_id);

		if (!isset($route->id)) throw new Exception('invalidRouteId');

		$waypoint = $route->addWaypoint($data);

		return [
			'route' => $route->extract(),
			'waypoint' => $waypoint->extract()
			];
	}

	/**
	 * Adds a waypoint from user navigation route
	 * @authorized
	 * @ajax
	 */
	public function addWaypoints($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data) || !is_array($data->waypoints))
            throw new Exception('dataNotAnObject');

		$route = $this->models->Route->getById($data->id);

		if (!isset($route->id)) throw new Exception('invalidRouteId');

		$records = $route->records();
		$records->beginTransaction();

		try {
			//Adds waypoints to the route
			$route = $route->addWaypoints($data->waypoints);

			//Updates the route itself
			$route->updated_on = time();
			$route = $route->updateInfo($route);
		} catch (Exception $exception) {
			//Cancels changes
			$records->rollBack();
			throw $exception;
		}

		//Confirms changes
		$records->commit();

		return [
			'route' => $route->extract(),
			'waypoints' => $route->AllWaypoints->map(function($waypoint) { return $waypoint->extract(); })->toArray()
			];
	}

	/**
	 * Removes a waypoint from user navigation route
	 * @authorized
	 * @ajax
	 */
	public function removeWaypoint($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$route = $this->models->Route->getById($data->route_id);

		if (!isset($route->id)) throw new Exception('invalidRouteId');

		//Transaction removal
		$routeRecords = $route->records();
		$routeRecords->beginTransaction();

		try {
			$waypoint = $route->removeWaypoint($data->id);
			//Sets updated time on route
			$route->updated_on = time();
			//Updates the route info
			$route = $route->updateInfo($route);
		}
		catch (Exception $exception) {
			//Cancels the transaction and rethrows an error
			$routeRecords->rollBack();
			throw new Exception('waypointNotRemoved');
		}

		$routeRecords->commit();

		return [
			'route' => $route->extract(),
			'waypoint' => $waypoint->extract()
			];
	}

	/**
	 * Removes an entire route from user navigation sources
	 * @authorized
	 * @ajax
	 */
	public function removeRoute($data) {
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		$route = $this->models->Route->getById($data->route_id);

		if (!isset($route->id)) throw new Exception('invalidRouteId');

		//Transaction removal
		$routeRecords = $route->records();
		$routeRecords->beginTransaction();

		try {
			//Route has a logical delete
			$route->delete();
		}
		catch (Exception $exception) {
			//Cancels the transaction and rethrows an error
			$routeRecords->rollBack();
			throw new Exception('routeNotRemoved');
		}

		$routeRecords->commit();

		return [ 'status' => $route->deleted ? 200 : 500 ];
	}
}
