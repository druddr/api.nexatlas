<?php

/**
 * UserProfile Provides data access for user profile.
 *
 * UserProfile User profile info.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class UserProfile extends Controller
{

    // /**
    //  *
    //  * @param mixed $dispositivo
	//  * @ajax
    //  */
    // public function adicionarDispositivo($info){
	// 	if(empty($info->push_registration_id)) return false;

	// 	$model = $this->models->PessoaPerfilDispositivos->records()
	// 		->where('id_dispositivo',$info->push_registration_id)
	// 		->where('id_pessoa_perfil',$info->pessoa_perfil->id)
	// 		->getFirstModel();
	// 	if($model){
	// 		$this->models->PessoaPerfilDispositivos->id = $model->id;
	// 	}
	// 	$plataforma = 'A';
	// 	switch($info->plataforma){
	// 		case 'android':
	// 			$plataforma ='A';
	// 			break;
	// 		case 'ios':
	// 			$plataforma ='I';
	// 			break;
	// 		case 'windows':
	// 			$plataforma ='W';
	// 			break;
	// 	}
	// 	$this->models->PessoaPerfilDispositivos->id_pessoa_perfil = $info->pessoa_perfil->id;
	// 	$this->models->PessoaPerfilDispositivos->id_dispositivo = $info->push_registration_id;
	// 	$this->models->PessoaPerfilDispositivos->autorizado = 1;
	// 	$this->models->PessoaPerfilDispositivos->marca = $info->manufacturer;
	// 	$this->models->PessoaPerfilDispositivos->modelo = $info->model;
	// 	$this->models->PessoaPerfilDispositivos->versao = $info->version;
	// 	$this->models->PessoaPerfilDispositivos->plataforma = $plataforma;
	// 	$this->models->PessoaPerfilDispositivos->save();
	// 	return $this->models->PessoaPerfilDispositivos;
	// }

	/**
	 * @ajax
	 */
	// public function queryByEmpresaFilial($query=null){
	// 	$records = $this->models->PessoaPerfil->records();
	// 	$records->select(['pessoa_perfil.*',
	// 					  'pessoa_fisica.nome',
	// 					  'pessoa_fisica.sobrenome',
	// 					  'pessoa_fisica.data_nascimento',
	// 					  'pessoa_fisica.cpf']);
	// 	$records->join("pessoa_fisica", ['pessoa_fisica.id_pessoa' => 'pessoa_perfil.id_pessoa']);
	// 	$records->join("empresa_filial_funcionario", ['empresa_filial_funcionario.id_pessoa'=>'pessoa_perfil.id_pessoa'],'LEFT');
	// 	$records->join("profissional", ['profissional.id_pessoa'=>'pessoa_perfil.id_pessoa'],'LEFT');
	// 	$records->join("empresa_filial_profissional", ['empresa_filial_profissional.id_profissional'=>'profissional.id'],'LEFT');
	// 	$records->whereMany(['empresa_filial_profissional.id_empresa_filial',AuthorizedSession::get()->getCurrentSubsidiary()],
	// 					    ['empresa_filial_funcionario.id_empresa_filial',AuthorizedSession::get()->getCurrentSubsidiary(),'=',true,'OR']);
	// 	if(isset($query->text)){
	// 		$records->whereMany(['pessoa_fisica.nome',"%{$query->text}%",'LIKE'],
	// 							['pessoa_fisica.sobrenome',"%{$query->text}%",'LIKE',true,'OR'],
	// 							['pessoa_fisica.cpf',"%{$query->text}%",'LIKE',true,'OR'],
	// 							['pessoa_fisica.data_nascimento',"%{$query->text}%",'LIKE',true,'OR']);
	// 	}
	// 	$records->group('pessoa_perfil.id_pessoa');
	// 	return $records->get()->toStandardArray();
	// }

    /**
	 * Creates user profile info
	 * @authorized
	 * @ajax
	 */
    public function create($data){
		//Checks the validity of the data
        if(!isset($data) || !is_object($data))
            throw new Exception('dataNotAnObject');

		if (!isset($data->user_id)) throw new Exception('paramUserIdNotDefined');

		$userRecords = $this->models->User->records();
		$user = $this->models->User->getById(intval($data->user_id));

		if (empty($user->id)) throw new Exception('invalidUser');

		//Sets user profile for editing mode
		if(!empty($user->Profile->id)) {
			$profile = $user->Profile;

			//Though if it's complete, nothing to do here
			if ($user->Profile->complete)
				throw new Exception('userProfileAlreadyExists');
		}
		//Transactioned process of profile filling
		$userRecords->beginTransaction();

		//Defines which mode the user is authenticating for the first time
		if ($user->google_connected)
			$dataProvider = OAuthDataProvider::GoogleMode;
		else if ($user->facebook_connected)
			$dataProvider = OAuthDataProvider::FacebookMode;
		else
			$dataProvider = OAuthDataProvider::AppMode;

		try {
			//Sets read terms or not
			$user->read_terms = ($user->read_terms || $data->read_terms);

			//gets the user profile properties and fills/save the data
			$profile = !isset($profile) ? $this->models->UserProfile->fill($data) : $profile->fill($data);

			//Sets creation date if not set yet
			$profile->creation_date = !isset($profile->id) ? time() : $profile->creation_date;
			$profile->complete = true;
			$profile->complete_on = time();
			$profile->records()->save();

			//Activates the user account, now the profile is filled
			$user->activate();

			//Auth not anymore only for app-mode, for facebook and google as well
			//$auth = $this->models->Authentication->setSession($user, OAuthDataProvider::AppMode);
			$auth = $this->models->Authentication->setSession($user, $dataProvider);
		} catch (Exception $exception) {
			//Rolls back the process of profile filling
			$userRecords->rollBack();

			throw $exception;
		}

		//Commits the process of profile filling
		$userRecords->commit();

		//Forcing attribution of profile info for email usage
		$user->Profile = $profile;

		//If the app should send an email
		$accountEmail = !config::$AppSendsEmail || $user->sendAccountCreationEmail($dataProvider);

		return utf8_decode_all([
			'user' => $user->toStandard(),
			'userProfile' => $profile->toStandard(),
			'remember' => $data->remember || true,
			'token' => $auth->token
		]);
	}

	///**
	// * @ajax
	// */
	//public function update($id,$data){
	//    $pessoa = $this->models->Pessoa->createPessoaFisica(new ModelPessoaFisica($data->pessoa_fisica),new ModelPessoaPerfil($data->pessoa_perfil));
	//    return utf8_decode_all([
	//        'pessoa_fisica'=>$data->pessoa_fisica,
	//        'pessoa_perfil'=>$data->pessoa_perfil
	//    ]);
	//}
}
