<?php

/**
 * Autenticacao short summary.
 *
 * Autenticacao description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Autenticacao extends Controller
{

    /**
	 * Summary of autenticar
	 * @param mixed $info
	 * @authorized
	 * @ajax
	 */
    public function autenticar($info){
		$record = $this->models->PessoaPerfil->records();
		$pessoa_perfil = $record
			->select(['id','id_pessoa','login','nome','sobrenome','usuario_google','usuario_facebook','foto_perfil','ultima_atualizacao'])
			->where('login',$info->login)
			->where('password', $this->models->PessoaPerfil->passwordHash($info->password))->getFirstModel();

		if(!$pessoa_perfil->id) return  ['error'=>true ];

		CurrentUser::set($pessoa_perfil);

		$sessao = new ModelPessoaPerfilSessao();
		//$sessao->
		
		return [
			'user' => CurrentUser::get()->user->extract(),
			'routes'=> CurrentUser::get()->routes,
			'companies'=> CurrentUser::get()->objects->companies,
			'subsidiaries'=> CurrentUser::get()->objects->subsidiaries
		];
	}
	/**
	 * @authorized
	 * @ajax
	 */
	public function logout(){
		session_destroy();
		session_start();
		return true;
	}
	/**
	 * @authorized
	 * @ajax
	 */
	public function autorizar(){
		if(CurrentUser::get()->user==null || (count(CurrentUser::get()->routes)==0 && !config::$DevMode)) {
			Response::unauthorized(true,'AUTH001');
		}
		$headers = getallheaders();

		if(!isset($headers['CCID']) && !isset($headers['Ccid'])){
			Response::unauthorized(true,'AUTH002');
		}
		$current_company_header = isset($headers['CCID']) ? $headers['CCID'] : $headers['Ccid'];
		$current_subsidiary_header = isset($headers['CSID']) ?  $headers['CSID'] : $headers['Csid'];

		if(!in_array($current_company_header,CurrentUser::get()->companies)){
			Response::forbidden(true,'AUTH003');
		}

		CurrentUser::get()->setCurrentCompany($current_company_header);
		CurrentUser::get()->setCurrentSubsidiary($current_subsidiary_header);

		//$route_authorized = false;
		//foreach(Currentuser::get()->routes as $rota){
		//	if($rota->rota==$this->router->getCurrentRouteUri()) $route_authorized = true;
		//}
		//if(!$route_authorized && !config::$DevMode) {
		//	Response::forbidden(true,'AUTH004');
		//}

		//Starts the Authorized Session that contains the current user and its companies
		AuthorizedSession::start();
		return true;
	}
}
