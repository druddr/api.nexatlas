<?php

/**
 * PessoaContato short summary.
 *
 * PessoaContato description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class PessoaContato extends Controller
{

    /**
     * Creates contacts for a given person
     * @ajax
     */
    public function create($data){
        if (!isset($data) || empty($data)) return [ 'error' => 1 ];

        //In case the data isn't an array, creates an array for iteration of records
        if (!is_array($data)) $data = [ $data ];

        //Returns the result to the views
        return $this->models->PessoaContato->saveMany($data);
	}

    /**
     * 
     * @param mixed $data 
     * @return mixed the changed data
     */
    public function update($data) {
        if (!isset($data) || !is_object($data) || !property_exists($data, 'id') || empty($data->id)) return [ 'error' => 1 ];

        ////Gets the records for the update operation
        //$records = $this->models->PessoaContato->records();

        ////Queries for the item to apply the updates
        //$contact = $records->get($data->id)->getFirstModel();
        
        ////The contact wasn't found, get outta here
        //if ($contact == null) return;

        ////Manipulates the object and applies the changes
        //$contact->fill($data);
        //$contact->save();

        //Updates the item
        $contact = $this->models->PessoaContato->update($data);

        //Extracts the standard object and returns
        return !$contact ? [ 'error' => true ] : $contact->extract();
    }

	/**
     * Summary of queryByIdPessoa
     * @param mixed $data
     * @return mixed
     * @ajax
     */
	public function queryByIdPessoa($data){
        //Validates to avoid error throwing
        if (!isset($data) || !property_exists($data, 'id_pessoa') || empty($data->id_pessoa)) return [ 'error' => 1 ];

		return $this->models->PessoaContato->getByIdPessoa($data->id_pessoa)->toStandardArray();
	}

    /**
     * @ajax
     */
	public function get($id=null){
		$person = $this->models->PessoaContato;
		$result = $person->records()->get($id,null)->limit(1)->toStandardArray();
		return reset($result);
	}

    /**
     * @ajax
     */
	public function query($query=null){
		$result = $this->model->PessoaContato->records()->get(null,$query)->toStandardArray();
		return array_merge($result,$result);
	}
}