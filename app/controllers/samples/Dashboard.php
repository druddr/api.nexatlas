<?php

/**
 * Dashboard short summary.
 *
 * Dashboard description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Dashboard extends Controller
{
    ///**
    // * @ajax
    // */
    //public function post($cliente){
    //    return true;
    //}
    ///**
    // * @ajax
    // */
    //public function put($id,$cliente){
    //    return true;
    //}
	/**
	 * @ajax
	 */
	public function delete($id){
		return true;
	}
	/**
	 * @ajax
	 */
	public function get($id=null){
		$inicio = date("Y-m-d 00:00:00",strtotime("last sunday this week"));
		$fim = date("Y-m-d 23:59:59",strtotime("next saturday this week"));
		$result = $this->models->Cliente->getClientesByInicioRelacionamento($inicio,$fim);
		return array (
			'clientes_novos' => $result->count_id,
			'servicos_realizados' => 123,
			'produtos_vendidos' => 223,
			'ficha_media' => 355
		);
	}
	/**
	 * @ajax
	 */
	public function query($query=null){			
		$result = $this->model->Clientes->records()->get(null,$query)->toStandardArray();
		return array_merge($result,$result);
	}
}
