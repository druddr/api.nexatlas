<?php

/**
 * PessoaFisica short summary.
 *
 * PessoaFisica description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class PessoaFisica extends Controller
{

    /**
     * Creates people
     * @ajax
     */
    public function create($data){
        
	}
	/**
	 * Summary of queryByCPF
	 * @param mixed $data 
	 * @return mixed
	 * @ajax
	 */
	public function queryByCPF($data){
        //Validates to avoid error throwing
        if (!isset($data) || !property_exists($data, 'cpf') || empty($data->cpf)) return [ 'error' => 'cpf-invalido' ];
		
		//Queries the person by it's ID
		$person = $this->models->PessoaFisica->getByCPF($data->cpf)->toStandard();

		return $person;
	}
	/**
	 * @ajax
	 */
	public function getByCPF($cpf){
        if (strlen($cpf)!=11) return [ 'error' => 'cpf-invalido' ];
		
		return $this->models->PessoaFisica->getByCPF($cpf)->extract();
	}
    /**
     * @ajax
     * Gets an object by three different properties:
     * * cpf
     * * nome
     * * sobrenome
     * @return array of pessoa
     */
	public function getByObject($query=null){
		return $this->models->PessoaFisica->records()
			->likeIf(isset($query->cpf) && !is_null($query->cpf), 'cpf','%'.@$query->cpf.'%')
            ->likeIf(isset($query->nome) && !is_null($query->nome), 'nome','%'.@$query->nome.'%')
			->likeIf(isset($query->sobrenome) && !is_null($query->sobrenome), 'sobrenome','%'.@$query->sobrenome.'%')
			->order("nome, sobrenome")
            ->limit(30)            
			->toStandardArray();
	}

    /**
     * @ajax
     */
	public function get($id=null){
		$person = $this->models->Pessoa;
		$result = $person->records()->get($id,null)->limit(1)->toStandardArray();
		return reset($result);
	}

    /**
     * @ajax
     */
	public function query($query=null){			
		$result = $this->model->Pessoa->records()->get(null,$query)->toStandardArray();
		return array_merge($result,$result);
	}
	
}
