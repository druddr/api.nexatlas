<?php
abstract class MailProvider {
	/**
	 * PEAR mail provider type
	 */
	const Pear = "PEAR";
	/**
	 * SendGrid mail provider type
	 */
	const SendGrid = "SendGrid";
}