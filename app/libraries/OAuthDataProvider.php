<?php

/**
 * OAuthDataProvider class gives access to the used providers.
 *
 * OAuthDataProvider has as many providers as implemented on the system.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class OAuthDataProvider
{
	/**
	 * Google OAuth mode
	 */
	const FacebookMode = "facebook";
	/**
	 * Google OAuth mode
	 */
	const GoogleMode = "google";
	/**
	 * App auth mode
	 */
	const AppMode = "app";
}
