<?php
class GeoJSONTypeProxy implements JsonSerializable {

	const Point = "Point";
	const MultiPoint = "MultiPoint";
	const LineString = "LineString";
	const Polygon = "Polygon";
	const MultiPolygon = "MultiPolygon";
	const Feature = "Feature";
	const FeatureCollection = "FeatureCollection";
	const GeometryCollection = "GeometryCollection";

	public $type;
	public $coordinates;
	public $geometry;
	public $geometries;
	public $properties;
	public $features;
	public $featureCollection;

	public function __construct($waypoints) {

	}

	public function jsonSerialize() {
		return [];
	}

	public static function toFeatureCollection() {
		//Preparing the feature collection for receiving objects
		$featureCollection = [
			'type' => GeoJSONTypeProxy::FeatureCollection,
			'features' => []
			];

		return $featureCollection;
	}

	public static function toFeature() {
		return [
			'type' => GeoJSONTypeProxy::Feature,
			'geometry' => "",
			'properties' => ""
			];
	}
}
