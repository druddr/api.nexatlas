<?php
require_once './vendor/autoload.php';
//require_once "Mail.php";

/**
 * CustomUtilities short summary.
 *
 * CustomUtilities description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class CustomUtilities
{
	/**
	 * Removes accents from a string
	 * @param mixed $string The string to have its accents removed
	 * @return mixed
	 */
	public static function removeAccents($string){
		return preg_replace(array("/(�|�|�|�|�)/","/(�|�|�|�|�)/","/(�|�|�|�)/","/(�|�|�|�)/","/(�|�|�|�)/","/(�|�|�|�)/","/(�|�|�|�|�)/","/(�|�|�|�|�)/","/(�|�|�|�)/","/(�|�|�|�)/","/(�)/","/(�)/"),explode(" ","a A e E i I o O u U n N"),$string);
	}

    /**
	 * This methods receives a string '0420' and returns '04.20'
	 */
    public static function getNumberFromMaskedString($s){

        if(count($s) < 4)
            return $s;

		return substr($s,0,-2).'.'.substr($s,-2);
	}

	/**
	 * Sends an email using the referred mail provider plugin
	 * This function does UTF-8 encoding
	 * @param mixed $mailProvider What kind of provider is to be used?
	 * @param mixed $toUser Who is the receiver (user name)?
	 * @param mixed $toEmail Who is the receiver (email)?
	 * @param mixed $subject Which subject to attach to the email?
	 * @param mixed $messageHTML The HTML version of the message body to append to the email
	 * @param mixed $messageText The text version of the message body to append to the email
	 * @param mixed $replyToUser The reply-to user
	 * @param mixed $replyToEmail The reply-to mail
	 * @return array With information regarding success or failure
	 */
	public static function sendEmail($mailProvider, $toUser, $toEmail, $subject, $messageHTML, $messageText, $replyToUser = null, $replyToEmail = null) {
		$mailSent;

		//Subject, message encoding
		$subject = trim(join(' ', [Config::SmtpHeaderSubjectTag, $subject]));
		////If necessary, these contents can be utf8 encoded
		//$messageHTML = utf8_encode($messageHTML);
		//$messageText = utf8_encode($messageText);

		try {
			//Switches through the implemented mailing types
			switch ($mailProvider)
			{
				case MailProvider::Pear:
					//PEAR mailing method
					$mailSent =
						CustomUtilities::pearMailSubmit(
							$toUser,
							$toEmail,
							$subject,
							$messageHTML,
							$messageText
						);
					break;

				case MailProvider::SendGrid:
					//SendGrid mailing method
					$mailSent =
						CustomUtilities::sendGridMailSubmit(
							$toUser,
							$toEmail,
							$subject,
							$messageHTML,
							$messageText,
							isset($replyToUser) ? $replyToUser : config::SmtpHeaderReplyToUser,
							isset($replyToEmail) ? $replyToEmail : config::SmtpHeaderReplyToMail
						);
					break;

				default:
					throw new Exception('invalidMailProviderType');
				//break;
			}
		}
		catch (Exception $exception) {
			//Error trying to execute the sending
			$mailSent = [ 'status' => 'mailNotSent', 'error' => $exception->getMessage ];
		}

		//Validation for correct output message and status
		if ($mailSent['status'] != 200 && $mailSent['status'] != 202) {
			$mailSent = [ 'status' => 'mailNotSent', 'error' => $mailSent['response'] ];
		} else {
			$mailSent = [ 'status' => 200 ];
		}

		return $mailSent;
	}

	/**
	 * Sends an email using PEAR plugin (require_once at the top of the page)
	 * This function does UTF-8 encoding
	 * @param mixed $to Who is the receiver?
	 * @param mixed $subject Which subject to attach to the email?
	 * @param mixed $messageHTML The HTML version of the message body to append to the email
	 * @param mixed $messageText The text version of the message body to append to the email
	 * @return array With information regarding success or failure
	 */
	public static function pearMailSubmit($toUser, $toEmail, $subject, $messageHTML, $messageText) {
		require_once 'Mail.php';
		require_once 'Mail/mime.php';

		$mailSent;
		$to = implode(' ', [$toUser, $toEmail]);

		//Sets the email headers
		$headers = [
		  'From' => implode(' ', [config::SmtpHeaderFromUser, '<'.config::SmtpHeaderFromMail.'>']),
		  'Reply-To' => implode(' ', [config::SmtpHeaderReplyToUser, '<'.config::SmtpHeaderReplyToMail.'>']),
		  'To' => $to,
		  'Subject' => $subject,
		  'Content-Type'  => 'text/html; charset=UTF-8'
		];

		//Builds smtp params array
		$smtpParams = [
		  'host' => Config::SmtpHost,
		  'port' => Config::SmtpPort,
		  'auth' => true,
		  'username' => Config::SmtpUser,
		  'password' => Config::SmtpPassword
		];

		//Mime type array
		$mime_params = array(
		  'text_encoding' => '7bit',
		  'text_charset'  => 'UTF-8',
		  'html_charset'  => 'UTF-8',
		  'head_charset'  => 'UTF-8'
		);

		//This object is going to encode the content correctly
		$mime = new Mail_mime();

		//Body is HTML
		$mime->setTXTBody(utf8_encode($messageText));
		$mime->setHTMLBody(utf8_encode($messageHTML));

		$message = $mime->get($mime_params);
		$headers = $mime->headers($headers);

		//Generates the email and sends it
		$smtp = Mail::factory('smtp', $smtpParams);
		$mail = $smtp->send($to, $headers, $message);

		//Validation for correct output message and status
		if (PEAR::isError($mail)) {
			return [ 'status' => 500, 'error' => $mail->getMessage() ];
		}

		return [ 'status' => 200 ];
	}

	/**
	 * Sends a SendGrid based email
	 * @param mixed $toUser The destination user name
	 * @param mixed $toEmail The destination email
	 * @param mixed $subject Subject of this email
	 * @param mixed $messageHTML HTML content version
	 * @param mixed $messageText Text content version
 	 * @param mixed $replyToUser The reply-to user
	 * @param mixed $replyToEmail The reply-to mail
	 * @return array The actual statuscode of this submission
	 */
	public static function sendGridMailSubmit($toUser, $toEmail, $subject, $messageHTML, $messageText, $replyToUser, $replyToEmail) {
		//API KEY from config
		$apiKey = config::SendGridAPIKey;
		$mail = CustomUtilities::buildSendGridEmail($toUser, $toEmail, $subject, $messageHTML, $messageText, $replyToUser, $replyToEmail);
		$sg = new \SendGrid($apiKey);
		$response = $sg->client->mail()->send()->post($mail);

		return [ 'status' => $response->statusCode(), 'headers' => $response->headers(), 'response' => $response->body() ];
	}

	/**
	 * Builds a SendGrid email for sending
	 * @param mixed $toUser The destination user name
	 * @param mixed $toEmail The destination email
	 * @param mixed $subject Subject of this email
	 * @param mixed $messageHTML HTML content version
	 * @param mixed $messageText Text content version
	 * @param mixed $replyToUser The reply-to user
	 * @param mixed $replyToEmail The reply-to mail
	 * @return SendGrid\Mail The mail ready for submission
	 */
	public static function buildSendGridEmail($toUser, $toEmail, $subject, $messageHTML, $messageText, $replyToUser, $replyToEmail)
	{
		//Gets "from" user information
		$fromUser = config::SmtpHeaderFromUser;
		$fromEmail = config::SmtpHeaderFromMail;
		//Sets up "from" info
		$from = new \SendGrid\Email($fromUser, $fromEmail);
		$to = new \SendGrid\Email($toUser, $toEmail);
		//First we set text, after, HTML RFC 1341
		$contentText = new \SendGrid\Content("text/plain", $messageText);
		$contentHTML = new \SendGrid\Content("text/html", $messageHTML);
		$mail = new \SendGrid\Mail($from, $subject, $to, $contentText);
		//$mail->addContent($contentText);
		$mail->addContent($contentHTML);
		//Defining reply-to
		$reply_to = new \SendGrid\ReplyTo(implode(' ', [$replyToUser, '<'.$replyToEmail.'>']));
		$mail->setReplyTo($reply_to);

		return $mail;

		//echo $response->statusCode();
		//print_r($response->headers());
		//echo $response->body();

		////Creating "from", "to" and defining mail instance
		//$fromEmail = new SendGrid\Email(config::SmtpHeaderFromUser, config::SmtpHeaderFromMail);
		//$to = new SendGrid\Email($toUser, $toEmail);

		////Text content
		//$contentText = new SendGrid\Content("text/plain", $messageText);
		////$mail->addContent($contentText);

		////HTML content
		//$contentHTML = new SendGrid\Content("text/html", $messageHTML);
		////$mail->addContent($contentHTML);

		////Creates the mail object to be sent
		//$mail = new Sendgrid\Mail($fromEmail, $subject, $to, $contentHTML);

		#region Other personalization samples

		////Personalization implementations
		//$to = new SendGrid\Email($toUser, $toMail);
		//$personalization = new SendGrid\Personalization();
		//$personalization->addTo($to);
		//$personalization->setSubject("Hello World from the SendGrid PHP Library");
		//$personalization->addHeader("X-Test", "test");
		//$personalization->addHeader("X-Mock", "true");
		//$personalization->addSubstitution("%name%", "Example User");
		//$personalization->addSubstitution("%city%", "Denver");
		//$personalization->addCustomArg("user_id", "343");
		//$personalization->addCustomArg("type", "marketing");
		//$personalization->setSendAt(1443636843);
		//$mail->addPersonalization($personalization);

		////Add more to, cc, bcc mails sample
		//$email2 = new Email("Example User", "test2@example.com");
		//$personalization->addTo($email2);
		//$email3 = new Email("Example User", "test3@example.com");
		//$personalization->addCc($email3);
		//$email4 = new Email("Example User", "test4@example.com");
		//$personalization->addCc($email4);
		//$email5 = new Email("Example User", "test5@example.com");
		//$personalization->addBcc($email5);
		//$email6 = new Email("Example User", "test6@example.com");
		//$personalization->addBcc($email6);

		//$personalization2 = new Personalization();
		//$email7 = new Email("Example User", "test7@example.com");
		//$personalization2->addTo($email7);
		//$email8 = new Email("Example User", "test8@example.com");
		//$personalization2->addTo($email8);
		//$email9 = new Email("Example User", "test9@example.com");
		//$personalization2->addCc($email9);
		//$email10 = new Email("Example User", "test10@example.com");
		//$personalization2->addCc($email10);
		//$email11 = new Email("Example User", "test11@example.com");
		//$personalization2->addBcc($email11);
		//$email12 = new Email("Example User", "test12@example.com");
		//$personalization2->addBcc($email12);
		//$personalization2->setSubject("Hello World from the SendGrid PHP Library");
		//$personalization2->addHeader("X-Test", "test");
		//$personalization2->addHeader("X-Mock", "true");
		//$personalization2->addSubstitution("%name%", "Example User");
		//$personalization2->addSubstitution("%city%", "Denver");
		//$personalization2->addCustomArg("user_id", "343");
		//$personalization2->addCustomArg("type", "marketing");
		//$personalization2->setSendAt(1443636843);
		//$mail->addPersonalization($personalization2);

		#endregion

		#region Attachments sample

		////Attachment mock
		//$attachment = new Attachment();
		//$attachment->setContent("TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gQ3JhcyBwdW12");
		//$attachment->setType("application/pdf");
		//$attachment->setFilename("balance_001.pdf");
		//$attachment->setDisposition("attachment");
		//$attachment->setContentId("Balance Sheet");
		//$mail->addAttachment($attachment);

		////#2 attachment mock
		//$attachment2 = new Attachment();
		//$attachment2->setContent("BwdW");
		//$attachment2->setType("image/png");
		//$attachment2->setFilename("banner.png");
		//$attachment2->setDisposition("inline");
		//$attachment2->setContentId("Banner");
		//$mail->addAttachment($attachment2);

		#endregion

		#region Templating sample

		//Templating
		//$mail->setTemplateId("439b6d66-4408-4ead-83de-5c83c2ee313a");
		# This must be a valid [batch ID](https://sendgrid.com/docs/API_Reference/SMTP_API/scheduling_parameters.html) to work
		# $mail->setBatchID("sengrid_batch_id");
		//$mail->addSection("%section1%", "Substitution Text for Section 1");
		//$mail->addSection("%section2%", "Substitution Text for Section 2");
		//$mail->addHeader("X-Test1", "1");
		//$mail->addHeader("X-Test2", "2");
		//$mail->addCategory("May");
		//$mail->addCategory("2016");
		//$mail->addCustomArg("campaign", "welcome");
		//$mail->addCustomArg("weekday", "morning");
		//$mail->setSendAt(1443636842);

		//$asm = new SendGrid\ASM();
		//$asm->setGroupId(99);
		//$asm->setGroupsToDisplay([4,5,6,7,8]);
		//$mail->setASM($asm);

		//$mail->setIpPoolName("23");

		#endregion

		#region Additional mail settings definition sample

		////Additional mail settings
		//$mail_settings = new SendGrid\MailSettings();

		//$bcc_settings = new BccSettings();
		//$bcc_settings->setEnable(true);
		//$bcc_settings->setEmail("test@example.com");
		//$mail_settings->setBccSettings($bcc_settings);
		//$sandbox_mode = new SandBoxMode();
		//$sandbox_mode->setEnable(true);
		//$mail_settings->setSandboxMode($sandbox_mode);

		//$bypass_list_management = new SendGrid\BypassListManagement();
		//$bypass_list_management->setEnable(true);
		//$mail_settings->setBypassListManagement($bypass_list_management);

		////Footer definition
		//$footer = new SendGrid\Footer();
		//$footer->setEnable(true);
		//$footer->setText("Footer Text");
		//$footer->setHtml("<html><body>Footer Text</body></html>");
		//$mail_settings->setFooter($footer);

		////Spam check setting
		//$spam_check = new SendGrid\SpamCheck();
		//$spam_check->setEnable(true);
		//$spam_check->setThreshold(1);
		//$spam_check->setPostToUrl("https://spamcatcher.sendgrid.com");
		//$mail_settings->setSpamCheck($spam_check);

		////Defines mail settings
		//$mail->setMailSettings($mail_settings);

		#endregion

		#region Tracking configuration sample

		////Tracking samples
		//$tracking_settings = new SendGrid\TrackingSettings();

		//$click_tracking = new SendGrid\ClickTracking();
		//$click_tracking->setEnable(true);
		//$click_tracking->setEnableText(true);
		//$tracking_settings->setClickTracking($click_tracking);

		//$open_tracking = new SendGrid\OpenTracking();
		//$open_tracking->setEnable(true);
		//$open_tracking->setSubstitutionTag("Optional tag to replace with the open image in the body of the message");
		//$tracking_settings->setOpenTracking($open_tracking);

		//$subscription_tracking = new SendGrid\SubscriptionTracking();
		//$subscription_tracking->setEnable(true);
		//$subscription_tracking->setText("text to insert into the text/plain portion of the message");
		//$subscription_tracking->setHtml("<html><body>html to insert into the text/html portion of the message</body></html>");
		//$subscription_tracking->setSubstitutionTag("Optional tag to replace with the open image in the body of the message");
		//$tracking_settings->setSubscriptionTracking($subscription_tracking);

		////Google Analytics tracking settings sample
		//$ganalytics = new SendGrid\Ganalytics();
		//$ganalytics->setEnable(true);
		//$ganalytics->setCampaignSource("some source");
		//$ganalytics->setCampaignTerm("some term");
		//$ganalytics->setCampaignContent("some content");
		//$ganalytics->setCampaignName("some name");
		//$ganalytics->setCampaignMedium("some medium");
		//$tracking_settings->setGanalytics($ganalytics);

		////Applying tracking settings
		//$mail->setTrackingSettings($tracking_settings);

		#endregion

		////echo json_encode($mail, JSON_PRETTY_PRINT), "\n";
		//return $mail;
	}

	/**
	 * Generates a simple sequence for salt
	 * @param mixed $length
	 * @param mixed $simple
	 * @return string
	 */
	public static function generateRandom($length=16, $simple=false){
        $result='';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()+=-_?~';
        if($simple)
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        for ($i = 0; $i < $length; $i++)
            $result .= $chars[mt_rand(0, strlen($chars)-1)];
        return $result;
    }
}
