<?php

/**
 * AuthorizedSession short summary.
 *
 * AuthorizedSession description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class AuthorizedSession
{
	const SESSION_USER = '#USER_$LOGGED%';
	public static function start(CurrentUser $user=null){
		$_SESSION[self::SESSION_USER] = serialize(!is_null($user) ? $user : CurrentUser::get());		
	}
	/**
	 * Summary of get
	 * @return CurrentUser
	 */
	public static function get(){
		if(!isset($_SESSION[self::SESSION_USER])) return false;
		return unserialize($_SESSION[self::SESSION_USER]);
	}
}
