<?php

/**
 * CurrentUser short summary.
 *
 * CurrentUser description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class CurrentUser
{
	/**
	 * Current User Object
	 * Singleton object
	 * @var CurrentUser
	 */
	private static $instance;

	/**
	 * user object
	 * @var ModelUser
	 * @property-read
	 * @property-write
	 */
	public $user;
	/**
	 * user profile object
	 * @var ModelUserProfile
	 * @property-read
	 * @property-write
	 */
	public $userProfile;

	/**
	 * User related subsidiaries
	 * @var Array
	 */
	//public $subsidiaries;
	/**
	 * User related companies
	 * @var Array
	 */
	//public $companies;
	/**
	 * User allowed routes
	 * @var Array
	 */
	public $routes;
	/**
	 * Keeps the object arrays for companies and subsidiaries
	 */
	//public $objects;


	private $current;

	const SESSION_NAME = 'current-user';
    /**
     * Private constructor to make singleton possible
     */
    private function __construct(){
	}

	/**
	 * Gets the singleton object from CurrentUser
	 * @return CurrentUser
	 */
	public static function get(){
		if(self::$instance == null){
			$session =  AuthorizedSession::get();
			self::$instance = !$session ? new CurrentUser() : $session ;
		}
		return self::$instance;
	}
	/**
	 * @return null|integer
	 */
	// public function getCurrentCompany() {
	// 	return $this->current->company;
	// }
	// public function getCurrentCompanyObject() {
	// 	return $this->objects->current->company;
	// }
	/**
	 * @return null|integer
	 */
	// public function getCurrentSubsidiary() {
	// 	return $this->current->subsidiary;
	// }
	// public function getCurrentSubsidiaryObject() {
	// 	return $this->objects->current->subsidiary;
	// }
	// public function setCurrentCompany($company){
	// 	if(!in_array($company,$this->companies)) return false;

	// 	foreach($this->objects->companies as $companyObject){
	// 		if($companyObject->id!=$company) continue;
	// 		$this->current->company = $company;
	// 		$this->objects->current->company = $companyObject;
	// 	}


	// 	foreach($this->objects->subsidiaries as $subsidiary){
	// 		if($subsidiary->id_empresa==$company){
	// 			$this->current->subsidiary = $subsidiary->id;
	// 			self::$instance->objects->current->subsidiary = $subsidiary;
	// 			break;
	// 		}
	// 	}
	// 	AuthorizedSession::start(self::$instance);
	// }

	// public function setCurrentSubsidiary($subsidiary){
	// 	if(!in_array($subsidiary,$this->subsidiaries)) return false;

	// 	foreach(self::$instance->objects->subsidiaries as $sub){
	// 		if($sub->id_empresa==self::$instance->current->company && $sub->id==$subsidiary){
	// 			$this->current->subsidiary=$subsidiary;
	// 			$this->objects->current->subsidiary = $sub;
	// 			AuthorizedSession::start(self::$instance);
	// 			return true;
	// 		}
	// 	}
	// 	return false;
	// }

	/**
	 * Summary of set
	 * @param ModelUserProfile $profile
	 */
	public static function set(ModelUser $authUser){
		self::$instance = new CurrentUser();
		self::$instance->user = $authUser;
		self::$instance->userProfile = $authUser->Profile;
		self::$instance->routes = [];
		// self::$instance->subsidiaries = [];
		// self::$instance->companies = [];
		// self::$instance->objects = new StdClass();
		// self::$instance->objects->companies = [];
		// self::$instance->objects->subsidiaries = [];
		// self::$instance->objects->current = new StdClass();
		// self::$instance->objects->current->company = null;
		// self::$instance->objects->current->subsidiary = null;
		// $permissoes_rota = (new ViewPermissoesRotas())->getByIdPessoa(intval($perfil->id_pessoa));
		// $modelFilial = new ModelEmpresaFilial();
		// $modelEmpresa = new ModelEmpresa();
		//foreach($permissoes_rota as $rota){
		//	self::$instance->routes[] = $rota;

			// if(!in_array($rota->id_empresa_filial,self::$instance->subsidiaries)) {
			// 	self::$instance->subsidiaries[] = $rota->id_empresa_filial;
			// 	self::$instance->objects->subsidiaries[] = $modelFilial->records()->getFirstStandard($rota->id_empresa_filial);
			// }
			// if(!in_array($rota->id_empresa,self::$instance->companies)) {
			// 	self::$instance->companies[] = $rota->id_empresa;
			// 	self::$instance->objects->companies[] = $modelEmpresa->records()->getFirstStandard($rota->id_empresa);
			// }
		//}

		// self::$instance->current = new StdClass();
		// self::$instance->current->company = null;
		// self::$instance->current->subsidiary = null;
		// if(isset(self::$instance->companies[0])){
		// 	self::$instance->setCurrentCompany(self::$instance->companies[0]);
		// }

		AuthorizedSession::start(self::$instance);
	}
}
