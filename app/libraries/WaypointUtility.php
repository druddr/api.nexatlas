<?php
/**
 * Provides waypoint manipulation tooling
 *
 * @author bruno.teixeira.silva
 * @version 1.0
 */
abstract class WaypointUtility {
	/**
	 * North Earth sector
	 */
	const SectorN = "N";
	/**
	 * South Earth sector
	 */
	const SectorS = "S";
	/**
	 * West Earth sector
	 */
	const SectorW = "W";
	/**
	 * East Earth sector
	 */
	const SectorE = "E";
	/**
	 * RegEx for matching DMS patterns
	 */
	const RegExDegreesMinutesSeconds = "/(\d){2}\s(\d){2}\s(\d){2}\s([NS]){1}\s\/\s(\d){2,3}\s(\d){2}\s(\d){2}\s([EW]){1}/i";

	/**
	 * Casts a model of string with degrees, minutes and seconds + sector to decimal degrees:
	 * E.g.:
	 * @param mixed $degrees
	 * @param mixed $minutes
	 * @param mixed $seconds
	 * @param mixed $sector The sector it refers to
	 * @return array|double|integer
	 */
	public static function degreeMinutesSecondsToDecimalDegrees($degrees, $minutes, $seconds, $sector)
	{
		// Converting DMS ( Degrees / minutes / seconds ) to decimal format
		$result = $degrees + ((($minutes * 60) + ($seconds)) / 3600);

		//Uppercase the sector
		$sector = strtoupper(trim($sector));

		//If the sector is not north nor east
		return $sector == WaypointUtility::SectorS || $sector == WaypointUtility::SectorW ? -$result : $result;
	}

	/**
	 * Casts decimal degrees to a formatted string of degrees, minutes and seconds + sector:
	 * E.g.: 17 03 50 N
	 * @param mixed $decimalDegree
	 * @param mixed $latitude
	 * @return string
	 */
	public static function decimalDegreesToDegreeMinutesSeconds($decimalDegree, $latitude=true)
	{
		// Converts decimal format to DMS ( Degrees / minutes / seconds )
		$vars = explode(".", $decimalDegree);
		$deg = $vars[0];
		$tempma = "0.".$vars[1];

		$tempma = $tempma * 3600;
		$min = floor($tempma / 60);
		$sec = $tempma - ($min*60);

		//Decides which sector it comes from
		$sector =
			($latitude ?
				(
					($decimalDegree >= 0) ?
						WaypointUtility::SectorN :
						WaypointUtility::SectorS
				) :
				(
					($decimalDegree >= 0) ?
						WaypointUtility::SectorE :
						WaypointUtility::SectorW
				)
			);

		return ($deg . " " . $min . " " . $sec . " " . $sector);
	}

	/**
	 * Casts a single waypoint to a GeoJson Feature object
	 * @param ModelWaypoint|ModelRouteWaypoint $waypoint The waypoint to be cast
	 * @return array A feature object
	 */
	public static function convertWaypointToGeoJson($waypoint) {

		//Casting types to be used by interface
		$waypoint->id = isset($waypoint->id) ? intval($waypoint->id) : null;
		$waypoint->latitude = doubleval($waypoint->latitude);
		$waypoint->longitude = doubleval($waypoint->longitude);
		$waypoint->Type->id = isset($waypoint->Type->id) ? intval($waypoint->Type->id) : null;

		$feature = [
			'type' => GeoJSONTypeProxy::Feature,
			'geometry' => [
				'type' => GeoJSONTypeProxy::Point,
				'coordinates' => [ $waypoint->longitude, $waypoint->latitude ]
			],
			'properties' => [
				'id' => intval($waypoint->id),
				'code' => $waypoint->code,
				'name' => $waypoint->name,
				'latitude' => doubleval($waypoint->latitude),
				'longitude' => doubleval($waypoint->longitude),
				'waypoint_type_id' => intval($waypoint->Type->id),
				'waypoint_type_name' => $waypoint->Type->name,
				'waypoint_type_code_name' => $waypoint->Type->code_name,
				'waypoint_type_icon' => $waypoint->Type->icon,
				'icao' => $waypoint->icao_code,
				'iata' => $waypoint->iata_code,
				'frequency' => $waypoint->frequency,
				'frequency_type' => $waypoint->frequency_type
			]
		];

		return $feature;
	}

	/**
	 * Converts a collection of waypoints to a FeatureCollection object
	 * @param array $waypointCollection the collection of waypoints to transform
	 * @return array of Features (FeatureCollection)
	 */
	public static function convertWaypointCollectionToGeoJson($waypointCollection) {

		//Preparing the feature collection for receiving objects
		$featureCollection = [
			'type' => GeoJSONTypeProxy::FeatureCollection,
			'features' => []
			];

		foreach ($waypointCollection as $waypoint) {
			//Casts the current waypoint to GeoJson
			$feature = WaypointUtility::convertWaypointToGeoJson($waypoint);

			//Adds it to the feature collection
			array_push($featureCollection['features'], $feature);
		}

		return $featureCollection;
	}

}
