<?php
/* Template engine
 * Usage sample
 * --
 * Create instance
	$template = new Template('app/designs/emails/AccountCreationEmail.php');
 * Set each variable to be used within
	$template->EmailConfirmationAppUrl = config::$EmailConfirmationAppUrl;
	$template->user = $this;
 * gets the rendered HTML as string
	$rendered_template = $template->render();
*/
class Template
{
    protected $vars = [];

    public function __construct($template)
    {
        $this->template = $template;
    }

    public function render()
    {
        if (!file_exists($this->template)) {
            throw new Exception("No template found at location: {$this->template}");
        }

		$output = file_get_contents($this->template);

		foreach ($this->vars as $key => $value) {
			$tagToReplace = "{@$key}";
			$output = str_replace($tagToReplace, $value, $output);
		}

		return $output;
    }

    public function __set($name, $value)
    {
        $this->vars[$name] = $value;
    }

    public function __get($name)
    {
        return $this->vars[$name];
    }
}
