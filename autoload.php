<?php
require_once("app/Config.php");
require_once(config::$SystemBasePath."system/library/functions.php");

spl_autoload_register(function ($class_name){
	$dash_class_name = camelToDash($class_name);
	$locations = array(
		config::$SystemBasePath.'system/library/',
		config::$SystemBasePath.'system/interfaces/',
		config::$SystemBasePath.'system/types/',
		config::$SystemBasePath.'system/core/',
		config::$SystemBasePath.'system/database/',
		config::$SystemBasePath.'system/exceptions/',
		config::$BasePath.'app/libraries/',
		config::$BasePath.'app/business/entities/',
		config::$BasePath.'app/business/models/',
		config::$BasePath.'app/business/actions/',
		config::$BasePath.'app/business/rules/',
		config::$BasePath.'app/controllers/',
		config::$BasePath.'app/controllers/sistema/',
		config::$BasePath.'app/controllers/admin/',
		config::$BasePath.'app/controllers/gcm/',
		config::$BasePath.'app/util/', //Template engine pre-processor
		config::$BasePath.'app/designs/emails/pt_br', //Template engine layouts (pt-BR)
		config::$BasePath.'app/models/views/',
		config::$BasePath.'app/components/'.$dash_class_name.'/',
		config::$BasePath.'app/libraries/'.$dash_class_name.'/',
		config::$BasePath.'app/libraries/geojson/',
		config::$BasePath.'app/models/',
		config::$BasePath.'app/'
	);
	foreach($locations as $location){
		if(file_exists($location.$class_name.'.php')){
			require_once($location.$class_name.'.php');
			return;
		}
	}
});
