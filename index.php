<?php
date_default_timezone_set('America/Sao_Paulo');
require_once __DIR__ . '/vendor/autoload.php';
include("autoload.php");

use Firebase\JWT\JWT;

function auth()
{
    /* mock Authorization: Token eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
	mock response:
	stdClass Object
	(
	[sub] => 1234567890
	[name] => John Doe
	[admin] => 1
	)
	 */
    // token signature
    $authKey = "secret";
    $authKeyword = "Token";
    //$authHeader = $_SERVER["HTTP_AUTHORIZATION"];
    $authHeader = $_SERVER["Authorization"];

	print_r($authHeader);

    if (!$authHeader) {
        // no header found
		// raise 403
		//This will set http responde code directly
		http_response_code(403);
		exit;
    }

    $authHeader = explode(" ", $authHeader);

	try {
        $requestAuthKeyWord = $authHeader[0];
        $requestAuthToken = $authHeader[1];
    }
	catch (Exception $e) {
        // invalid token header format
		// raise 403;
		//This will set http responde code directly
		//http_response_code(403);
		exit;
    }

    if ($requestAuthKeyWord !== $authKeyword) {
        // token auth keyword does not match
        // raise 403;
        //This will set http responde code directly
		//http_response_code(403);
		exit;
    }

    try {
        $decodedToken = Firebase\JWT\JWT::decode($requestAuthToken, $authKey, array('HS256'));
    }
	catch (Exception $e) {
        // decoding token error
		// raise 403;
		//This will set http responde code directly
		//http_response_code(403);
		exit;
    }

    // print_r(json_encode($decodedToken));

    // gets the user that will access the resource / endpoint
}

if (strtolower($_SERVER['REQUEST_METHOD'])=='options') {
    session_start();
    session_regenerate_id();
    exit;
}

// auth();

TypedHint::init();

if (strpos($_SERVER['REQUEST_URI'], "EntityMapper")) {
    \EntityMapper::init();
} else {
    \ExceptionsHandler::init();
    \Router::getInstance()->findRoute();
}
