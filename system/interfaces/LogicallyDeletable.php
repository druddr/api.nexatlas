<?php
/**
 * LogicallyDeletable is an interface for objects which shouldn't be removed phisycally from DB
 */
interface LogicallyDeletable {
	/**
	 * Implement a logical exclusion function "delete" when implementing LogicallyDeletable
	 */
	public function delete();
}
