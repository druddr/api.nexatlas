<?php
/**
 * CustomExtractable is an interface for objects which should have a
 * Custom extract method
 */
interface CustomExtractable {
	/**
	 * Implement an extraction method for objects implementing this interface
	 */
	public function extract();
}
