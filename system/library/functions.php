<?php
function map_objects($object){
	$isString = is_string($object);
	if($object===null) return $object;
	if(is_scalar($object) && !$isString){
		return $object;
	}
	if ($isString) {
		return $object;
	}
	if(is_array($object)){
		foreach ($object as $property => $value) {
			$object[$property] = map_objects($value);
		}
		return $object;
	}
	if(is_subclass_of($object,"EntityCollection")|| get_class($object)=="EntityCollection"){
		$object = $object->toArray();
	}
	if(is_subclass_of($object,"Model")){
		$object = $object->extract();
	}
	foreach ((array)$object as $property => $value) {
		$object->{$property} = map_objects($value);
	}
	unset($isString);
	return $object;
}
function utf8_encode_all($object){
	$isString = is_string($object);
	if($object===null) return $object;
	if(is_scalar($object) && !$isString){
		return $object;
	}
	if ($isString) {
		return utf8_encode($object);
	}
	if(is_array($object)){
		foreach ($object as $property => $value) {
			$object[$property] = utf8_encode_all($value);
		}
		return $object;
	}
	if(is_subclass_of($object,"EntityCollection")|| get_class($object)=="EntityCollection"){
		$object = $object->toArray();
	}
	if(is_subclass_of($object,"Model")){
		$object = $object->extract();
	}
	foreach ((array)$object as $property => $value) {
		$object->{$property} = utf8_encode_all($value);
	}
	unset($isString);
	return $object;
}
function utf8_decode_all($object){
	$isString = is_string($object);
	if($object===null) return $object;
	if(is_scalar($object) && !$isString){
		return $object;
	}
	if ($isString) {
		return utf8_decode($object);
	}
	if(is_array($object)){
		foreach ($object as $property => $value) {
			$object[$property] = utf8_decode_all($value);
		}
		return $object;
	}
	foreach ((array)$object as $property => $value) {
		$object->{$property} = utf8_decode_all($value);
	}
	unset($isString);
	return $object;
}

function camelToDash($string) {
    return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $string));
}
function dashToCamel($string, $pascal = false) {
	if(strlen($string)==0) return '';
	$string = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
	if (!$pascal)  $string[0] = strtolower($string[0]);
    return $string;
}
function dashToPascal($string){
	return dashToCamel($string,true);
}
function sign( $number ) {
    return ( $number > 0 ) ? 1 : ( ( $number < 0 ) ? -1 : 0 );
}
