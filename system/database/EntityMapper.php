<?php

class EntityMapper
{

	public static function init(){

		//EntityMapper::checkAuthentication();

		$uri = new Uri(/*null , Uri::LOWER_CASE*/);
		switch($uri->verb){
			case 'get':
				return new EntityReader($uri);
			case 'post':
				return new EntityWritter($uri);
		}
		return null;
	}

	public static function checkAuthentication(){
		//Session will last a day
		//But it refreshes every time session_regenerate_id() is called
		//So it last a day with no activity on this session
		ini_set('session.cookie_lifetime', 60 * Config::SessionTimeout);
		ini_set('session.gc_maxlifetime', 60 * Config::SessionTimeout);
		session_start();
		session_regenerate_id();
		$authorizationClass = config::AuthorizationController;
		$authorizationMethod = config::AuthorizationMethod;
		(new $authorizationClass(Router::getInstance()))->{$authorizationMethod}();
	}
}