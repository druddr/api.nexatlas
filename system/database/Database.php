<?php

/**
 * Database short summary.
 *
 * Database description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Database
{
	/**
	 * @var array(PDO)
	 */
	private static $connections=array();
	public $last_query;
	public $last_insert_id;
	private $current_connection_id=NULL;
	public function __construct(){

	}
	/**
	 * Summary of pdo
	 * @param mixed $connection_id
	 * @return PDO
	 */
	private function pdo($connection_id=false){
		$id = $connection_id===false ? $this->current_connection_id : $connection_id;
		return self::$connections[$id];
	}
	/**
	 * Summary of connect
	 * @param mixed $host
	 * @param mixed $db
	 * @param mixed $user
	 * @param mixed $pass
	 * @param mixed $driver
	 * @return string
	 */
	public function connect($host=false,$db=false,$user=false,$pass=false,$driver="mysql"){
		$host = !$host ? config::$Database['host'] : $host;
		$db = !$db? config::$Database['db'] : $db;
		$user = !$user ? config::$Database['user'] : $user ;
		$pass = !$pass ? config::$Database['pass'] : $pass;
		//$dsn = $driver.":dbname={$db};host={$host};";
		$dsn = $driver.":dbname={$db};host={$host};charset=utf8";
		$this->current_connection_id = md5($host.$db.$user.$pass);
		if(isset(self::$connections[$this->current_connection_id])) return $this->current_connection_id;
		//self::$connections[$this->current_connection_id] = new PDO($dsn,$user,$pass);
		self::$connections[$this->current_connection_id] = new PDO($dsn,$user,$pass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
		return $this->current_connection_id;
	}
	/**
	 * Summary of getConnection
	 * @param mixed $connection_id
	 * @return null|PDO
	 */
	public static function getConnection($connection_id){
		return isset(self::$connections[$connection_id]) ? self::$connections[$connection_id] : null;
	}
	/**
	 * Summary of query
	 * @param mixed $sql
	 * @param mixed $model
	 * @param mixed $connection_id
	 * @throws Exception
	 * @return PDOStatement
	 */
	public function query($sql, $model=false, $connection_id=false) {
		//This was made necessary given the fact the DBMS was applying an encode function
		//Thus, causing the encoding errors; Here we cheat on the encoding by decoding
		//its UTF8 charset content
		//$sql = utf8_decode_all($sql);
		$this->pdo($connection_id)->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->pdo($connection_id)->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

		$sql = config::$UTF8EncodeSQL ? utf8_encode($sql) : $sql;
		$this->last_query=$sql;
		if($model) {
			$query = $this->pdo($connection_id)->query($sql, PDO::FETCH_CLASS , get_class($model),array());
		}
		else{
			$query = $this->pdo($connection_id)->query($sql, PDO::FETCH_ASSOC);
		}
		if(!$query) {
			$mensagem = $sql."\r\n\r\n".print_r($this->pdo($connection_id)->errorInfo(),true);
			throw new Exception($mensagem);
		}

		//print_r($query);

		$this->last_insert_id = $this->pdo($connection_id)->lastInsertId();
		return $query;
	}
	/**
	 * Summary of beginTransaction
	 * @param mixed $connection_id
	 */
	public function beginTransaction($connection_id=false){
		if($this->pdo($connection_id)->inTransaction()){
			return false;
		}
		$this->pdo($connection_id)->beginTransaction();
		return true;
	}
	/**
	 * Summary of commit
	 * @param mixed $connection_id
	 */
	public function commit($connection_id=false){
		$this->pdo($connection_id)->commit();
	}
	/**
	 * Summary of rollBack
	 * @param mixed $connection_id
	 */
	public function rollBack($connection_id=false){
		$this->pdo($connection_id)->rollBack();
	}
	/**
	 * Summary of getDriver
	 * @param string $driver
	 * @return ISQLDriver
	 */
	public static function getDriver($driver){
		$driverclass = ucfirst(strtolower($driver))."Driver";
		include(dirname(__FILE__).'/drivers/'.$driver.'/'.($driverclass)).'.php';
		return new $driverclass();
	}

	public function __destruct(){
		//self::$connections = null;
		unset($this->connection);
	}
}
