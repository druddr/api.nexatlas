<?php

/**
 * ActiveRecord short summary.
 *
 * ActiveRecord description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ActiveRecord {
	private $select=array();
	private $join=array();
	private $where=array();
	private $order=array();
	private $group=array();
	private $having=array();
	private $limit=null;
	private $offset=null;
	private $count = null;
	private $sum = null;
	private $wherebracketscounter=0;
	private $connection_id=false;
	/**
	 * Contains the database connection stored
	 * @var Database
	 */
	private $db;
	private $model;

	private $callbacks = [];


	public $last_sql;
	/**
	 * This constructor will set the properties of this model in case $properties param is set.
	 * Also, the connection with database is started here.
	 * *** Database does not connect 2 times at the same database, so there is no problem calling connect
	 * multiple times
	 * @param array $properties This array must contain the keys with the same name as this model properties
	 */
	public function __construct(Model $model){
		$this->model = $model;
		$this->db = new Database();
		$this->db->connect();
		//TODO - Remove this once all the queries are optimized to the new MYSQL
		$this->db->query("SET SQL_MODE='';");
	}
	/**
	 * Create a differente connection. Database class will judge if connection is diferent or not
	 * @param mixed $host
	 * @param mixed $db
	 * @param mixed $user
	 * @param mixed $pass
	 * @param mixed $driver
	 * @return ActiveRecord For chain methods
	 */
	public function custom_connection($host=false,$db=false,$user=false,$pass=false,$driver="mysql"){
		$db = new Database();
		$this->connection_id = $db->connect($host,$db,$user,$pass,$driver);
		return $this;
	}
	/**
	 * Summary of checkFieldToJoin
	 * @param mixed $field
	 * @return mixed
	 */
	private function checkFieldToJoin($field,$alias=null){
		$source = $field;
		$config = isset($this->model->field_config[$field]) ? $this->model->field_config[$field]: false;
		if( $config
		&& isset( $config['belongsTo'] )
		&& !isset($this->join[ $config['belongsTo']['table'] ]) ){
			$this->join($config['belongsTo']['table'],$config['belongsTo']['on']/*TODO: decide whether to use LEFT for the JOIN, here*/, 'LEFT');
		}
		if(( $config )
		&& isset( $config['belongsTo'] )) {
			if(isset($config['belongsTo']['field'])){
				$config['alias'] = $field;
				$field = $config['belongsTo']['field'];
			}
			$source = str_replace($config['belongsTo']['table'],"",$field);
			$source = $config['belongsTo']['table'].".".$field;
		}
		if(($config) && !isset($config['belongsTo'])){
			$source = str_replace($this->model->table_name,"",$field);
			$source = $this->model->table_name.".".$field;
		}
		if(isset($config['alias'])){
			$source = $source." as ".$config['alias'];
		}
		if(!is_null($alias)){
			$source = $source." as ".$alias;
		}
		return $source;
	}
	/**
	 * Summary of getSql
	 * @return string
	 */
	private function getSql(){
		$sql = "SELECT ";
		$select = [];
		if(count($this->select)==0) $this->select = array_keys($this->model->field_config);
		foreach($this->select as $k=>$field){
			$real_field = is_numeric($k) ? $field : $k;
			$alias = is_numeric($k) ? null : $field;
			$select[$k] = $this->checkFieldToJoin($real_field,$alias);
		}
		$sql .= implode("\r\n , ",$select);
		$sql .= "\r\n FROM {$this->model->table_name} ";

		foreach($this->join as $table=>$on){
			if (!isset($on[0]) && !isset($on[1])) {
				$sql .= "\r\n JOIN {$table} ";
				continue;
			}

			$sql .= "\r\n {$on[1]} JOIN {$table} ON ";
			$ons = array();
			foreach($on[0] as $field1=>$field2){
				$ons[] = " {$field1} = {$field2}";
			}
			$sql .= implode(" AND ",$ons);
		}

		$this->getWhereSql($sql);

		if(count($this->group)>0) $sql .= "\r\n GROUP BY ".implode(",",$this->group);

		$this->getHavingSql($sql);

		if(count($this->order)>0) {
			$sql .= "\r\n ORDER BY ";
			foreach($this->order as $field=>$type){
				$sql .=  " {$field} {$type} ,";
			}
			$sql = substr($sql,0,-1);
		}
		if($this->limit!=null) $sql .=  "\r\n LIMIT {$this->limit} ";
		if($this->offset!=null) $sql .=  "\r\n OFFSET {$this->offset} ";
		$this->last_sql = $sql;
		return $sql;

	}

	/**
	 * Builds the where clause
	 * @param mixed $sql the sql to have a where clause attached to it
	 */
	private function getWhereSql(&$sql){
		//$is_next_or = false;
		$is_current_or = false;
		$and_not_where = false;
		foreach($this->where as $a=>$statements){
			$is_current_or = !is_array($statements);

			if($is_current_or){
				$sql .= "\r\n OR  ";
				continue;
			}
			else {
				if(!$and_not_where){
					$sql .= "\r\n WHERE ";
				}
				else if(count($statements)>0){
					if(isset($this->where[$a-1]) && $this->where[$a-1]!= " OR "){
						$sql .= "\r\n AND ";
					}
				}
				$and_not_where = true;
			}
			if(count($statements)==0) continue;
			$sql .= "(";
			foreach($statements as $k=>$where){
				//Here it is all AND Statements
				if(isset($statements[$k-1]) && $statements[$k-1]!= " OR "){
					if(isset($where['between'])){
						$where[4] = ' AND ';
					}
					$sql .= "\r\n  ".$where[4]." ";
				}
				if(isset($where['between'])){
					$not = $where['between'][3] ? '' : ' NOT ';
					if(!isset($this->model->field_config[$where['between'][1]])) $where['between'][1] = " '".$where['between'][1]."' ";
					if(!isset($this->model->field_config[$where['between'][2]])) $where['between'][2] = " '".$where['between'][2]."' ";
					$sql .= " {$where['between'][0]} {$not} BETWEEN {$where['between'][1]} AND {$where['between'][2]} ";
					continue;
				}
				$field = explode(" ",trim($this->checkFieldToJoin($where[0])))[0];
				$value = $where[1];
				//Adds space from each side of the operator
				$equalchar = strtoupper(" {$where[2]} ");
				//If it is a string value
				$value = $where[3] ? "'".addslashes($value)."'" : $value;
				//Concats the comparison
				$sql .= $field.$equalchar.$value." ";
			}

			$sql .=")";
		}
	}

	/**
	 * Builds the having clause
	 * @param mixed $sql the sql to have a having clause attached to it
	 */
	private function getHavingSql(&$sql){
		////$is_next_or = false;
		//$is_current_or = false;
		//$and_not_having = false;
		foreach($this->having as $a=>$statements){
			//$is_current_or = !is_array($statements);

			//if($is_current_or){
			//    $sql .= "\r\n OR  ";
			//    continue;
			//}
			//else {
			//    if(!$and_not_having){
			//        $sql .= "\r\n HAVING ";
			//    }
			//    else if(count($statements)>0){
			//        if(isset($this->having[$a-1]) && $this->having[$a-1]!= " OR "){
			//            $sql .= "\r\n AND ";
			//        }
			//    }
			//    $and_not_having = true;
			//}
			if(count($statements) == 0) continue;

			if (!strpos($sql, 'HAVING'))
				$sql .= "\r\n HAVING ";

			//$sql .= "(";
			//foreach($statements as $k=>$having){
			//Here it is all AND Statements
			//if(isset($statements[$k-1]) && $statements[$k-1] != " OR "){
			//if(isset($having['between'])){
			//    $having[4] = ' AND ';
			//}
			//	$sql .= "\r\n  ".$having[4]." ";
			//}
			//if(isset($having['between'])){
			//    $not = $having['between'][3] ? '' : ' NOT ';
			//    if(!isset($this->model->field_config[$having['between'][1]])) $having['between'][1] = " '".$having['between'][1]."' ";
			//    if(!isset($this->model->field_config[$having['between'][2]])) $having['between'][2] = " '".$having['between'][2]."' ";
			//    $sql .= " {$having['between'][0]} {$not} BETWEEN {$having['between'][1]} AND {$having['between'][2]} ";
			//    continue;
			//}
			//$field = explode(" ",trim($this->checkFieldToJoin($having[0])))[0];
			//$value = $having[1];
			$value = $statements[1];
			//Adds space from each side of the operator
			//$equalchar = strtoupper(" {$having[2]} ");
			$equalchar = strtoupper(" {$statements[2]} ");
			//If it is a string value
			//$value = $having[3] ? "'".addslashes($value)."'" : $value;
			$value = $statements[3] ? "'".addslashes($value)."'" : $value;
			//Concats the comparison
			//$sql .= $field.$equalchar.$value." ";
			$sql .= $statements[0].$equalchar.$value." ";
			//}

			//$sql .=")";
		}
	}

	public function getSelectedFields(){
		return $this->select;
	}
	public function isGrouped(){
		return count($this->group) > 0 || count($this->count);
	}
	public function select($fields){
		if(func_num_args()>1) $fields = func_get_args();
		$this->select = is_array($fields) ? $fields : [$fields];
		return $this;
	}
	public function clearSelect(){
		$this->select = [];
	}
	public function addSelect($fields){
		if(func_num_args()>1) $fields = func_get_args();

		if(is_array($fields)){
			foreach($fields as $k=>$field){
				$this->select[$k] = $field;
			}
		}
		else{
			$this->select[] = $fields;
		}
		return $this;
	}

	public function max($field, $alias=false){
		if(!$alias){
			$alias = "max_{$field}";
		}
		$this->select(["MAX({$field})"=>$alias]);
		return $this;
	}

	public function selectGrouped($fields){
		if(func_num_args()>1) $fields = func_get_args();
		$this->select = is_array($fields) ? $fields : [$fields];
		$this->group($this->select);
		return $this;
	}
	/**
	 * @param string $table Table name
	 * @param array $on array('field1'=>'field2') The fields must be inside $field_config, so its not necessary table alias on them
	 */
	public function join($table,array $on,$type='INNER'){
		$this->join[$table] = [$on,$type];
		return $this;
	}
	/**
	 * Adds a straightforward SQL join command (hand-written)
	 * @param string $table Table name
	 */
	public function joinSql($table){
		$this->join[$table] = [null,null];
		return $this;
	}

	/**
	 * @param mixed $field this param can be String or Array.
	 * @param mixed $value this param can be String or any scalar.
	 * @param mixed $operator this param can be String.
	 * @param mixed $string this param can be String.
	 * The fields must be inside $field_config, so its not necessary table alias on them
	 * If the field is configured on belongsto it will automatically join that table
	 * Array Format = array('Field'=>'Value')
	 */
	public function where($field,$value=null,$operator='=', $string=true,$statemenet='AND'){
		$field = is_object($field) ? (array)$field : $field;
		$value = is_object($value) ? (array)$value : $value;

		if(is_array($value)){
			$value = ' ('.implode(',',$value).') ';
		}

		if(is_array($field)){
			foreach($field as $key=>$value){
				$this->where[$this->wherebracketscounter][] = array($key,$value,$operator,$string,$statemenet);
			}
			return $this;
		}
		$this->where[$this->wherebracketscounter][] = array($field,$value,$operator,$string,$statemenet);
		return $this;
	}

    /**
	 * Inserts a where clause in case of valid boolean condition or a boolean function
	 * @param mixed $condition condition to whether the where clause will be added or not relies on
	 * @param mixed $field this param can be String or Array.
	 * @param mixed $value this param can be String or any scalar.
	 * @param mixed $operator this param can be String.
	 * @param mixed $string this param can be String.
	 * The fields must be inside $field_config, so its not necessary table alias on them
	 * If the field is configured on belongsto it will automatically join that table
	 * Array Format = array('Field'=>'Value')
	 * @return ActiveRecord
	 */
	public function whereIf($condition, $field, $value = null, $operator='=', $string = true){
        //Inserts the property only
        if ((is_callable($condition) && !call_user_func($condition)) || (is_bool($condition) && !$condition)) {
            return $this;
        }

        //Inserts the like condition
        return $this->where($field, $value, $operator, $string);
    }
	/**
	 * [#field, #value, #operator, #string, #statement]
	 * @return ActiveRecord
	 */
	public function whereMany(){
		$this->where[++$this->wherebracketscounter] = [];
		$or = false;
		foreach(func_get_args() as $argument){
			if($argument===true){
				$or = true;
				continue;
			}
			$string = isset($argument[3]) ? $argument[3] : true ;
			$operator = isset($argument[2]) ? $argument[2] : '=';
			$statement = isset($argument[4]) ? $argument[4] : 'OR';
			if(!$string && empty($argument[1])) continue;
			$this->where($argument[0], $argument[1], $operator, $string, $statement);
		}
		$this->where[++$this->wherebracketscounter] = $or ? ' OR ' :[];
		return $this;
	}
	/**
	 * Summary of likeThis
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function like($field,$value=null){
		$this->where($field, $value, 'LIKE', true);
		return $this;
	}

    /**
	 * Inserts a property in case of valid boolean condition or a boolean function
	 * @param mixed $condition
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
    public function likeIf($condition, $field, $value=null) {
        //Inserts the property only
        if ((is_callable($condition) && !call_user_func($condition)) || (is_bool($condition) && !$condition)) {
            return $this;
        }

        //Inserts the like condition
        return $this->like($field, $value);//->orThen();
    }

    /**
	 * Inserts a WHERE IN clause in case of valid boolean condition or a boolean function
	 * @param mixed $condition condition to whether the where clause will be added or not relies on
	 * @param mixed $field this param can be String or Array.
	 * @param mixed $array this param can be String or any scalar.
	 * @return ActiveRecord
	 */
	public function whereInIf($condition, $field, array $array = array()){
        //Inserts the property only
        if ((is_callable($condition) && !call_user_func($condition)) || (is_bool($condition) && !$condition)) {
            return $this;
        }

        //Inserts the like condition
        return $this->whereIn($field, $array);
    }

	/**
	 * Summary of whereIn
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function whereIn($field,array $array=array(),$statement='AND'){
		$this->where($field, $array, 'IN', false, $statement);
		return $this;
	}
	/**
	 * Summary of whereIn
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function whereNotInIf($condition,$field,array $array=array(),$statement='AND'){
		//Inserts the property only
        if ((is_callable($condition) && !call_user_func($condition)) || (is_bool($condition) && !$condition)) {
            return $this;
        }

		$this->where($field, $array, 'NOT IN', false, $statement);
		return $this;
	}
	/**
	 * Summary of whereIn
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function whereNotIn($field,array $array=array(),$statement='AND'){
		$this->where($field, $array, 'NOT IN', false, $statement);
		return $this;
	}
	/**
	 * Summary of lessThen
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function less($field,$value,$scape=false){
		$value = $scape ? "'{$value}'" : $value;
		$this->where($field, $value, ' < ', false);
		return $this;
	}

	/**
	 * Summary of higherThen
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function higher($field,$value,$scape=false){
		$value = $scape ? "'{$value}'" : $value;
		$this->where($field, $value, ' > ', false);
		return $this;
	}


	/**
	 * Summary of lessOrEqualThen
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function lessOrEqual($field,$value,$scape=false){
		$value = $scape ? "'{$value}'" : $value;
		$this->where($field, $value, ' <= ', false);
		return $this;
	}

	/**
	 * Summary of higherOrEqualThen
	 * @param mixed $field
	 * @param mixed $value
	 * @return ActiveRecord
	 */
	public function higherOrEqual($field,$value,$scape=false){
		$value = $scape ? "'{$value}'" : $value;
		$this->where($field, $value, ' >= ', false);
		return $this;
	}

	/**
	 * This method will open an OR statement. So, how this works:
	 * Example:
	 * $this->where("field_1","test_1")
	 *        ->where("field_2","test_2")
	 *        ->where(array("field_3"=>"test_3","field_4"=>"test_4"))
	 *        ->orThen()
	 *        ->where("field_2","test_3")
	 *        ->where(array("field_3"=>"test_1","field_4"=>"test_2"))
	 * SQL Generated
	 *	Select ....blabla.....
	 * Where
	 *		(field_1 = 'test_1' And field_2 = 'test_2' And field_3 = 'test_3' And field_4 = 'test_4')
	 *		OR
	 *		(field_2 = 'test_3' And field_3 = 'test_1' And field_4 = 'test_2')
	 */
	public function orThen(){
		$this->where[++$this->wherebracketscounter] = " OR ";
		$this->wherebracketscounter++;
		return $this;
	}
	/**
	 * This method will open an OR statement. Though, only in case the $condition is satisfied. Hence, how will it work:
	 * Example:
	 * $this->where("field_1","test_1")
	 *        ->where("field_2","test_2")
	 *        ->where(array("field_3"=>"test_3","field_4"=>"test_4"))
	 *        ->orThen()
	 *        ->where("field_2","test_3")
	 *        ->where(array("field_3"=>"test_1","field_4"=>"test_2"))
	 * SQL Generated
	 *	Select ....blabla.....
	 * Where
	 *		(field_1 = 'test_1' And field_2 = 'test_2' And field_3 = 'test_3' And field_4 = 'test_4')
	 *		OR
	 *		(field_2 = 'test_3' And field_3 = 'test_1' And field_4 = 'test_2')
	 * @param mixed $condition
	 */
    public function orThenIf($condition){

        //Inserts the property only
        if ((is_callable($condition) && !call_user_func($condition)) || (is_bool($condition) && !$condition)) {
            return $this;
        }

		return $this->orThen();
	}
	/**
	 * Summary of betweenThis
	 * @param mixed $field
	 * @param mixed $start
	 * @param mixed $end
	 * @return ActiveRecord
	 */
	public function between($field,$start,$end){
		$this->where[$this->wherebracketscounter][]['between'] = [$field,$start,$end,true];
		return $this;
	}
	/**
	 * Summary of notBetweenThis
	 * @param mixed $field
	 * @param mixed $start
	 * @param mixed $end
	 * @return ActiveRecord
	 */
	public function notBetween($field,$start,$end){
		$this->where[$this->wherebracketscounter][]['between'] = [$field,$start,$end,false];
		return $this;
	}

	/**
	 * Summary of count
	 * @param mixed $field
	 * @return ActiveRecord
	 */
	public function count($field=null, $alias=null){
		$field = is_null($field) ? $this->model->primary_key : $field;
		$alias = is_null($alias) ? "count_".$field : $alias;
		$field = "COUNT({$field})";
		$this->count[] = $field;
		$this->addSelect([$field=>$alias]);
		return $this;
	}
	/**
	 * Summary of count
	 * @param mixed $field
	 * @return ActiveRecord
	 */
	public function sum($field=null){
		$this->sum[] = is_null($field) ? $this->model->primary_key : $field;
		$newField = is_array($field) ? $field : [$field=>"sum_".$field];
		$this->addSelect($newField);
		return $this;
	}
	/**
	 * Summary of order
	 * @param mixed $field
	 * @param mixed $order
	 * @return ActiveRecord
	 */
	public function order($field, $order=null){
		if(is_array($field)){
			//numero=>asc
			//0=>numero
			foreach($field as $key=>$value){
				$field = is_numeric($key) ? $value : $key;
				$order = is_numeric($key) ? ' ASC ': $value;
				$this->order[$field] = $order;
			}
			return $this;
		}
		$this->order[$field] = $order;
		return $this;
	}
	/**
	 * Summary of group
	 * @param mixed $field
	 * @return ActiveRecord
	 */
	public function group($field) {
		if(is_array($field)){
			foreach($field as $value){
				$this->group[] = addslashes($value);
			}
			return $this;
		}
		$this->group[] = addslashes($field);
		return $this;
	}

	public function having($field, $value, $operator="=", $string=true, $statement='AND') {
		//if (is_array($field)) {
		//    foreach($field as $value)
		//}
		$this->having[] = [addslashes($field), addslashes($value), $operator, $string, $statement];
		return $this;
	}

	/**
	 * Summary of limit
	 * @param mixed $limit
	 * @return ActiveRecord
	 */
	public function limit($limit){
		$this->limit = $limit;
		return $this;
	}
	/**
	 * Summary of offset
	 * @param mixed $offset
	 * @return ActiveRecord
	 */
	public function offset($offset){
		$this->offset = $offset;
		return $this;
	}



	/**
	 * Gets a records from its class based on its ID or including a $query
	 * @param mixed $id the item ID
	 * @param mixed $query a query to search for an item
	 * @return ActiveRecord
	 */
	public function get($id=null,$query=array()){
		if($id!=null) $this->where($this->model->primary_key,$id);
		if(!empty($query)) $this->where($query);
		return $this;
	}
	/**
	 * Summary of toModelArray
	 * @param array $filter_properties
	 * @return Model[]
	 */
	public function toModelArray(array $filter_properties=array()){
		$return = array();
		foreach($this->db->query($this->getSql(),$this->model,$this->connection_id) as $object){
			if(!empty($filter_properties)) $this->filter($object,$filter_properties);
			$return[] = $object;
		}

		$this->resolveCallbacks($return);

		return $return;
	}
	/**
	 * Summary of toStandardArray
	 * @param array $filter_properties
	 * @return array
	 */
	public function toStandardArray(array $filter_properties=array()){
		$return = array();

		foreach($this->db->query($this->getSql(),$this->model->getStandardObject(),$this->connection_id) as $object){
			if(!empty($filter_properties)) $this->filter($object,$filter_properties);
			$return[] = $object;
		}

		$this->resolveCallbacks($return);

		return $return;
	}

	public function toEntityCollection(array $filter_properties=array()){
		$return = array();

		foreach($this->db->query($this->getSql(),$this->model,$this->connection_id) as $object){
			if(!empty($filter_properties)) $this->filter($object,$filter_properties);
			$return[] = $object;
		}
		$entityCollection = new EntityCollection($return);

		$this->resolveCallbacks($entityCollection);

		return $entityCollection;
	}

	/**
	 * Summary of getFirstModel
	 * @return Model
	 */
	public function getFirstModel($id=null){
		if($id!=null) $this->where($this->model->primary_key,$id);
		$query = $this->db->query($this->getSql(),$this->model,$this->connection_id);
		$object = $query->fetchObject(get_class($this->model));

		$this->model->fill($object);

		$this->resolveCallbacks($this->model);

		return $this->model;
	}
	/**
	 * Summary of getFirstStandard
	 * @return StdClass
	 */
	public function getFirstStandard($id=null){
		if($id!=null) $this->where($this->model->primary_key,$id);
		$query = $this->db->query($this->getSql(),$this->model->getStandardObject(),$this->connection_id);
		$result = $query->fetch();

		$this->resolveCallbacks($result);

		return $result;
	}
	/**
	 *
	 */
	public function delete($id=null,$query=null){
		if(!is_null($id)) $this->where($this->model->primary_key,$id);
		$sql = " DELETE FROM {$this->model->table_name} ";
		$this->getWhereSql($sql);
		return $this->db->query($sql,$this->model,$this->connection_id);
	}

	/**
	 * Summary of insert
	 * @return PDOStatement
	 */
	public function insert(){
		$sql = " INSERT INTO {$this->model->table_name} (";
		$fields = array();
		foreach($this->model->field_config  as $field=>$config){
			if($field==$this->model->primary_key
				&& CustomString::isNullOrEmpty($this->model->{$this->model->primary_key})
				&& $this->model->primary_key_is_auto_increment) continue;
			if(isset($this->model->field_config[$field]['belongsTo'])) continue;
			if(isset($this->model->field_config[$field]['hasOne'])) continue;
			if(isset($this->model->field_config[$field]['hasMany'])) continue;
			$fields[]= $field;
		}
		$sql .= implode(",",$fields);
		$sql .= ") VALUES (";
		$values = array();
		foreach($fields as $field){
			//Dealing with boolean types
			if($this->model->field_config[$field]['type']==Model::type_boolean || is_bool($this->model->{$field})){
				$values[] = $this->model->{$field} === null ? " null" : " " . ($this->model->{$field} ? "1" : "0");
			}
			//Dealing with time type
			else if ($this->model->field_config[$field]['type']==Model::type_time && is_int($this->model->{$field})) {
				$values[] = $this->model->{$field} === null ? " null" : " '" . addslashes(date("Y-m-d H:i:s", $this->model->{$field})) . "'";
			}
			//Other types
			else{
				$values[] = $this->model->{$field} === null ? " null" : " '" . addslashes($this->model->{$field}) . "'";
			}
		}
		$sql .= implode(",",$values);
		$sql .= ");";
		try {
			$r = $this->db->query($sql,$this->model,$this->connection_id);
		}
		catch(Exception $e){
			throw $e;
		}
		//var_dump($r->errorInfo());
		if($r!==false && !is_null($this->model->primary_key) && strlen($this->model->primary_key)>0){
			if($this->model->primary_key)
				$this->model->{$this->model->primary_key} = $this->db->last_insert_id;
		}

		return $r;
	}
	/**
	 * Summary of insertBatch
	 * [[1,'Testing',true],[1,'Testing',true]]
	 * @param mixed $fields
	 * @param mixed $batchValues
	 * @return bool|PDOStatement
	 */
	public function insertBatch($fields, $batchValues){
		$sql = " INSERT INTO {$this->model->table_name} (";
		$sql .= implode(",",$fields);
		$sql .= ") VALUES ";
		$valuesArray=[];
		foreach($batchValues as $values){
			$sqlValue = [];
			foreach($values as $value){
				if(is_bool($value)){
					$sqlValue[] = $value === null ? " null" : " " . ($value ? 1 : 0);
				}
				else{
					$sqlValue[] = $value === null ? " null" : " '" . addslashes($value) . "'";
				}
			}
			$valuesArray[] = "(".join(",",$sqlValue).")";
		}
		$sql .= implode(",",$valuesArray).";";
		try {
			$r = $this->db->query($sql,$this->model,$this->connection_id);
		}
		catch(Exception $e){
			throw $e;
		}

		return $r;
	}
	/**
	 * Summary of update
	 * @return PDOStatement
	 */
	public function update($custom_fields=false){
		$key = $this->model->{$this->model->primary_key};
		$sql = " UPDATE {$this->model->table_name} SET ";
		$fields = array();

		if(!$custom_fields) {
			foreach($this->model->field_config  as $field=>$config){
				if($field==$this->model->primary_key) continue;
				if(isset($config['belongsTo'])) continue;
				if(isset($config['hasOne'])) continue;
				if(isset($config['hasMany'])) continue;
				if($config['type']==Model::type_boolean){
					$fields[] = " {$field} = ". (CustomString::isNullOrEmpty($this->model->{$field}) ? "0" : addslashes(boolval($this->model->{$field}) ? "1" : "0"));
					//$fields[] = " {$field} = ". (!isset($this->model->{$field}) || !$this->model->{$field}) ? "0" : addslashes(addslashes(boolval($this->model->{$field}) ? "1" : "0"));
				}
				else if ($config['type']==Model::type_time && is_int($this->model->{$field})) {
					$fields[] = " {$field} = ". (CustomString::isNullOrEmpty($this->model->{$field}) ? "null" : "'" . addslashes(date("Y-m-d H:i:s", intval($this->model->{$field}))) . "'");
					continue;
				} else {
					$fields[] = " {$field} = ". (CustomString::isNullOrEmpty($this->model->{$field}) ? "null" : "'" . addslashes($this->model->{$field}) . "'");
				}
			}
		}
		else {
			foreach($custom_fields as $field=>$value){
				if($field==$this->model->primary_key) continue;
				if(isset($this->model->field_config[$field])){
					if(isset($this->model->field_config[$field]['belongsTo'])) continue;
					if(isset($this->model->field_config[$field]['hasOne'])) continue;
					if(isset($this->model->field_config[$field]['hasMany'])) continue;
					if($this->model->field_config[$field]['type']==Model::type_boolean){
						$fields[] = " {$field} = ". (CustomString::isNullOrEmpty($value) ? "0" : addslashes(boolval($value) ? "1" : "0"));
						continue;
					}
					else if ($this->model->field_config[$field]['type']==Model::type_time && is_int($value)) {
						$fields[] = " {$field} = ". (CustomString::isNullOrEmpty($value) ? "null" : "'" . addslashes(date('Y-m-d H:i:s', intval($value))) . "'");
						continue;
					} else {
						$fields[] = " {$field} = ". (CustomString::isNullOrEmpty($value) ? "null" : "'" . addslashes($value) . "'");
					}
				}
				else{
					$fields[] = " {$field} = ". (CustomString::isNullOrEmpty($value) ? "null" : "'" . addslashes($value) . "'");
				}
			}
		}
		//Joins the fields' data for the query
		$sql .= implode(",",$fields);
		if(count($this->where)==0) {
			if(!$this->model->primary_key_is_string) {
				$sql .= " WHERE {$this->model->primary_key} = {$key}";
			} else {
				$sql .= " WHERE {$this->model->primary_key} = '{$key}'";
			}
		}
		else{
			$this->getWhereSql($sql);
		}
		//Inserts the trailing semicolon
		$sql .= ";";

		$r = $this->db->query($sql,$this->model,$this->connection_id);

		return $r;
	}


	/**
	 * Summary of save
	 * @return bool|PDOStatement
	 */
	public function save(){
		$key = $this->model->{$this->model->primary_key};

		//print_r($this->model->primary_key);
		//print_r($key);
		//Register already exists
		//TODO: Make the update automatic for !primary_key_is_auto_increment Models
		//An idea is to implement a callback function on the non-standard PK at the Model class
		if($this->model->primary_key_is_auto_increment && $this->model->primary_key!=null && $key!==null && $key!=="" && $key!==0)
			return $this->update();

		$query= $this->insert();

		return $query;
	}
	/**
	 * Summary of filter
	 * @param mixed $object
	 * @param array $properties
	 */
	public function filter(&$object,array $properties){
		foreach(get_class_vars(get_class($object)) as $prop){
			if(property_exists($object,$prop)) continue;
			unset($object->{$prop});
		}
	}
	/**
	 * Summary of beginTransaction
	 */
	public function beginTransaction(){
		return $this->db->beginTransaction($this->connection_id);
	}
	/**
	 * Summary of commit
	 */
	public function commit(){
		return $this->db->commit($this->connection_id);
	}
	/**
	 * Summary of rollBack
	 */
	public function rollBack(){
		return $this->db->rollBack($this->connection_id);
	}
	/**
	 * Summary of reuse
	 * @return ActiveRecord
	 */
	public function reuse() {
		$this->model->{$this->model->primary_key} = null;
		return $this;
	}

	public function hasSelectedDefined(){
		return count($this->select) > 0;
	}

	public function whenResolve($callback){
		$this->callbacks[] = $callback;
		return $this;
	}
	private function resolveCallbacks($result){
		foreach($this->callbacks as $callback){
			$callback($result);
		}
	}
}
