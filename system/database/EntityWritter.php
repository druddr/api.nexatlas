<?php


class EntityWritter {

	private $uri;
	private $uriStartIndex=0;

	private $data;

	/**
	 * @var Model
	 */
	private $entity;


	public function __construct($uri){
		$this->uri = $uri;
		$this->write();
	}

	private function write(){
		$this->data = json_decode(file_get_contents('php://input'));
		$this->setParams();
		$this->setEntity();
		$this->fillEntity();
		$this->saveEntity();
		//print_r($this->entity);
	}
	private function setParams(){
		$uri='';
		while($uri!=='entitymapper' && $this->uriStartIndex<=count($this->uri->parts)){
			$uri = strtolower($this->uri->parts[$this->uriStartIndex++]);
		}
	}
	private function setEntity(){
		if(strpos($this->uri->parts[$this->uriStartIndex],"view")!==false){
			$this->uriStartIndex++;
			$entityName = "view".$this->uri->parts[$this->uriStartIndex];
		}
		else{
			$entityName = "model".$this->uri->parts[$this->uriStartIndex];
		}
		$this->entity = new $entityName;
	}
	private function fillEntity(){
		$this->entity->fill($this->data);
		$this->entity;		

	}
	private function saveEntity(){

		$this->entity->save();

	}
}