<?php

/**
 * MysqlDriver short summary.
 *
 * MysqlDriver description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class MysqlDriver implements ISQLDriver
{
	private $sql;
	public function getSql(){
		return $this->sql;
	}
	public function cleanSql(){
		return $this->sql = '';
	}
	public function checkForeignKeys($check){
		$this->sql .= 'SET FOREIGN_KEY_CHECKS = '.($check? 1: 0).";\r\n";
		return $this;
	}
	public function selectViews($database){
		$this->sql .= "SELECT INFORMATION_SCHEMA.VIEWS.*
			  FROM INFORMATION_SCHEMA.VIEWS
			 WHERE TABLE_SCHEMA = '{$database}';\r\n";
		return $this;
	}
	public function selectTables($database){
		$this->sql .= "SELECT INFORMATION_SCHEMA.COLUMNS.*
			  FROM INFORMATION_SCHEMA.COLUMNS
			 WHERE TABLE_SCHEMA = '{$database}'
		  ORDER BY TABLE_NAME ASC
			     , ORDINAL_POSITION ASC;\r\n";
		return $this;
	}
    public function selectForeignKeys($database){
		$this->sql .= "SELECT INFORMATION_SCHEMA.KEY_COLUMN_USAGE.*
				  FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
				 WHERE REFERENCED_TABLE_NAME IS NOT NULL
				   AND TABLE_SCHEMA = '{$database}'
			  ORDER BY TABLE_NAME ASC
					 , ORDINAL_POSITION ASC
					 , COLUMN_NAME ASC;\r\n";
		return $this;
	}
	public function selectPrimaryKeys($database){
		$this->sql .= "SELECT * FROM INFORMATION_SCHEMA.COLUMNS
				   WHERE TABLE_SCHEMA = '{$database}'
					 AND COLUMN_KEY = 'PRI'
			    ORDER BY TABLE_NAME ASC
					   , ORDINAL_POSITION ASC
					   , COLUMN_NAME ASC;\r\n";
		return $this;
	}
	public function selectUniqueKeys($database){
		$this->sql .= "SELECT * FROM INFORMATION_SCHEMA.COLUMNS
				   WHERE TABLE_SCHEMA = '{$database}'
					 AND COLUMN_KEY = 'UNI'
			    ORDER BY TABLE_NAME ASC
					   , ORDINAL_POSITION ASC
					   , COLUMN_NAME ASC;\r\n";
		return $this;
	}
	public function selectIndexes($database){
		$this->sql .= "SELECT DISTINCT
					   TABLE_NAME
					 , INDEX_NAME
					 , COLUMN_NAME
				  FROM INFORMATION_SCHEMA.STATISTICS
				 WHERE TABLE_SCHEMA = '{$database}';\r\n";
		return $this;
	}
	public function createIndexes($tables){
		$sql = "";
		$idxname = 'idx0000000';
		foreach($tables as $table){
			foreach($table->indexes as $index){
				$name = ($index->index_name!='PRIMARY') ? ++$idxname : 'PRIMARY';
				$sql .= "CREATE INDEX `{$name}` ON `{$table->table_name}` (`{$index->column_name}`);\r\n";
			}
		}
		$this->sql .= $sql;
		return $this;
	}
	public function dropViews($views){
		$sql = '';
		foreach($views as $view){
			$sql .= "DROP TABLE IF EXISTS `{$view->name}`;\r\n\r\n";
			$sql .= "DROP VIEW IF EXISTS `{$view->name}`;\r\n\r\n";
		}
		$this->sql .= $sql;
		return $this;
	}
	public function createViews($views){
		$sql = '';
		foreach($views as $view){
			$sql .= "CREATE VIEW `{$view->name}` AS \r\n";
			$sql .= "\t\t{$view->definition};\r\n\r\n";
		}
		$this->sql .= $sql;
		return $this;
	}
	public function createTables($tables){
		$sql = '';
		$pkname = 'pk0000001';//primary key constraint
		foreach($tables as $table){
			$keys = [];
			$sql .= "\r\nCREATE TABLE IF NOT EXISTS `{$table->table_name}` (";
			foreach($table->columns as $kc=>$column){
				$sql.=  "\r\n\t`{$column->column_name}` ".strtoupper($column->column_type)." ".($column->is_nullable=='no' ? 'NOT NULL': 'NULL')." ".strtoupper($column->extra).(!is_null($column->column_default)?' DEFAULT '.$column->column_default.' ' : '');
				if($column->column_key=='pri' || $column->column_key=='uni'){
					$keys[] = $column->column_name;
				}
				if(isset($table->columns[$kc+1])){
					$sql.= ",";
				}
			}
			$primaries = [];
			foreach($table->primaries as $primary){
				$primaries[] = $primary->column;
			}
			if(count($primaries)>0){
				$sql .= ",\r\n\tCONSTRAINT `{$pkname}` PRIMARY KEY ( ".join(", ",$primaries)." )\r\n";
				$pkname++;
			}
			$sql.= "\r\n);\r\n";
		}
		//$sql.= "ENGINE = InnoDB;\r\n\r\n";
		$this->sql .= $sql;
		return $this;
	}
	public function createConstraints($tables){
		$sql = "";
		$fkname = 'fk0000001';//foreing key constraint
		$ukname = 'uk0000001';//primary key constraint
		foreach($tables as $table){
			foreach($table->constraints as $constraint){
				$sql .= "ALTER TABLE `{$table->table_name}` ADD CONSTRAINT `{$fkname}` FOREIGN KEY (`{$constraint->column}`) REFERENCES `{$constraint->reference}` (`{$constraint->reference_column}`);\r\n";
				$fkname++;
			}
			$uniques = [];
			foreach($table->uniques as $unique){
				$uniques[] = $unique->column;
			}
			if(count($uniques)>0){
				$sql .= "ALTER TABLE `{$table->table_name}` ADD CONSTRAINT `{$ukname}` UNIQUE (".join(",",$uniques).");\r\n";
				$ukname++;
			}
		}
		$this->sql .= $sql;
		return $this;
	}

	public function dropTables($tables){
		$sql = "";
		foreach($tables as $table){
			$sql .= "DROP TABLE IF EXISTS `{$table->table_name}`;\r\n";
		}
		$this->sql .= $sql;
		return $this;
	}
	public function dropConstraints($tables){
		$sql = "";
		$fkname = 'fk0000001';
		foreach($tables as $table){
			foreach($table->constraints as $constraint){
				$sql .= "ALTER TABLE `{$table->table_name}` DROP FOREIGN KEY `{$constraint->name}`;\r\n";
				$fkname++;
			}
		}
		return $this;
	}
	public function dropDatabase($database){
		$this->sql .= "DROP DATABASE IF EXISTS {$database};\r\n";
		return $this;
	}

	public function createDatabase($database){
		$this->sql .= "CREATE DATABASE IF NOT EXISTS {$database};\r\n";
		return $this;
	}
}
