<?php
class EntityReader {
	/**
	 * @var Uri
	 */
	private $uri;
	private $uriStartIndex=0;

	private $filterTypes = [
		'is'=>['operator'=>'IS','string'=>false],
		'isnot'=>['operator'=>'IS NOT','string'=>false],
		'like'=>['operator'=>'LIKE','prefix'=>'%','sufix'=>'%'],
		'equals'=>['operator'=>'='],
		'different'=>['operator'=>'!='],
		'in'=>['operator'=>'IN','prefix'=>'(','sufix'=>')','string'=>false,'params-glue'=>','],
		'notin'=>['operator'=>'NOT IN','prefix'=>'(','sufix'=>')','string'=>false,'params-glue'=>','],
		'greaterthan'=>['operator'=>'>'],
		'lessthan'=>['operator'=>'<'],
		'greaterorequalthan'=>['operator'=>'>='],
		'lessorequalthan'=>['operator'=>'<='],
		'between'=>['operator'=>'BETWEEN', 'string'=>false,'params-glue'=>'AND'],
		'notbetween'=>['operator'=>'NOT BETWEEN', 'string'=>false,'params-glue'=>'AND']
	];
	private $filterFunction = [
		'date' => 'DATE',
		'common' => '',
	];
	private $groupFunction =  [
		'date' => 'DATE',
		'common' => '',
	];

	/**
	 * @var Model
	 */
	private $entity;
	private $identity;
	private $limit;
	private $offset;
	private $operator;
	private $expand;
	private $filter;
	private $group;
	private $sum;
	private $order;
	private $count;
	private $firstOnly=false;
	private $select;
	private $call;

	private $reflector;

	public function __construct($uri=null){
		$this->uri = $uri;
		$this->read();
	}
	public function CheckOptionsRequest(){
		if(strtoupper($_SERVER['REQUEST_METHOD'])!='OPTIONS') return;
		Response::ok(true);
	}
	private function checkAuth(){
		return;
		ini_set('session.cookie_lifetime', 60 * Config::SessionTimeout);
		ini_set('session.gc_maxlifetime', 60 * Config::SessionTimeout);
		session_start();
		session_regenerate_id();
		if(CurrentUser::get()->user==null || (count(CurrentUser::get()->routes)==0 && !config::$DevMode)) {
			Response::unauthorized(true,'AUTH001');
		}
		$headers = getallheaders();
		if(config::$requiredCCID){
			if(!isset($headers['CCID']) && !isset($headers['Ccid'])){
				Response::unauthorized(true,'AUTH002');
			}
			$current_company_header = isset($headers['CCID']) ? $headers['CCID'] : $headers['Ccid'];
			$current_subsidiary_header = isset($headers['CSID']) ?  $headers['CSID'] : $headers['Csid'];

			if(!in_array($current_company_header,CurrentUser::get()->companies)){
				Response::forbidden(true,'AUTH003');
			}

			CurrentUser::get()->setCurrentCompany($current_company_header);
			CurrentUser::get()->setCurrentSubsidiary($current_subsidiary_header);
		}

		AuthorizedSession::start();
		return true;
	}

	private function read(){
		$this->CheckOptionsRequest();
		$this->checkAuth();
		$this->setParams();
		$this->setEntity();
		if(!strpos($this->uri->uriString,'$metadata')){
			$this->setIdentity();
			$this->setOperator();
			$this->setLimit();
			$this->setOffset();
			$this->setExpand();
			$this->setFilter();
			$this->setGroup();
			$this->setSum();
			$this->setOrder();
			$this->setCount();
			$this->setFirstOnly();
			$this->setSelect();
			$this->setCall();
			$result = $this->release();
		}
		else {
			$result = $this->buildMetadata();
		}

		header("Content-Type:application/json");

		//if(config::$AngularJsJSON) echo ")]}',\n";
		if(config::$UTF8DecodeResult) {
			exit(json_encode(utf8_decode_all($result)));
		}
		if(config::$UTF8EncodeResult){
			exit(json_encode(utf8_encode_all($result)));
		}
		exit(json_encode(($result)));

	}

	private function setParams(){
		$uri='';
		while($uri!=='entitymapper' && $this->uriStartIndex<=count($this->uri->parts)){
			$uri = strtolower($this->uri->parts[$this->uriStartIndex++]);
		}
	}

	private function setSelect(){
		if(!isset($this->uri->uriParams['select'])){
			return;
		}
		$this->select  = is_array($this->uri->uriParams['select']) ? $this->uri->uriParams['select'] : [$this->uri->uriParams['select']];
	}

	private function setEntity(){
		if(strpos(strtolower($this->uri->parts[$this->uriStartIndex]),"view")!==false){
			$this->uriStartIndex++;
			$entityName = "View".$this->uri->parts[$this->uriStartIndex];
		}
		else{
			$entityName = "Model".$this->uri->parts[$this->uriStartIndex];
		}
		$this->entity = new $entityName;
	}

	private function setIdentity(){
		if(!isset($this->uri->parts[$this->uriStartIndex+1]) || !is_numeric($this->uri->parts[$this->uriStartIndex+1])){
			return;
		}
		$this->identity = intval($this->uri->parts[$this->uriStartIndex+1]);
		$this->entity->{$this->entity->primary_key} = $this->identity;
	}

	private function setLimit(){
		if(isset($_GET['limit']) && is_numeric($_GET['limit'])){
			$this->limit = $_GET['limit'];
		}
	}

	private function setOffset(){
		if(isset($_GET['offset']) && is_numeric($_GET['offset'])){
			$this->offset = $_GET['offset'];
		}
	}

	private function setOperator(){
		if(isset($_GET['operator']) && !is_numeric($_GET['operator'])){
			$this->operator = $_GET['operator'];
		}
	}

	private function setExpand(){
		if(!isset($this->uri->uriParams['expand'])){
			return;
		}
		$params = is_array($this->uri->uriParams['expand']) ? $this->uri->uriParams['expand'] : [$this->uri->uriParams['expand']];
		$expand=[];
		foreach($params as $param){
			$params = explode(":",$param);
			$expand[] =[
				'property'=>$params[0],
				'limit'=>isset($params[1]) && $params[1]=='limit' && isset($params[2]) && is_numeric($params[2]) ? $params[2] : null
			];

		}
		$this->expand = $expand;
	}

	private function setFilter(){
		$uriParams = (array)json_decode(strtolower(json_encode($this->uri->uriParams)));
		foreach($this->filterTypes as $type=>$config){
			if(!isset($uriParams[$type])){
				continue;
			}

			$filters = is_array($uriParams[$type]) ? $uriParams[$type] : [$uriParams[$type]];
			foreach($filters as $filter){
				$params = explode(":",$filter);
				if(!isset($this->filter[$type][$params[0]])){
					$this->filter[$type][$params[0]] = [
						'function'=> isset($params[2]) ? $params[1] : 'common',
						'value' => isset($params[2]) ? $params[2] : $params[1],
						'extra' => isset($params[2]) ? array_slice($params,3) : array_slice($params,2)
					];
				} else {
					$value = $this->filter[$type][$params[0]]['value'];
					$this->filter[$type][$params[0]]['value'] = is_array($value) ? $value : [$value];
					$this->filter[$type][$params[0]]['value'][] = isset($params[2]) ? $params[2] : $params[1];
				}
			}
		}
	}

	private function setGroup(){
		if(!isset($this->uri->uriParams['group'])) return;

		$this->group = is_array($this->uri->uriParams['group']) ? $this->uri->uriParams['group'] : [$this->uri->uriParams['group']];
		foreach($this->group as &$group){
			$params = explode(":",$group);
			$group = [
				'function' => isset($params[1]) ? $params[0] : 'common',
				'field' => isset($params[1]) ? $params[1] : $params[0],
			];
		}
	}

	private function setSum(){
		if(!isset($this->uri->uriParams['sum'])) return;

		$this->sum = is_array($this->uri->uriParams['sum']) ? $this->uri->uriParams['sum'] : [$this->uri->uriParams['sum']];
		foreach($this->sum as &$sum){
			$params = explode(":",$sum);
			$sum = [
				'field'=>$params[0],
				'alias'=>isset($params[1]) ? $params[1] : $params[0]
			];
		}
	}

	private function setOrder(){
		if(!isset($this->uri->uriParams['order'])) return;

		$this->order = is_array($this->uri->uriParams['order']) ? $this->uri->uriParams['order'] : [$this->uri->uriParams['order']];
		foreach($this->order as &$order){
			$params = explode(":",$order);
			$order = [
				'field'=>$params[0],
				'order'=>isset($params[1]) ? $params[1] : 'ASC'
			];
		}
	}

	private function setCount(){
		if(!isset($this->uri->uriParams['count'])) return;

		$this->count = is_array($this->uri->uriParams['count']) ? $this->uri->uriParams['count'] : [$this->uri->uriParams['count']];
		foreach($this->count as &$count){
			$params = explode(":",$count);
			$count = [
				'field'=>$params[0],
				'alias'=>isset($params[1]) ? $params[1] :'count_'.$params[0]
			];
		}
	}

	private function setFirstOnly(){
		$this->firstOnly = false;
		if(isset($_GET['first']) && !empty($_GET['first']) && $_GET['first']==1){
			$this->firstOnly = true;
		}
	}
	private function setCall(){
		if(!isset($this->uri->uriParams['call'])) return;

		$this->call = is_array($this->uri->uriParams['call']) ? $this->uri->uriParams['call'] : [$this->uri->uriParams['call']];
		foreach($this->call as &$call){
			$split = explode("(",str_replace(")","",$call));
			$params = explode(",",$split[1]);
			$call = [
				'method' => $split[0],
				'params' => $params
			];
		}
	}

	private function processSingleEntity(){
		$records = $this->entity->records();
		
		if(isset($this->select)){
			$records->select($this->select);
		}
		$records->get($this->identity)->getFirstModel();

		if($this->expand){
			foreach($this->expand as $expand){
				$property = $expand['property'];
				$limit = $expand['limit'];
				$value = $this->entity->hasMany($property,$limit);
				if($value===false){
					$value = $this->entity->hasOne($property);
				}
				if($value===false){
					$value = $this->entity->belongsTo($property);
				}
				$this->entity->{$property} = $value;
			}
		}
		if($this->call){
			foreach($this->call as $call){
				$sMethod= $call['method'];
				if(!$this->docCommentHasVar($sMethod,"@api")) continue;
				$resultPropertyName = $call['method']."_result";
				$this->entity->{$resultPropertyName} = call_user_func_array([$this->entity,$sMethod],$call['params']);
			}
		}

		if(isset($_GET['debug']) && $_GET['debug']==1){
			echo '<pre>';
			print_r($records->last_sql);
			print_r($this->entity->extract());
			exit;
		}
		if(!isset($this->select)){
			return ($this->entity->extract());
		}
		else {
			$entity = $this->entity->extract();
			$filteredResult = new stdClass();
			foreach($this->select as $field){
				$filteredResult->{$field} = $entity->{$field};
			}
			return $filteredResult;
		}
	}

	private function processMultipleEntities(){
		$records = $this->entity->records();
		if($this->filter){
			//['is','isnot','like','equals','different','in','notin','greaterthan','lessthan','lessorequalthan','between'];
			foreach($this->filterTypes as $type=>$config){

				if(isset($this->filter[$type])){
					foreach($this->filter[$type] as $property=>$params){
						$value = $params['value'];
						$string = $this->isFilterString($value) && (!isset($config['string']) || (isset($config['string']) && $config['string'] )) ;

						$prefix = isset($config['prefix']) ? $config['prefix'] : '';
						$sufix = isset($config['sufix']) ? $config['sufix'] : '';


						if(!$string){
							if(is_array($value)){
								$value = $prefix.(join($value,$config['params-glue'])).$sufix;
							} else {

								$value = $value=="null" ? null : $value;
								$value = json_encode($value);
								if(count($params['extra'])){
									$value = "{$this->filterFunction[$params['function']]}({$value})";
									foreach($params['extra'] as &$param){
										$param = "{$this->filterFunction[$params['function']]}(".json_encode($param).")";
									}
									$value .= " ".$config['params-glue']." ". join(" {$config['params-glue']} ",$params['extra']);
								}
								$value = $prefix.($value).$sufix;
							}
						}
						else{
							if(isset($this->entity->field_config[$value])){
								$string = false;
							}
							$value = $prefix.($value).$sufix;
						}
						$records->where($property, $value, $config['operator'], $string, isset($this->operator) ? $this->operator : "AND");
					}
				}

			}
		}
		//Current Company
		if(config::$requiredCCID && isset($this->entity->field_config['id_empresa'])){
			//$records->where('id_empresa',AuthorizedSession::get()->getCurrentCompany());
		}
		//Current Subsidiary
		if(config::$requiredCCID && isset($this->entity->field_config['id_empresa_filial'])){
			//$records->where('id_empresa_filial',AuthorizedSession::get()->getCurrentSubsidiary());
		}

		if($this->group){
			foreach($this->group as $group){
				$field = $this->groupFunction[$group['function']]."({$group['field']})";
				$records->group($field);
				$records->addSelect($group['field']);
			}
			$records->clearSelect();
		}
		if($this->sum){
			foreach($this->sum as $sum){
				$records->addSelect(["SUM({$sum['field']})" => $sum['alias']]);
			}
		}
		if($this->order){
			foreach($this->order as $order){
				$records->order($order['field'],$order['order']);
			}
		}
		if($this->count){
			foreach($this->count as $count){
				$records->count($count['field'], $count['alias']);
			}
		}
		if($this->firstOnly){
			$records->limit(1);
		}
		if(!$this->firstOnly && $this->limit){
			$records->limit($this->limit);
		}
		if(isset($this->offset)){
			$records->offset($this->offset);
		}
		if($this->select){
			foreach($this->select as $select){
				$records->addSelect($select);
			}
		}

		try {
			$result = $records->get()->toEntityCollection();
		}
		catch (Exception $e) {
			echo '<pre>';
			print_r($records->last_sql);
			echo "\r\n\r\n";
			print_r($e);
			exit;
		}

		$custom_select = [];
		$result->each(function(Model $entity) use(&$custom_select) {
			if($this->expand){
				foreach($this->expand as $expand){
					$property = $expand['property'];
					$limit = $expand['limit'];
					$value = $entity->hasMany($property,$limit);
					if($value===false){
						$value = $entity->hasOne($property);
					}
					if($value===false){
						$value = $entity->belongsTo($property);
					}
					$entity->{$property} = $value;
					$custom_select[] = strtolower($property);
				}
			}
			if($this->call){
				foreach($this->call as $call){
					$sMethod= $call['method'];
					if(!$this->docCommentHasVar($sMethod,"@api")) continue;
					$resultPropertyName = $sMethod."_result";
					$entity->{$resultPropertyName} = call_user_func_array([$entity,$sMethod],$call['params']);
					$custom_select[] = strtolower($resultPropertyName);
				}
			}
		});

		if(isset($_GET['debug']) && $_GET['debug']==1){
			echo '<pre>';
			print_r($records->last_sql);
			echo "\r\n";
			print_r($result->map(function($entity) {
				if(!isset($this->select)){
					return $entity->extract();
				}
				else {
					$entityExtracted = $entity->extract();
					$filteredResult = new stdClass();
					foreach($this->select as $field){
						$filteredResult->{$field} = $entityExtracted->{$field};
					}
					return $filteredResult;
				}
			})->toArray());
			exit;
		}
		$return = $result->map(function($entity) use($records, $custom_select) {
		    if(!isset($this->select) && !isset($this->group) && !$records->isGrouped()){
				return $entity->extract();
			}
			else {
				$select = array_merge($records->getSelectedFields(), $custom_select);
				$entityExtracted = $entity->extract();
				$filteredResult = new stdClass();
				foreach($select as $field){
					$filteredResult->{$field} = $entityExtracted->{$field};
				}
				return $filteredResult;
			}
		})->toArray();

		return $this->firstOnly ? $return[0] : $return;
	}

	private function getReflector(){
		if($this->reflector==null){
			$this->reflector = new ReflectionClass($this->entity);
		}
		return $this->reflector;
	}

	private function docCommentHasVar($sMethod, $sVar){
		return strpos($this->getReflector()->getMethod($sMethod)->getDocComment(), $sVar)>-1;
	}

	private static function isFilterString($value){
		return !(is_null($value) || is_bool($value) ||is_array($value) || $value=='null');
	}

	private function buildMetadata(){
		$oResult = new stdClass();
		$oResult->_metadata = new stdClass();
		$oResult->_metadata->fields = $this->entity->field_config;
		$oResult->_metadata->expand = new stdClass();
		$oResult->_metadata->expand->single = new stdClass();
		$oResult->_metadata->expand->list = new stdClass();
		if(isset($this->entity->hasOne) && is_array($this->entity->hasOne)){
			foreach($this->entity->hasOne as $name=>$config){
				$oResult->_metadata->expand->single->{$name} = isset($config['model']) ? $config['model'] : $config['table'];
			}
		}
		if(isset($this->entity->hasMany) && is_array($this->entity->hasMany)){
			foreach($this->entity->hasMany as $name=>$config){
				$oResult->_metadata->expand->list->{$name} = isset($config['model']) ? $config['model'] : $config['table'];
			}
		}
		return $oResult;
	}

	private function release(){
		if($this->identity){
			return $this->processSingleEntity();
		}
		return $this->processMultipleEntities();
	}

}