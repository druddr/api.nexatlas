<?php

/**
 * ISQLDriver short summary.
 *
 * ISQLDriver description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
interface ISQLDriver
{

	/**
	 * Summary of selectConstraints
	 * @param mixed $database 
	 */
	public function selectForeignKeys($database);
	/**
	 * Summary of selectTables
	 * @param mixed $database 
	 */
	public function selectTables($database);
	/**
	 * Summary of selectIndexes
	 * @param mixed $database 
	 */
	public function selectIndexes($database);
	/**
	 * Summary of selectPrimarykeys
	 * @param mixed $database 
	 */
	public function selectPrimarykeys($database);
	/**
	 * Summary of selectUniqueKeys
	 * @param mixed $database 
	 */
	public function selectUniqueKeys($database);
	/**
	 * Summary of createIndex
	 * @param mixed $tables 
	 */
	public function createIndexes($tables);
	/**
	 * Summary of createTable
	 * @param mixed $tables 
	 */
	public function createTables($tables);
	/**
	 * Summary of createConstraint
	 * @param mixed $tables 
	 */
	public function createConstraints($tables);
	/**
	 * Summary of dropTables
	 * @param mixed $tables 
	 */
	public function dropTables($tables);
	/**
	 * Summary of dropConstraints
	 * @param mixed $tables 
	 */
	public function dropConstraints($tables);
	/**
	 * Summary of dropDatabase
	 * @param mixed $database 
	 */
	public function dropDatabase($database);
	/**
	 * Summary of createDatabase
	 * @param mixed $database 
	 */
	public function createDatabase($database);
	/**
	 * Summary of getSql
	 */
	public function getSql();
	/**
	 * Summary of cleanSql
	 */
	public function cleanSql();
}
