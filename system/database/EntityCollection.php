<?php


class EntityCollection {
	private $items = [];


	public function __construct($items){
		$this->items = $items;
	}
	public function add($item){
		$this->items[] = $item;
	}
	public function remove($condition){
		foreach($this->items as $k=>$item) {
			if($condition($item,$k)) {
				unset($this->items[$k]);
			}
		}
	}
	public function filter($condition){
		$items = new EntityCollection([]);
		foreach($this->items as $k=>$item) {
			if($condition($item,$k)) {
				$items->add($item);
			}
		}
		return $items;
	}
	public function map($transformation){
		$items = new EntityCollection([]);
		foreach($this->items as $k=>$item) {
			$items->add($transformation($item,$k));
		}
		return $items;
	}
	public function order($fields){
		usort($this->items, function($current,$next) use ($fields){
			$current = (object)$current;
			$next = (object)$next;
			foreach($fields as $field){
				$comparsion = strcmp($current->{$field},$next->{$field});
				if($comparsion!==0) return $comparsion;
			}
			return 0;
		});
		return $this;
	}
	public function group($criteria){

	}

	public function size(){
		return count($this->items);
	}
	public function first(){
		return reset($this->items);
	}
	public function next(){
		return next($this->items);
	}
	public function prev(){
		return prev($this->items);
	}
	public function last(){
		return end($this->items);
	}

	public function sum($field){
		$sum = 0;
		foreach($this->items as $k=>$item) {
			$sum += $field($item,$k);
		}
		return $sum;
	}

	public function each($callback){
		foreach($this->items as $k=>$item){
			$callback($item,$k);
		}
		return $this;
	}

	public function toArray(){
		return $this->items;
	}
}

