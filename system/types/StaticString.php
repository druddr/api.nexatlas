<?php

/**
 * StaticString short summary.
 *
 * StaticString description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */

class CustomStaticString {
    /* static methods wrapping multibyte */

    /**
     * Wrapper for substr
     */
    public static function substr ($string, $start, $length = null) {
        if(CustomString::$multibyte) {
            return new CustomString(mb_substr($string, $start, $length, CustomString::$multibyte_encoding));
        }
        return new CustomString(substr($string, $start, $length));
    }

    public function charAt ($str, $point) {
        return new CustomString(self::substr($str, $point, 1));
    }

    public function charCodeAt ($str, $point) {
        return new CustomString(ord(self::substr($str, $point, 1)));
    }

    public static function concat () {
        $args = func_get_args();
        $r = "";
        foreach($args as $arg) {
            $r .= (string)$arg;
        }
        return new CustomString($arg);
    }

    public static function fromCharCode ($code) {
        return new CustomString(chr($code));
    }

    public static function indexOf ($haystack, $needle, $offset = 0) {
        if(CustomString::$multibyte) {
            return mb_strpos($haystack, $needle, $offset, CustomString::$multibyte_encoding);
        }
        return new CustomString(strpos($haystack, $needle, $offset));
    }

    public static function lastIndexOf ($haystack, $needle, $offset = 0) {
        if(CustomString::$multibyte) {
            return mb_strrpos($haystack, $needle, $offset, CustomString::$multibyte_encoding);
        }
        return new CustomString(strrpos($haystack, $needle, $offset));
    }

    public static function match ($haystack, $regex) {
        preg_match_all($regex, $haystack, $matches, PREG_PATTERN_ORDER);
        return new Arr($matches[0]);
    }

    public static function replace ($haystack, $needle, $replace, $regex = false) {
        if($regex) {
            $r = preg_replace($needle, $replace, $haystack);
        }
        else {
            if(CustomString::$multibyte) {
                $r = mb_str_replace($needle, $replace, $haystack);
            }
            else {
                $r = str_replace($needle, $replace, $haystack);
            }
        }
        return new CustomString($r);
    }

    public static function strlen ($string) {
        if(CustomString::$multibyte) {
            return new CustomString(mb_strlen($string, CustomString::$multibyte_encoding));
        }
        return new CustomString(strlen($string));
    }

    public static function toLowerCase ($string) {
        if(CustomString::$multibyte) {
            return new CustomString(mb_strtolower($string, CustomString::$multibyte_encoding));
        }
        return new CustomString(strtolower($string));
    }

    public static function toUpperCase ($string) {
        if(CustomString::$multibyte) {
            return new CustomString(mb_strtoupper($string, CustomString::$multibyte_encoding));
        }
        return new CustomString(strtoupper($string));
    }

    public static function split ($string, $at = '') {
        if(empty($at)) {
            if(CustomString::$multibyte) {
                return new Arr(mb_str_split($string));
            }
            return new Arr(str_split($string));
        }
        return new Arr(explode($at, $string));
    }

    /* end static wrapper methods */
}