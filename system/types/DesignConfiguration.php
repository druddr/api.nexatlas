<?php

/**
 * DesignConfiguration short summary.
 *
 * DesignConfiguration description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class DesignConfiguration extends TypedClass{
	/**
	 * @property-read
	 * @property-write
	 * @var array(String)
	 */
	private $default_css = array();
	/**
	 * @property-read
	 * @property-write
	 * @var array(String)
	 */
	private $default_css_assets = array();
	/**
	 * @property-read
	 * @property-write
	 * @var array(String)
	 */
	private $default_js = array();
	/**
	 * @property-read
	 * @property-write
	 * @var array(String)
	 */
	private $default_js_assets = array();
}