<?php


if(!function_exists('mb_str_replace')) {
    function mb_str_replace($search, $replace, $subject){
        return preg_replace('@'.preg_quote($search).'@u',$replace,$subject);
    }
}
if (!function_exists('mb_str_split')) {
    function mb_str_split($str, $length = 1)
    {
		preg_match_all('/.{1,' . $length . '}/us', $str, $matches);
		return $matches[0];
    }
}

class CustomString implements ArrayAccess {
    public static $multibyte_encoding = null;
    public static $multibyte = false;

    private $value;
    private static $checked = false;

    /* magic methods */
    public function __construct ($string) {
        if(!self::$checked) {
            self::$multibyte = extension_loaded('mbstring');
        }
        if(is_null(self::$multibyte_encoding)) {
            if(self::$multibyte) {
				self::$multibyte_encoding = mb_internal_encoding();
            }
        }
        $this->value = (string)$string;
    }

    public function __toString () {
        return (string)$this->value;
    }

    /* end magic methods */

    /* ArrayAccess Methods */

    /** offsetExists ( mixed $index )
	 *
	 * Similar to array_key_exists
	 */
    public function offsetExists ($index) {
        return !empty($this->value[$index]);
    }

    /* offsetGet ( mixed $index )
	 *
	 * Retrieves an array value
	 */
    public function offsetGet ($index) {
		$index = is_null($index) ? CustomStaticString::strlen($this->value) : $index;
        return CustomStaticString::substr($this->value, $index, 1);
    }

    /* offsetSet ( mixed $index, mixed $val )
	 *
	 * Sets an array value
	 */
    public function offsetSet ($index, $val) {
		$index = is_null($index) ? CustomStaticString::strlen($this->value) : $index;
        $this->value = CustomStaticString::substr($this->value, 0, $index) . $val . CustomStaticString::substr($this->value, $index+1, CustomStaticString::strlen($this->value));
    }

    /* offsetUnset ( mixed $index )
	 *
	 * Removes an array value
	 */
    public function offsetUnset ($index) {
		$index = is_null($index) ? CustomStaticString::strlen($this->value) : $index;
        $this->value = CustomStaticString::substr($this->value, 0, $index) . CustomStaticString::substr($this->value, $index+1);
    }


    public static function isNullOrEmpty($string){
        return ($string==="" || $string===null) && $string!==0;
    }

    public static function create ($obj) {
        if($obj instanceof String) return new CustomString($obj);
        return new CustomString($obj);
    }

    /* public methods */
    public function substr ($start, $length) {
        return CustomStaticString::substr($this->value, $start, $length);
    }


    public function charAt ($point) {
        return CustomStaticString::substr($this->value, $point, 1);
    }

    public function charCodeAt ($point) {
        return ord(CustomStaticString::substr($this->value, $point, 1));
    }

    public function indexOf ($needle, $offset) {
        return CustomStaticString::indexOf($this->value, $needle, $offset);
    }

    public function lastIndexOf ($needle) {
        return CustomStaticString::lastIndexOf($this->value, $needle);
    }

    public function match ($regex) {
        return CustomStaticString::match($this->value, $regex);
    }

    public function replace ($search, $replace, $regex = false) {
        return CustomStaticString::replace($this->value, $search, $replace, $regex);
    }

    public function first () {
        return CustomStaticString::substr($this->value, 0, 1);
    }

    public function last () {
        return CustomStaticString::substr($this->value, -1, 1);
    }

    public function search ($search, $offset = null) {
        return $this->indexOf($search, $offset);
    }

	public function contains ($search) {
        return $this->indexOf($search, 0) > -1;
    }

    public function slice ($start, $end = null) {
        return CustomStaticString::slice($this->value, $start, $end);
    }

    public function toLowerCase () {
        return CustomStaticString::toLowerCase($this->value);
    }

    public function toUpperCase () {
        return CustomStaticString::toUpperCase($this->value);
    }

    public function toUpper () {
        return $this->toUpperCase();
    }

    public function toLower () {
        return $this->toLowerCase();
    }

    public function split ($at = '') {
        return CustomStaticString::split($this->value, $at);
    }

    public function trim ($charlist = null) {
        return new CustomString(trim($this->value, $charlist));
    }

    public function ltrim ($charlist = null) {
        return new CustomString(ltrim($this->value, $charlist));
    }

    public function rtrim ($charlist = null) {
        return new CustomString(rtrim($this->value, $charlist));
    }

    public function concat() {
		$args = func_get_args();
        $value = $this->value;
		$value = $value.implode("",$args);
		return new CustomString($value);
    }

    public function toString () {
        return $this->__toString();
    }
}


