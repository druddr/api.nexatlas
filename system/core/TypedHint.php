<?php

/**
 * TypedHint short summary.
 *
 * TypedHint description.
 *
 * @version 1.0
 * @author bruno.teixeira
 */
define('TYPEDHINT_PCRE','/^Argument (\d)+ passed to (?:(\w+)::)?(\w+)\(\) must be an instance of (\w+), (\w+) given/');
abstract class TypedHint {
    private static $types = array(
        'Int'		=> 'is_int',
        'int'		=> 'is_int',
        'string'	=> 'is_string',
        'bool'		=> 'is_bool',
        'Boolean'   => 'is_bool',
        'Integer'   => 'is_int',
        'Float'     => 'is_float',
        'String'    => 'is_string',
        'Resource'  => 'is_resource',
        'Object'    => 'is_object',
        
    );
    private function __construct() {}
    public static function init() {
        set_error_handler('TypedHint::handle');
        return true;
    }
    private static function getArgument($backTrace, $function, $index, &$argument) {
        foreach ($backTrace as $trace){
            // Match the function; Note we could do more defensive error checking.
            if (isset($trace['function']) && $trace['function'] == $function){
                $argument = $trace['args'][$index - 1];
                return true;
            }
        }
        return true;
    }

    public static function handle($errorLevel, $message) {		
        if ($errorLevel == E_RECOVERABLE_ERROR){
			//If it find a match means argument was passaed with wrong value type
            if (preg_match(TYPEDHINT_PCRE, $message, $matches)) {
				//create vars comming from match
                list($match, $index, $class, $function, $type, $realType) = $matches;
                if (isset(self::$types[$type])){
                    $backTrace = debug_backtrace();
					
                    if (self::getArgument($backTrace, $function, $index, $value)){						
						
                        if (call_user_func(self::$types[$type], $value)){
							if(strtolower($type)=="string"){
								$argNumber = count($backTrace[0]['args'][4]);
								$object = $backTrace[1]['object'];
								$backTrace[1]['args'][$argNumber] = new $type($value);
							}
							$value  = NULL;
							return true;
                        }						
                    }
                }
            }
        }
		return false;
    }
}


