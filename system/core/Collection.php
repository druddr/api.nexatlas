<?php

/**
 * Collection short summary.
 *
 * Collection description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Collection implements ArrayAccess, Iterator, Countable
{
    private $_collectionData=array();
	private $_collectionType;
	private $_isObject=false;
	public function __construct($type){
		switch(strtolower($type)){
			case 'array':
			case 'string':
			case 'integer':
			case 'boolean':
			case 'float':
				$this->_isObject = false;
				break;
			case 'object':
				$this->_isObject = true;
				break;
			default: 
				if(!class_exists($type)){
					throw new Exception("Class {$type} does not exists");
				}
				$this->_isObject = true;
				break;
		}
		$this->_collectionType = $type;
	}
	public function fill($array){
		foreach($array as $k=>$position){
			$this->offsetSet(null,$position);
		}
	}
    public function offsetSet($offset, $value) 
    {
		$value_type = strtolower(TypedClass::getType($value));
		$real_type = strtolower($this->_collectionType);		
		if($real_type != 'object') {
		
			if(((!$this->_isObject && $real_type != $value_type) || ($this->_isObject && ( $real_type != $value_type && !is_subclass_of($value_type,$real_type)))) && $real_type != 'mixed'){
				throw new \Exception("Can not add '".ucfirst(TypedClass::getType($value))."' type to a Collection of '{$this->_collectionType}'. Collection items must be of type '{$this->_collectionType}'.");
			}
		}
		
		if (is_null($offset)) {
            $this->_collectionData[] = $value;
        } else {
            $this->_collectionData[$offset] = $value;
        }
    }

    public function offsetExists($offset) 
    {
        return isset($this->_collectionData[$offset]);
    }

    public function offsetUnset($offset) 
    {
        unset($this->_collectionData[$offset]);
    }

    public function offsetGet($offset) 
    {
        return isset($this->_collectionData[$offset]) ? $this->_collectionData[$offset] : null;
    }

    public function rewind() {
            reset($this->_collectionData);
    }

    public function current() {
            return current($this->_collectionData);
    }

    public function key() {
            return key($this->_collectionData);
    }

    public function next() {
            return next($this->_collectionData);
    }

    public function valid() {
            return $this->current() !== false;
    }   

    public function count() {
     return count($this->_collectionData);
    }
	
	
	public function filter(){
		
	}
	public function toArray(){
		$arrayReturn = array();
		foreach($this->_collectionData as $k=>$data){
			$arrayReturn[$k] = $data;
		}
		return $arrayReturn;
	}
	
}
