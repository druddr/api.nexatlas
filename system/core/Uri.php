<?php

/**
 * Uri short summary.
 *
 * Uri description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Uri
{
	const LOWER_CASE = 'LOWER_CASE_MODE';
	const UPPER_CASE = 'UPPER_CASE_MODE';
	const KEEP_CASE = 'KEEP_CASE_MODE';

    public $parts;
	public $verb;
	public $uriString;
	public $uriParams=array();
	public function __construct($uri=null,$case=Uri::KEEP_CASE){
		$prefix = trim(strtolower(config::$BaseDomain),"/");
		$final_uri = trim($_SERVER['REQUEST_URI'],"/");



		if (substr($final_uri, 0, strlen($prefix)) == $prefix) {
			$final_uri = substr($final_uri, strlen($prefix));
		}
		switch($case){
			case Uri::LOWER_CASE:
				$final_uri = strtolower($final_uri);
				break;
			case Uri::UPPER_CASE:
				$final_uri = strtoupper($final_uri);
				break;
			case Uri::KEEP_CASE:
			default:
				break;

		}

		$this->uriString = is_null($uri) ? trim($final_uri,"/") : $uri;
		$parts = explode("?",$this->uriString);
		if(isset($parts[1])){
			$params = explode("&",$parts[1]);
			foreach($params as $param){
				list($name,$value) = explode("=",$param);
				if(isset($this->uriParams[$name]) && !is_array($this->uriParams[$name])){
					$old = $this->uriParams[$name];
					$this->uriParams[$name] = [];
					$val = json_decode(urldecode($old));
					if(!empty($old) && empty($val)){
						$val = (urldecode($old));
					}
					$this->uriParams[$name][] = $val;
				}
				if(isset($this->uriParams[$name]) && is_array($this->uriParams[$name])){
					$val = json_decode(urldecode($value));
					if(!empty($value) && empty($val)){
						$val = (urldecode($value));
					}
					$this->uriParams[$name][] = $val;
				}
				else{
					$val = json_decode(urldecode($value));
					if(!empty($value) && empty($val)){
						$val = (urldecode($value));
					}
					$this->uriParams[$name] = $val;
				}
			}
		}



		$this->parts = explode("/",$parts[0]);
		$this->verb = strtolower($_SERVER['REQUEST_METHOD']);
	}
}
