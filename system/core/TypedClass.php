<?php
/**
 * Summary of TypedClass
 *
 * @version 1.0
 * @author bruno.teixeira.silva - aboutblank.com.br
 */
abstract class TypedClass{
	/**
	 * Indica se a classe irá jogar exceções ou não
	 */
	protected $exceptions = true;
	/**
	 * Ira receber a instância da classe filha
	 */
	protected $reflector=null;
	/**
	 * Ira receber a instância da classe pai
	 */
	protected $parentReflector=null;
	/**
	 * Indica se a versão da \ReflectionClass(navite) suporta edição de propriedades privadas.
	 */
	protected $isPrivatePropertySuported=null;
	/**
	 * Retorna a instância da \ReflectionClass da classe filha
	 */
	public function getReflector(){
		if(is_null($this->reflector)){
			$this->reflector = new \ReflectionClass($this);
		}
		return $this->reflector;
	}
	public function getParentReflector(){
		if(is_null($this->parentReflector)){
			$this->parentReflector = new \ReflectionClass(get_parent_class($this));
		}
		return $this->parentReflector;
	}
	public function hasMethod($method_name){
		$has_method =$this->getReflector()->hasMethod($method_name);
		if(!$has_method){
			$has_method =$this->getParentReflector()->hasMethod($method_name);
		}
		return $has_method;
	}
	public function hasProperty($property_name){
		$has_property = $this->getReflector()->hasProperty($property_name);
		if(!$has_property){
			$has_property = $this->getParentReflector()->hasProperty($property_name);
		}
		return $has_property;
	}
	public function getProperty($property_name){
		try{
			$property = $this->getReflector()->getProperty($property_name);
		}
		catch (Exception $exception)
		{
			$property = $this->getParentReflector()->getProperty($property_name);
		}
		return $property;
	}
	public function getMethod($method_name){
		try{
			$method = $this->getReflector()->getMethod($method_name);
		}
		catch (Exception $exception)
		{
			$method = $this->getParentReflector()->getMethod($method_name);
		}
		return $method;
	}
	/**
	 * Este método mágico ira receber todas as chamadas a propriedades que
	 * não podem ser acessadas diretamente.
	 * Para utilizar esta lógica, as propriedades que deverão ter validação de tipo precisam
	 * ser privadas ou protegidas, para que este método possa ser chamado, pois, este método
	 * só é chamado quando a propriedade/método não é encontrado na classe ou é inacessível.
	 * Portanto as classes que extenderão TypedClass devem possuir suas propriedades tipadas
	 * de forma privada ou protegida para que esta lógica possa ser utilizada.
	 */
	public function &__get($name){
		if(empty($name)){
			$this->throwEmptyPropertyName();
		}
		//O padrão dos nomes das propriedades deve ser começando com letra minúscula.
		$property_name = strtolower(substr($name,0,1)).substr($name,1,strlen($name));
		//O padrão dos métodos get de propriedades devem ser 'get'+nome da propriedade começando com letra maiúscula
		$method_name = "get".ucfirst($name);
		//Verifica se o método existe
		$has_method =$this->hasMethod($method_name);
		//Verifica se a propriedade existe
		$has_property = $this->hasProperty($property_name);
		//Receberá o objeto ReflectionProperty
		$prop;
		//Receberá o objeto ReflectionMethod
		$method;

		//Se não possui nem método nem propriedade joga uma exceção
		if(!$has_property && !$has_method){
			$this->throwPropertyDontExistsException($name);
		}
		//Busca o acesso a propriedade
		$access = $this->getPropertyRead($property_name,$method_name,$has_property,$has_method);
		//Se o acesso for igual a Denied então joga uma exceção informando.
		if($access===false){
			$this->throwAccessReadException($property_name);
		}
		//Procura saber se a classe possui um método get para a propriedade
		if($has_method){
			//Este valor será utilizado para verificar se o tipo é Collection
			$type = $this->getPropertyType($property_name,$method_name,$has_property,$has_method);
			//Se possui um método get E suporta edição de propriedades privadas
			//então utiliza o refletor para chamar o método.
			//Esta é a maneira mais rápida e permite edição de propriedades privadas
			if($this->isPrivatePropertySuported()){
				$method = $this->getMethod($method_name);
				$method->setAccessible(true);
				$value = $method->invoke($this);
				$method->setAccessible(false);

				if(strpos(strtolower($type),"collection")>-1 && is_null($value)){
					$this->throwCollectionNotSet($property_name);
				}
				return $value;
			}
			//Se não possui então chama da maneira comum.
			return	$this->{$method_name}();
		}
		//Chega aqui se não possui um método get.
		//então procura se existe uma propriedade com o nome requisitado
		if($has_property){
			//Se possui uma propriedade E suporta edição de propriedades privadas
			//então utiliza o refletor para modificar a propriedade.
			//Esta é a maneira mais rápida e permite edição de propriedades privadas
			if($this->isPrivatePropertySuported()){

				$prop = $this->getProperty($property_name);
				$prop->setAccessible(true);
				$value = $prop->getValue($this);
				$prop->setAccessible(false);

				return $value;
			}
			//Se chegou aqui então retorna a propriedade da maneira comum.
			//Não supota propriedades privadas.
			return $this->{$property_name};
		}
		//Caso chegue aqui é porque não existe método ou propriedade com o nome solicitado
		//Limpa as variáveis e joga uma exeção na tela Joga uma exceção na tela
		$exeptionText = "Property ".get_class($this)."::{$property_name} not found.";
		unset($name);
		unset($prop);
		unset($method);
		unset($this);
		unset($method_name);
		unset($property_name);
		throw new \LogicException($exceptionText);
	}
	/**
	 *
	 */
	public function __set($name, $value){
		if(empty($name)){
			$this->throwEmptyPropertyName();
		}
		$property_name = strtolower(substr($name,0,1)).substr($name,1,strlen($name));
		$method_name = "set".ucfirst($name);
		$has_property = $this->hasProperty($name);

		$has_method = $this->hasMethod($method_name);

		$type = NULL;
		if(!$has_property && !$has_method){
			$this->throwPropertyDontExistsException($name);
		}
		//Busca o acesso a propriedade
		$access = $this->getPropertyWrite($property_name,$method_name,$has_property,$has_method);
		//Se o acesso for igual a Denied então joga uma exceção informando.
		if($access===false){
			$this->throwAccessWriteException($property_name);
		}
		$type = $this->getPropertyType($property_name,$method_name,$has_property,$has_method);
		//Verifica o tipo da variável
		$this->checkType($value,$property_name,$type);
		//Se possui um método set E suporta edição de propriedades privadas
		//então utiliza o refletor para chamar o método.
		//Esta é a maneira mais rápida e permite edição de propriedades privadas
		if($has_property && !$has_method){
			if($this->isPrivatePropertySuported()){
				$prop = $this->getProperty($property_name);
				$prop->setAccessible(true);
				$prop->setValue($this,$value);
				return;
			}
			//Se não possui então chama da maneira comum.
			$this->{$property_name} = $value;
			return;
		}
		//Se possui um método set E suporta edição de propriedades privadas
		//então utiliza o refletor para chamar o método.
		//Esta é a maneira mais rápida e permite edição de propriedades privadas
		if($this->isPrivatePropertySuported()){
			$method = $this->getMethod($method_name);
			$method->setAccessible(true);
			$method->invoke($this,$value);
			return;
		}
		//Se não possui então chama da maneira comum.
		return	$this->{$method_name}($value);
	}
	public $teste=null;
	public function __isset($name){
		try {
			$var = $this->__get($name);
		}
		catch(Exception $e){
			$var = false;
		}
		return $var!=null && $var!=false ;

	}
	/**
	 * Verifica o tipo da variável requerida
	 * @param $value Valor que foi passado no método Get
	 * @param $property_name Nome da propriedade
	 * @param $method_name Nome do método set
	 * @param $has_property Se possui a propriedade
	 * @param $has_method Se possui o método
	 */
	public function checkType($value,$property_name,$type){
		$realType = strtolower(trim(str_replace(array('[',']'),'',substr($type,0,(strpos($type,"[")>0?strpos($type,"["):strlen($type))))));
		$value_type = strtolower(self::getType($value));
		$is_object = is_object($value);

		if(((!$is_object &&  $realType!==$value_type)
		  ||($is_object && ($realType!==$value_type && !is_subclass_of($value_type,$realType) )))
		  && $type!="mixed"){
			$this->throwTypeException($property_name,$realType,$value_type);
		}
	}

	//EXCEPTIONS
	/**
	 * \Exception para quando uma variável do tipo Collection esta sendo requerida e não foi setada;
	 * Variáveis do tipo Collection DEVEM ser setadas sempre.
	 * @param $propertyName nome do item de array
	 * @param $itemType tipo configurado no item de array
	 * @param $wrongType tipo passado para o item de array
	 * @param $itemPosition posição do item de array
	 */
	public function throwCollectionNotSet($propertyName){
		throw new \Exception("Property ".get_class($this)."::{$propertyName}, Collection type, was not set. Please set before using Collection properties.");
	}
	/**
	 * \Exception enviada do tipo array
	 * @param $propertyName nome do item de array
	 * @param $itemType tipo configurado no item de array
	 * @param $wrongType tipo passado para o item de array
	 * @param $itemPosition posição do item de array
	 */
	public function throwArrayItemTypeException($propertyName,$itemType,$wrongType,$itemPosition){
		throw new \Exception("Can not add ".ucfirst($wrongType)." type to an Array of ".ucfirst($itemType).". String passed at position ".$itemPosition.". Property ".get_class($this)."::{$propertyName} array items must be of type '{$itemType}'.");
	}
	/**
	 * \Exception para propriedades inexistentes
	 * @param $propertyName nome da propriedade
	 */
	public function throwPropertyDontExistsException($propertyName){
		throw new \Exception("Property [".get_class($this)."::".$propertyName."] does not exists.");
	}
	/**
	 * \Exception para propriedades com tipo diferente do padrão
	 * @param $propertyName nome da propriedade
	 * @param $type tipo configurado na propriedade
	 * @param $wrongType tipo passado para a propriedade
	 */
	public function throwTypeException($propertyName,$type,$wrongType){
		throw new \Exception("Property ".get_class($this)."::{$propertyName} must be of type '{$type}'. Value of '{$wrongType}' type was used instead");
	}
	/**
	 * \Exception para propriedades que não possuem acesso de leitura na variável.
	 * @param $propertyName nome da propriedade
	 */
	public function throwAccessReadException($propertyName){
		throw new \Exception("Property ".get_class($this)."::{$propertyName} is not read accessible from outside class. Check @property-read var defined in comment.");
	}
	/**
	 * \Exception para propriedades que não possuem acesso de escrita na variável.
	 * @param $propertyName nome da propriedade
	 */
	public function throwAccessWriteException($propertyName){
		throw new \Exception("Property ".get_class($this)."::{$propertyName} is not write accessible from outside class. Check @property-write var defined in comment.");
	}
	/**
	 * \Exception para propriedades com tipo diferente do padrão
	 * @param $propertyName nome da propriedade
	 * @param $type tipo configurado na propriedade
	 * @param $wrongType tipo passado para a propriedade
	 */
	public function throwEmptyPropertyName(){
		throw new \Exception("Cannot access empty property name.");
	}
	//EXCEPTIONS END
	/**
	 * Retorna o tipo da propriedade requerida
	 * @param String $property_name nome da propriedade
	 * @param String $method_name nome do possível método
	 * @param Boolean $is_property indica se é uma propriedade
	 * @param Boolean $is_method indica se é também um método
	 * @return String
	 */
	public function getPropertyType($property_name,$method_name,$is_property,$is_method){
		$type_clean = $this->getCommentVar("@var",$property_name, $method_name, $is_property,$is_method);
		return empty($type_clean) ? "mixed" : $type_clean;
	}
	/**
	 * Retorna o nível de acesso da propriedade requerida
	 * Se é private, protected ou public
	 * @param String $property_name nome da propriedade
	 * @param String $method_name nome do possível método
	 * @param Boolean $is_property indica se é uma propriedade
	 * @param Boolean $is_method indica se é também um método
	 * @return String
	 */
	public function getPropertyWrite($property_name,$method_name,$is_property,$is_method){
		return  $this->docVarExists("@property-write",$property_name, $method_name, $is_property,$is_method);
	}
	/**
	 * Retorna o nível de acesso da propriedade requerida
	 * Se é private, protected ou public
	 * @param String $property_name nome da propriedade
	 * @param String $method_name nome do possível método
	 * @param Boolean $is_property indica se é uma propriedade
	 * @param Boolean $is_method indica se é também um método
	 * @return String
	 */
	public function getPropertyRead($property_name,$method_name,$is_property,$is_method){
		return $this->docVarExists("@property-read",$property_name, $method_name, $is_property,$is_method);
	}
	/**
	 * Retorna a variável da propriedade requerida
	 * Sempre da prioridade a variável definida no método get/set
	 * de modo que, se uma variável foi definida como privada mas existe
	 * um método get/set para ela, o método será chamado, se estiver apto
	 * @param String $var nome da variável que será achada
	 * @param String $property_name nome da propriedade
	 * @param String $method_name nome do possível método
	 * @param Boolean $is_property indica se é uma propriedade
	 * @param Boolean $is_method indica se é também um método
	 * @return String
	 */
	public function getCommentVar($var,$property_name,$method_name,$is_property,$is_method){
		$reflectorMethod = ($is_method) ? 'getMethod' : 'getProperty';
		$name_to_call = ($is_method) ? $method_name : $property_name;
		$property =	$this->$reflectorMethod($name_to_call);
		$comment = $property->getDocComment();
		if(empty($comment) && $is_property){
			$property =	$this->getProperty($property_name);
			$comment = $property->getDocComment();
		}
		$var_definition = trim(substr($comment,strpos($comment,$var)+strlen($var)));

		$type_dirty = substr($var_definition,0,strpos($var_definition," "));
		$type_clean = preg_replace("/([^a-zA-Z0-9()\[\]])/","",$type_dirty);

		return $type_clean;
	}
	public function hasComment($var,$property_name,$method_name,$is_property,$is_method){
		$reflectorMethod = ($is_method) ? 'getMethod' : 'getProperty';
		$name_to_call = ($is_method) ? $method_name : $property_name;
		$property =	$this->$reflectorMethod($name_to_call);
		$comment = $property->getDocComment();
		if(empty($comment) && $is_property){
			$property =	$this->getProperty($property_name);
			$comment = $property->getDocComment();
		}
		return stripos($comment,$var)>-1;
	}
	/**
	 * Indica se uma determinada tag do PHP Doc existe no comentário
	 * @param String $var nome da variável que será achada
	 * @param String $property_name nome da propriedade
	 * @param String $method_name nome do possível método
	 * @param Boolean $is_property indica se é uma propriedade
	 * @param Boolean $is_method indica se é também um método
	 * @return Boolean
	 */
	public function docVarExists($var,$property_name,$method_name,$is_property,$is_method){
		$reflectorMethod = ($is_method) ? 'getMethod' : 'getProperty';
		$name_to_call = ($is_method) ? $method_name : $property_name;
		$property =	$this->$reflectorMethod($name_to_call);
		$comment = $property->getDocComment();
		if(empty($comment) && $is_property){
			$property =	$this->getProperty($property_name);
			$comment = $property->getDocComment();
		}
		return strpos($comment,$var)>-1;
	}
	/**
	 * retorna o tipo do objeto passado por parâmetro
	 * @param $var Mixed
	 * @return String
	 */
	public static function getType($var){
		if(is_array($var)) return 'array';
		if(is_object($var)) return get_class($var);
		if(is_null($var)) return 'null';
		if(is_string($var)) return 'string';
		if(is_int($var)) return 'integer';
		if(is_bool($var)) return 'boolean';
		if(is_float($var)) return 'float';
		if(is_resource($var)) return 'resource';
		//throw new NotImplementedException();
		return 'unknown';
	}
	public function getValueBasedOnType($type){

	}
	/**
	 * Verifica pela versão do PHP se o ReflectionClass irá ter acesso
	 * ou não à propriedades privadas das classes para setar/capturar
	 * os dados das mesmas.
	 * @return Boolean
	 */
	public function isPrivatePropertySuported(){
		if(is_null($this->isPrivatePropertySuported)){
			$version = substr(phpversion(),2,1);
			$majorVersion = substr(phpversion(),0,1);
			$this->isPrivatePropertySuported = $majorVersion==7 || ($majorVersion>=5 && $version >= 3) ? true : false;
			unset($version);
		}
		return $this->isPrivatePropertySuported;
	}
}
