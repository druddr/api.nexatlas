<?php

/**
 * Route short summary.
 *
 * Route description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Route extends TypedClass
{
	/**
	 * @var String
	 * @property-read
	 * @property-write
	 */
	private $controller;	
	private function getController(){
		return $this->controller;
	}
	private function setController($value){
		$this->controller = $value;		
	}
	/**
	 * @var String
	 * @property-read
	 * @property-write
	 */
	private $action;
	/**
	 * @var Array
	 * @property-read
	 * @property-write
	 */
	public $params;
	private function getParams(){
		return $this->params;
	}
	
	private function setParams($value){
		$this->params = $value;		
	}
	/**
	 * @var String
	 * @property-read
	 * @property-write
	 */
	private $view;
	
	
    public function __construct($controller=null,$action=null,$params=null){
		$this->controller = $controller;
		$this->action = $action;
		$this->params = $params;
	}
}
