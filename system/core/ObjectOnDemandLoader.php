<?php

/**
 * ObjectOnDemandLoader short summary.
 *
 * ObjectOnDemandLoader description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ObjectOnDemandLoader
{
	private $prefix;
	private $suffix;
	private $properties = array();
    
	public function __construct($prefix='',$suffix=''){
		$this->prefix = $prefix;
		$this->suffix = $suffix;
	}
	
	public function __get($property){
		if(!isset($this->properties[$property])){
			$className = $this->prefix.$property.$this->suffix;
			if(!class_exists($className)){
				throw new Exception("{$property} {$this->prefix} class not found.");
			}
			$this->properties[$property] = new $className();
		}
		return $this->properties[$property];
	}

	public function __set($property,$value){
		$this->properties[$property] = $value;
	}
}
