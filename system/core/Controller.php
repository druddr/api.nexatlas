<?php

/**
 * Controller short summary.
 *
 * Controller description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
abstract class Controller extends TypedClass implements IController
{
	public function __construct(Router $router) {
		$this->data = new stdClass();
		$this->mvc = new Mvc($this);
		$this->router = $router;
		if(!$router->isAjax()){
			$this->design = new Design($this);
		}
		$this->models = new ObjectOnDemandLoader('Model');
		$this->views = new ObjectOnDemandLoader('View');
	}

	/**
	 * @property-read
	 * @var Router
	 */
	protected $router;

	/**
	 * @property-read
	 * @property-write
	 * @var Mvc
	 */
	private $mvc;

	public $data;

	/**
	 * Refers to subroutes like Order/1/Services, or Order/4/Product, or Company/2/Service
	 * Where Services is the controller and will receive the id of its parent (e.g. Order)
	 */
	protected $parentId;
	/**
	 * Indicates that the REST call to an action is passing multiple ID's
	 * The format expected is controller/action/N1,N2,N...
	 */
	protected $hasMultipleId = false;
	/**
	 * This is the reference, for easy access, of the object of Design class. It won't be changed.
	 * @property-read
	 * @var Design
	 */
	private $design;
	/**
	 * This is the name of the page design
	 * By default it will use the Config::DefaultDesign property
	 * @var String
	 */
	protected $page_design;
	/**
	 * This is the name of Layout that will be used to render all views on this controller
	 * It might be changed at any moment
	 * @property-write
	 * @property-read
	 * @var String
	 */
	private $layout;

	/**
	 * This contains all the css that will be autoloaded inside the view
	 * @var array(String)
	 */
	public $default_css = array();
	/**
	 * This contains all the js that will be autoloaded inside the view
	 * @var array(String)
	 */
	public $default_js = array();

	/**
	 * This contains all the models which will be autoloaded inside a property
	 * This object will only create models instances when program first call model]
	 * $controller->models->ModelNameX
	 * Only on that moment and beyond model will be an instance, not before, so,
	 * connection will only be instanced after model is called from controller, or, ofc, manually
	 * @property-read
	 * @var Object
	 */
	protected $models=null;
	/**
	 * This object is exactly like $models but we have it only to disting between common tables and views
	 * @property-read
	 * @var Object
	 */
	private $views=null;
	/**
	 * This method will set up a new layout instead of the current one
	 * It will call design->setLayout method
	 * @param string $layout Must be a path to desired layout
	 */
	private function setLayout($layout){
		$this->design->setLayout($layout);
	}
	public function __destruct(){
		unset($this->mvc);
		unset($this->data);
		unset($this->layout);
		unset($this->design);
		unset($this->models);
		unset($this->load_models);
	}

	/**
	 * This method will set up a new layout instead of the current one
	 * It will call design->setLayout method
	 * @param string $mode Must be one of the available options
	 */
	protected function returnMode($mode){
		return new Header();
	}
	public function setHasMultipleId($hasMultipleId){
		$this->hasMultipleId = $hasMultipleId;
	}
	public function hasMultipleId(){
		return $this->hasMultipleId;
	}
	public function setParentId($id){
		$this->parentId = $id;
	}
	public function getParentId($id){
		$this->parentId = $id;
	}

	public function grantFilters(&$object,$expectedFilters){
		if(!$object){
			$object = new stdClass();
		}
		foreach($expectedFilters as $filter){
			if(isset($object->{$filter})) continue;
			$object->{$filter} = '@SOME#STRING$THAT%WILL}{PROBLABLY&NOT*BE(ON)ANY_FIELD';
		}
	}

	private static $singletons = [];


	public static function __callStatic($method, $args){
		$class = get_called_class();
		$method = substr($method,1);

		//if(!method_exists($class,$method)){
		//	throw new Exception("Method {$class}::{$method} does not exists");
		//}
		if(!isset(Controller::$singletons[$class])){
			Controller::$singletons[$class]  = new $class(Router::getInstance());
		}
		return call_user_func_array([Controller::$singletons[$class],$method],$args);
	}

	public function __call($method, $args){
		//Using call to set property like
		// $blainstance->id(1)
		//will make
		// $blainstance->id = 1
		//To allow setting properties and chain method
		// $blainstance->id(1)->name('bla')
		$this->{$method} = $args[0];
		return $this;
	}
}
