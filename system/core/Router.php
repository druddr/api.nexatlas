<?php

/**
 * Router short summary.
 *
 * Router description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class Router
{
	/**
	 * @var Route
	 * @property-read
	 */
    private $route;
	/**
	 * @var Uri
	 * @property-read
	 */
	private $uri;
	/**
	 * @var Controller
	 */
	private $controller;
	private $controllerIndex = 0;
	private $controllerDefined = false;
	private $controllerNameIsAlsoDir = false;
	private $baseContext = false;
	private $viewNotSet = false;
	private $isAjax = false;
	private $isREST = false;
	private $controllerParentId = false;
	private $controllerHasMultipleId = false;
	/**
	 * @var Boolean
	 * @property-read
	 */
	private $preventChangingRoute=FALSE;

	private $isErrorRouting=FALSE;

	#region SINGLETON OBJECT
	/**
	 * @var Router
	 * @property-read
	 */
	private static $instance;
	public static function getInstance(){
		if(!isset(self::$instance)){
			self::$instance = new Router();
		}
		return self::$instance;
	}
	private function __construct(){
		$this->uri = new Uri();
		$this->route = new Route();
	}
	#endregion
	/**
	 * Returns the string of the current route uri
	 * @return mixed
	 */
	public function getCurrentRouteUri(){
		return $this->uri->uriString;
	}


	/**
	 * Summary of ChangeRoute
	 * @param Route $route
	 * @param $prevent
	 * @return
	 */
	public function ChangeRoute(Route $route,$prevent=FALSE){
		$route->params = [];
		$this->uri = new Uri($route->controller.'/'.$route->action.'/'.implode("/",$route->params));
		$this->controllerIndex = 0;
		$this->viewNotSet = false;
		return $this->FindRoute($prevent);
	}
	/**
	 * Summary of FindRoute
	 * @param $prevent
	 * @return
	 */
	public function FindRoute($prevent=FALSE){
		$this->preventChangingRoute = $prevent;
		$this->CheckOptionsRequest();
		$this->FetchDesignRequest();
		$this->FindController();
		$this->FindAction();
		$this->FindParams();
		$this->FindView();
		$this->CheckAuth();
		$this->ExecuteRouting();
        return true;
	}
	public function CheckOptionsRequest(){
		if(strtoupper($_SERVER['REQUEST_METHOD'])!='OPTIONS') return;
		Response::ok(true);
	}
	/**
	 * Summary of setErrorRouting
	 * @param Exception $e
	 * @return
	 */
	public function setErrorRouting($e){
		if(config::$DevMode){
			print_r($e);
			exit;
		}
		if($this->isAjax || $this->isREST){
			echo json_encode(['error'=>true,'message'=>$e->getMessage(),'code'=>$e->getCode()]);
			exit;
		}
		$errorMessage = !CustomString::isNullOrEmpty($e->getMessage()) ? $e->getMessage() : null;

		if($this->preventChangingRoute!==FALSE) return false;

		//SET params with the current params in URL more an extra with the error message url encoded

		$params = isset($this->route->params) ? $this->route->params : array();

		if(!empty($errorMessage) && !is_null($errorMessage)){
			$params['ERROR_MESSAGE'] = urlencode($errorMessage);
		}
		$this->isErrorRouting=TRUE;
		//Change the route to de error Controller
		return $this->ChangeRoute(new Route(config::DefaultErrorController,config::DefaultErrorAction,$params),true);
	}

	/**
	 * Summary of isDesignRequest
	 */
	private function FetchDesignRequest(){
		return;
	}


	/**
	 * Summary of FindController
	 */
	private function FindController(){
		$this->baseContext = new CustomString("/");
		$baseContext = $this->baseContext;
		$controllerIndex = $this->controllerIndex;
		/**
		 * TODO - remove this on a loop considering multiple folders /bla/ble/bli/blo/blu
		 */
		if($this->uri->parts[0]==trim(config::$BaseDomain,"/")){
			unset($this->uri->parts[0]);
		}
		$hasSubRoute = false;
		$mainRouteIndex = false;

		foreach($this->uri->parts as $index=>$part){

			//Checks whether uri parts has more than 3 parts (possible subroute)
			if(isset($this->uri->parts[$index+1])
				&& isset($this->uri->parts[$index+2])) {
				//Checks whether the next uri part is a numeric + there's a controller after the identificator (indicates sub route)
				if(
					(
					is_string($this->uri->parts[$index+1])
					&& strlen($this->uri->parts[$index+1]) > 2
					&& is_numeric($this->uri->parts[$index+2])
					&& count($this->uri->parts) > 3
					)
					|| (
					is_numeric($this->uri->parts[$index+1])
					&& is_string($this->uri->parts[$index+2])
					&& strlen($this->uri->parts[$index+2]) > 2)){
					$hasSubRoute = true;
					$mainRouteIndex = $index;
				}
			}

			if(!($hasSubRoute && $mainRouteIndex==$index)) {
				//In case it is already a controller, we need to do a check whether it is a folder too
				$this->IsController($this->baseContext->concat(ucfirst(dashToCamel($part))));
			}

			//If controller was defined, though, it is also a dir
			if($this->controllerDefined) break;
			if($hasSubRoute) {
				//If the current part is numeric
				if (is_numeric($part)) {
					$this->controllerParentId = $part;
					$this->controllerIndex++;
					continue;
				}

				//Subroute defined, checks whether the current part is a dir
				//$this->IsDir($this->baseContext->concat(ucfirst(dashToCamel($part))));
			}
			$this->baseContext = $this->baseContext->concat($part,"/");
			$this->controllerIndex++;
		}

		$controller = $this->isErrorRouting ? config::DefaultErrorController : config::DefaultController;
		$IsController = $this->controllerDefined ? true : $this->IsController($controller,$baseContext,$controllerIndex);

		if(!$this->controllerDefined && $IsController){
			$this->baseContext = $baseContext;
			$this->controllerIndex = $controllerIndex;
			$this->route->controller = config::DefaultController;
		}
		else if(!$this->controllerDefined && !$IsController){
			$this->ThrowControllerNotExistsException($controller);
		}
	}
	/**
	 * Summary of FindAction
	 */
	private function FindAction(){
		$actionIndex = $this->controllerIndex+1;

		$this->controllerHasMultipleId = isset($this->uri->parts[$actionIndex]) ? strpos($this->uri->parts[$actionIndex],',') : false;

		if(isset($this->uri->parts[$actionIndex])
			&& !$this->IsAction(dashToCamel($this->uri->parts[$actionIndex]))
			&& !is_numeric($this->uri->parts[$actionIndex])
			&& !$this->controllerHasMultipleId){
			$this->ThrowActionNotExistsException(dashToCamel($this->uri->parts[$actionIndex]));
		}

		$headers = getallheaders();
		$this->isAjax = isset($headers['Rest']) || (isset($headers['X-Requested-With']) && $headers['X-Requested-With']=='XMLHttpRequest');

		if($this->isAjax && (((isset($this->uri->parts[$actionIndex])
			&& is_numeric($this->uri->parts[$actionIndex])) || $this->controllerHasMultipleId ) || !isset($this->uri->parts[$actionIndex])
			&& !empty($this->uri->parts[$this->controllerIndex]) )
			&& !$this->isErrorRouting) {
			$this->isREST = true;
			return;
		}

		if(!isset($this->uri->parts[$actionIndex])){
			$this->route->action = config::DefaultAction;
			if(!$this->IsAction(config::DefaultAction)){
				$actionException = isset($this->uri->parts[$actionIndex]) ? $this->uri->parts[$actionIndex] : config::DefaultAction;
				$this->ThrowActionNotExistsException($actionException);
			}
			return;
		}
		$this->route->action = dashToCamel($this->uri->parts[$actionIndex]);
	}
	/**
	 * Summary of FindParams
	 */
	private function FindParams(){
		$this->route->params = array();
		//What the heck?
		$paramindex = $this->isREST ? $this->controllerIndex+1 : $this->controllerIndex+2;
		foreach($this->uri->parts as $k=>$part){
			if($k<$paramindex) continue;
			$this->route->params[] = urldecode($part);
		}
		if($this->controllerHasMultipleId){
			$this->route->params[0] = explode(',',$this->route->params[0]);
		}
		//I got to do this right here to make sure the params came from the URI not somewhere else
		$hasId = count($this->route->params) > 0 && is_numeric($this->route->params[0]);
		$paramnext = count($this->route->params);

		if(count($_GET) > 0) $this->route->params[$paramnext] = new stdClass();

		foreach($this->uri->uriParams as $k=>$part){
			$this->route->params[$paramnext]->{$k} = $part;
		}
		//Not getting parameters from $_GET because PHP
		//does not handle multiple GET params with the same name
		//like a=1&a=2 should be a[0] = 1 a[1]=2
		//$queryString  = explode('&', $_SERVER['QUERY_STRING']);
		//foreach($queryString as $param) {
		//    if(empty($param)) continue;
		//    list($name, $value) = explode('=', $param, 2);
		//    $paramname = urldecode($name);
		//    $paramvalue = json_decode(urldecode($value));
		//    //If the param was already set than we are facing multiple params
		//    //with the same name
		//    if(isset($this->route->params[$paramnext]->{$paramname}) && !is_array($this->route->params[$paramnext]->{$paramname})){
		//        $firstValue = $this->route->params[$paramnext]->{$paramname};
		//        $this->route->params[$paramnext]->{$paramname}[] = $firstValue;
		//        $this->route->params[$paramnext]->{$paramname}[] = $paramvalue;
		//    }
		//    //First set of the param
		//    //It will not be an array to keep the single defined params
		//    if(!isset($this->route->params[$paramnext]->{$paramname})){
		//        $this->route->params[$paramnext]->{$paramname} = $paramvalue;
		//    }
		//    //here we already have an array set because of multiple
		//    //params of the same name
		//    if(is_array($this->route->params[$paramnext]->{$paramname})){
		//        $this->route->params[$paramnext]->{$paramname}[] = $paramvalue;
		//    }
		//    //Sometimes the url is not set correctly
		//    if($paramvalue==null && urldecode($value)!=null) {
		//        $this->route->params[$paramnext]->{$paramname} = urldecode($value);
		//    }
		//}

		$this->route->params[count($this->route->params)] = json_decode(file_get_contents('php://input'));

		$copy = $this->route->params;
		foreach($copy as $k=>$param){
			if(is_object($param) && count(get_object_vars($param))==0){
				unset($this->route->params[$k]);
			}
		}

		if(!$this->isREST) return;

		$this->route->action = config::$RESTMethodNames[strtoupper($this->uri->verb)];
		switch($this->uri->verb){
			case 'get':
				if(!$hasId) $this->route->action = config::$RESTMethodNames["QUERY"];
				break;
			case 'post':
				if(isset($this->route->params[0]) && is_numeric($this->route->params[0])){
					$this->route->action = config::$RESTMethodNames["PUT"];
				}
				if(count($_GET)>0) $this->route->action = config::$RESTMethodNames["PUT"];
				break;
		}
	}

	private function isViewFile($route,$controller,$filePath,$extension) {
		if(!is_file($filePath.$controller.'/'.$route->action.$extension)){
			$controller = str_replace("Controller","",$route->controller);
			if(!is_file($filePath.$controller.'/'.$route->action.$extension)){
				return false;
			}
			return true;
		}
		return true;
	}
	/**
	 * Summary of FindView
	 */
	public function FindView($parts=null){
		if($this->isAjax){
			return;
		}
		$filePath = config::$BasePath.'/app/views/';
		$parts = !is_null($parts) ? $parts : $this->uri->parts;
		foreach($parts as $k=>$part){
			if($k > $this->controllerIndex) break;
			if($k < $this->controllerIndex){
				$filePath.= $part."/";
			}
		}
		$controller = $this->route->controller;


		$extension = '.php';
		if(!$this->isViewFile($this->route,$controller,$filePath,$extension)){
			$extension = '.html';
			if(!$this->isViewFile($this->route,$controller,$filePath,$extension)){
				$this->viewNotSet=true;
			}
			else{
				$this->viewNotSet=false;
			}
		}
		$filePath .= $controller.'/'.$this->route->action.$extension;
		$this->route->view = $filePath;
	}
	/**
	 * Summary of CheckAuth
	 */
	public function CheckAuth(){
		//Session will last a day
		//But it refreshes every time session_regenerate_id() is called
		//So it last a day with no activity on this session
		ini_set('session.cookie_lifetime', 60 * Config::SessionTimeout);
		ini_set('session.gc_maxlifetime', 60 * Config::SessionTimeout);
		session_start();
		session_regenerate_id();
		$reflectionMethod = new ReflectionMethod($this->route->controller,$this->route->action);
		$var ='@'.config::AuthorizationMark;
		$authorized = strpos($reflectionMethod->getDocComment(),$var)>-1;
		if($authorized) return;
		//if there is no @authorized comment on method
		//And there is no Authorization header on request
		//Throws an unauthorized response
		$authorizationClass = config::AuthorizationController;
		$authorizationMethod = config::AuthorizationMethod;
		(new $authorizationClass($this))->{$authorizationMethod}();
	}

	/**
	 * Summary of ExecuteRouting
	 */
	public function ExecuteRouting(){
		$this->controller = new $this->route->controller($this);
		//Sets it even if was not defined
		//It will be false if not defined so that is ok
		$this->controller->setParentId($this->controllerParentId);
		$this->controller->setHasMultipleId($this->controllerHasMultipleId);
		$this->isAjax = $this->controller->hasComment("@ajax",$this->route->action,$this->route->action,false,true);

		////Calls a controller behaviour, passing all specified params
		//try {
		$methodReturn = call_user_func_array( array($this->controller,$this->route->action), $this->route->params );
		//} catch (Exception $exception) {
		//	$methodReturn = [ "error" => true, "message" => $exception->getMessage(), "code" => $exception->getCode() ];
		//}

		if((is_object($methodReturn) && get_class($methodReturn)=='EntityCollection') ||
			(is_object($methodReturn) && is_subclass_of($methodReturn,"EntityCollection"))){
			$methodReturn = $methodReturn->toArray();
		}
		if(!$this->isAjax && $this->viewNotSet){
			$this->ThrowViewNotExistsException($this->route->action,$this->route->controller);
		}
		if($this->isAjax || $this->isREST){
			if(config::$AngularJsJSON) echo ")]}',\n";
			if(config::$UTF8DecodeResult) {
				exit(json_encode(utf8_decode_all($methodReturn), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
			}
			if(config::$UTF8EncodeResult){
				exit(json_encode(utf8_encode_all($methodReturn), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
			}
			exit(json_encode(map_objects($methodReturn), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
			//exit(json_encode($methodReturn, JSON_UNESCAPED_UNICODE));
			//exit(json_encode(utf8_encode_all(utf8_decode_all(utf8_encode_all($methodReturn)))));
		}
		$this->controller->design->setViews($this->route->view);
		$this->controller->design->render();
	}

	/**
	 * Summary of IsController
	 * @param $file
	 * @return
	 */
	public function IsController($file,$baseContext=false,$controllerIndex=false){
		$file = trim($file,"/");
		if(is_file(config::$BasePath."app/controllers/".$file.".php")){
			include(config::$BasePath."app/controllers/".$file.".php");
			$arr = explode("/",$file);
			$className = ucfirst($arr[count($arr)-1]);
			if(!class_exists($className)){
				$className = $className."Controller";
				if(!class_exists($className)){
					$this->ThrowControllerNotExistsException($className);
				}
			}
			if(!in_array("IController",class_implements($className))){
				$this->ThrowControllerNotImplements($className);
			}
			if($baseContext!==false) $this->baseContext = $baseContext;
			if($controllerIndex!==false) $this->controllerIndex = $controllerIndex;
			$this->route->controller = $className;
			$this->controllerDefined=true;
			return true;
		}
		return false;
	}

	/**
	 * Checks whether a filepath is a dir or not
	 * @param $file
	 * @return
	 */
	public function IsDir($file){
		$file = trim($file,"/");
		if(is_dir(config::$BasePath."app/controllers/".$file)){
			$this->controllerNameIsAlsoDir = true;
			return true;
		}
		return false;
	}
	/**
	 * Summary of IsAction
	 * @param $action
	 * @return
	 */
	public function IsAction($action){
		if(method_exists($this->route->controller,$action)){
			$this->route->action = $action;
			return true;
		}
		return false;
	}
	public function isAJax(){

		return $this->isAjax || $this->isREST;
	}
	//EXCEPTIONS
	/**
	 * Summary of throwControllerNotExistsException
	 * @param $className
	 */
	private function ThrowControllerNotExistsException($className){
		throw new Exception("The application controller <b>{$className}</b> does not exists in controllers path.");
	}
	/**
	 * Summary of throwControllerNotExistsException
	 * @param $className
	 */
	private function ThrowViewNotExistsException($view, $controller){
		throw new Exception("The application view <b>{$view}</b> for controller <b>{$controller}</b> does not exists in views path.");
	}
	/**
	 * Summary of throwActionNotExistsException
	 * @param $action
	 */
	private function ThrowActionNotExistsException($action){
		throw new Exception("The application controller method <b>{$this->route->controller}::{$action}</b> does not exists in controllers class.");
	}
	/**
	 * Summary of throwControllerNotImplements
	 * @param $className
	 */
	private function ThrowControllerNotImplements($className){
		throw new Exception("The application controller class <b>{$className}</b> does not implements <b>IController</b> interface or inherits <b>Controller</b> class.");
	}
	/**
	 * Summary of CastExceptionMessage
	 * @param Exception $e
	 */
	public function CastExceptionMessage($e){
		ob_start();
		$message = $e->getMessage();
		include(config::$SystemBasePath."/system/defaults/ErrorPage.php");
		$contents = ob_get_contents();
		ob_end_clean();
		echo $contents;
		exit;
	}
}

