<?php


class Helper{
	public static function utf8_encode_all($object){
		$isString = is_string($object);
		if($object===null) return $object;
		if(is_scalar($object) && !$isString){
			return $object;
		}
		if ($isString) {
			return utf8_encode($object);
		}
		if(is_array($object)){
			foreach ($object as $property => $value) {
				$object[$property] = Helper::utf8_encode_all($value);
			}
			return $object;
		}
		foreach ($object as $property => $value) {
			$object->{$property} = Helper::utf8_encode_all($value);
		}
		unset($isString);
		return $object;
	}
	public static function utf8_decode_all($object){
		$isString = is_string($object);
		if($object===null) return $object;
		if(is_scalar($object) && !$isString){
			return $object;
		}
		if ($isString) {
			return utf8_decode($object);
		}
		if(is_array($object)){
			foreach ($object as $property => $value) {
				$object[$property] = Helper::utf8_decode_all($value);
			}
			return $object;
		}
		foreach ($object as $property => $value) {
			$object->{$property} = Helper::utf8_decode_all($value);
		}
		unset($isString);
		return $object;
	}
}
?>