<?php

/**
 * BasicType short summary.
 *
 * BasicType description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class BasicType
{
    protected $value;
    public function __construct($value){
		$this->value = $value;
	}
	public function __toString(){
		return $this->value;
	}
}
