<?php

/**
 * Model short summary.
 *
 * Model description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 *
 * @version 1.0.1
 * @author bruno.teixeira.silva
 */
class Model implements JsonSerializable
{
	/**
	 * Name of primary key field
	 * @var string
	 */
	public $primary_key = '';
	/**
	 * If the primary key is a string field
	 * @var string
	 */
	public $primary_key_is_string = false;
	/**
	 * By default, keys should be auto_increment
	 * @var bool
	 */
	public $primary_key_is_auto_increment = true;
	/**
	 * This is the table name that's gonna be used on all queries
	 * @var string
	 */
	public $table_name = '';
	/**
	 * Must have the field list. Each field must have it's type set
	 * @var array
	 */
	public $field_config = array();


	public $hasMany = [];
	public $hasOne = [];
	public $belongsTo = [];

	#region Data Types
	const type_int="int";
	const type_smallint="smallint";
	const type_tinyint="tinyint";
	const type_tinyblob="tinyblob";
	const type_blob="blob";
	const type_mediumblob="mediumblob";
	const type_longblob="longblob";
	const type_varchar="varchar";
	const type_binary="binary";
	const type_char="char";
	const type_text="text";
	const type_float="float";
	const type_double="double";
	const type_decimal="decimal";
	const type_boolean="boolean";
	const type_date="date";
	const type_datetime="datetime";
	const type_time="timestamp";
	#endregion

	/**
	 * This constructor will set the properties of this model in case $dataobject param is set.
	 * Also, the connection with database is started here.
	 * *** Database does not connect 2 times at the same database, so there is no problem calling connect
	 * multiple times
	 * @param StdClass $dataobject The data object to create the instance
	 */
	public function __construct(StdClass $dataobject=null){
		$this->fill($dataobject);
	}
	/**
	 * 'records' creates an instance of ActiveRecord class for query purposes
	 * @param StdClass $dataobject
	 * @return ActiveRecord
	 */
	public function records(StdClass $dataobject=null){
		$this->fill($dataobject);
		return new ActiveRecord($this);
	}
	/**
	 * Fills a Model object with data from another Model or stdClass (for instance, an incoming json) object
	 * @param mixed $dataobject
	 * @return Model
	 */
	public function fill($dataobject) {

		if(isset($dataobject) && is_object($dataobject)){
			foreach($this->field_config as $name => $value){
				if($name == 'table_name' || $name == 'primary_key' || $name == 'db' || $name == 'entities') continue;
				if(!property_exists($dataobject,$name)) continue;
				$val = $dataobject->{$name};
				if($value['type']==Model::type_boolean) {
					$val = !(!$dataobject->{$name}) ? '1': '0';
				}
				$this->{$name} = $val;
			}
		}
		return $this;
	}
	/**
	 * Summary of extract
	 * @alias getStandardObject
	 * @return StdClass
	 */
	public function extract(){
		return $this->getStandardObject();
	}
	/**
	 * Summary of getStandardObject
	 * @return StdClass
	 */
	public function getStandardObject(){
		$return = new StdClass();
		foreach($this->field_config as $field=>$config){
			$this->{$field} = str_replace(["\0","\1"], [0,1], $this->{$field});
            //Applies the correct data interface for the standard object
            $return->{$field} = ($config['type'] == Model::type_boolean) ? (bool)(intval($this->{$field}) == 1) : $this->{$field};
		}
		//for newly added properties
		$default_properties = get_class_vars(get_class($this));
		foreach(get_object_vars($this) as $field=>$value){
			if(isset($default_properties[$field])) continue;
			if(is_object($this->{$field})) {
				if(is_subclass_of($this->{$field},"Model")){
					$newField =strtolower($field);
					$return->{$newField} = $this->{$field}->extract();
				}
				else if(is_subclass_of($this->{$field},"EntityCollection") || get_class($this->{$field})=="EntityCollection"){
					$newField =strtolower($field);
					$return->{$newField} = $this->{$field}->map(function($model){
						if(!is_subclass_of($model,"Model")) return $model;
						return $model->extract();
					})->toArray();
				}
				else{
					$newField =strtolower($field);
					$return->{$newField} = $this->{$field};
				}
			}
			else{
				$this->{$field} = str_replace(["\0","\1"], [0,1], $this->{$field});
				$return->{$field} = $value;
			}
		}
		return $return;
	}
	/**
	 * Summary of save
	 * @return bool|PDOStatement
	 */
	public function save($customData=null){
		return $this->records()->save();
	}
	public function insert(){
		return $this->records()->insert();
	}
	public function update($customFields=null){
		return $this->records()->update($customFields);
	}
	/**
	 * Deletes a model object instance from DB
	 * @return bool|PDOStatement
	 */
	public function delete(){
		return $this->records()->delete($this->{$this->primary_key});
	}
	/**
	 * Gets the model object itself, no the stdClass
	 * @param int $id
	 * @return Model
	 */
	public function getModelById(int $id){
		return $this->records()->get($id)->getFirstModel();
	}
	/**
	 * Summary of getById
	 * @param int $id
	 * @return StdClass
	 */
	public function getById(int $id){
		return $this->records()->get($id)->getFirstStandard();
	}
    /**
	 * Summary of toStandard
	 * @return StdClass
	 */
    public function toStandard() {
        return $this->getStandardObject();
    }

	/*MAGIC*/
	/**
	 * Used to HasMany, HasOne and BelongsTo functionality
	 *
	 */
	public function __get($propertyName){
		if(isset($this->hasMany[$propertyName])){
			return $this->hasMany($propertyName);
		}
		if(isset($this->hasOne[$propertyName])){
			return $this->hasOne($propertyName);
		}

		if(isset($this->belongsTo[$propertyName])){
			return $this->belongsTo($propertyName);
		}
		return false;
	}

	public function hasMany($propertyName, $limit=null){
		$records = $this->getEntity('hasMany',$propertyName, $limit);
		if(!$records) return $records;
		return $this->{$propertyName} = $records->toEntityCollection();
	}

	public function belongsTo($propertyName){
		$records = $this->getEntity('belongsTo',$propertyName, 1);
		if(!$records) return $records;
		return $this->{$propertyName} = $records->getFirstModel();
	}

	public function hasOne($propertyName){
		$records = $this->getEntity('hasOne',$propertyName, 1);
		if(!$records) return $records;
		if($records->hasSelectedDefined()){
			return $this->{$propertyName} = $records->getFirstStandard();
		}
		return $this->{$propertyName} = $records->getFirstModel();
	}

	/**
	 * @return ActiveRecord|boolean
	 */
	private function getEntity($type,$propertyName,$limit=false){
		if(!isset($this->{$type}[$propertyName])){
			return false;
		}
		if(isset($this->{$propertyName})) return $this->{$propertyName};
		$configuration = $this->{$type}[$propertyName];
		$className = $configuration['model'];
		$model = new $className();
		$records = $model->records();

		if(isset($configuration['select'])){
			$records->select($configuration['select']);
		}

		$configuration['join'] = isset($configuration['join']) ? $configuration['join']: [];
		foreach($configuration['join'] as $rule){

			foreach($rule['condition'] as $field => $value){
				if(!strpos($value,".") && isset($this->{$value})){
					$rule['condition'][$field] = $this->{$value};
				}
			}
			$table = isset($rule['table']) ? $rule['table'] : '';
			if(isset($rule['model'])){
				$modelClass = $rule['model'];
				$table = (new $modelClass)->table_name;
			}
			$records->join($table, $rule['condition'], $rule['type']);
		}

		$configuration['where'] = isset($configuration['where']) ? $configuration['where']: [];
		foreach($configuration['where'] as $table=>$rule){
			$string = isset($rule[3]) ? $rule[3] : true ;
			$statement = isset($rule[4]) ? $rule[4] : 'AND';
			$value = $rule[2];

			if($string && isset($this->{$rule[2]})) {
				$value = $this->{$rule[2]};
			}

			$field = strpos($rule[0],".") ? $rule[0] : $model->table_name.'.'.$rule[0];

			$records->where($field,
							$value,
							$rule[1],
							$string,
							$statement);

		}
		if(isset($configuration['order'])) $records->order((array)$configuration['order']);
		if($limit) $records->limit($limit);
		return $records;
	}
	public function __call($methodName,$arguments){
		if(!property_exists($this,strtolower($methodName)) || count($arguments)==0) return false;
		$this->{$methodName} = $arguments[0];
		return $this;
	}

	public static function __callStatic($method, $args){
		$class = get_called_class();

		return call_user_func_array([new $class(),$method],$args);
	}

	public function jsonSerialize(){
		return $this->extract();
	}
}
