<?php

/**
 * Designs short summary.
 *
 * Designs description.
 *
 * @version 1.0
 * @author cfagundes
 */
class Design extends TypedClass
{
	
	public $title;
	public $meta;
	private $js = array();
	private $css = array();
	private $linebreak= "\r\n";
    private $design;


	private $controller;
	/**
	 * Summary of $configuration
	 * @var DesignConfiguration
	 * @property-read 
	 */
	private $configuration;
	/**
	 * Summary of $design
	 * @var String
	 * @property-read
	 * @property-write
	 */
	public function __construct(Controller $controller){
		$this->controller = $controller;
		$this->setDesign(isset($this->controller->page_design) ? $this->controller->page_design : config::DefaultDesign);
		$this->setLayout(isset($this->controller->layout) ? $this->controller->layout: config::DefaultLayout);
	}
	
	private function setDesign($design){
		$design = trim($design,"/");
		$design_path = config::$BasePath."app/designs/";
		$extension = '.php';
		if(!is_dir($design_path.$design)){
			throw new Exception("Design <b>{$design}</b> not found on designs path</b>");	
		}
		if(!is_file($design_path.$design."/".$design.$extension)){
			$extension = '.html';
			if(!is_file($design_path.$design."/".$design.$extension)){
				throw new Exception("File of configuration class <b>{$design}.php</b> was not found on design {$design} path");
			}
		}
		require_once($design_path.$design."/".$design.$extension);
		if(!class_exists($design)){
			throw new Exception("Class <b>{$design}</b> was not found on file <b>{$design}.{$extension}</b>");
		}
		$this->configuration = new $design();
		$this->design = $design;
		return true;
	}
	
	public function addCss($relative_path,array $options=array(),$asset=false){
		$relative_path = str_replace(".css","",$relative_path).".css";
		$type = $asset==false ? "css"  :"assets";
		$path = ($relative_path[0]=='/') ? "" : config::$BaseDomain."app/designs/{$this->design}/{$type}/";
		$css = $this->linebreak."<link rel='stylesheet' href='{$path}{$relative_path}' type='text/css' ";
		foreach($options as $key=>$value){
			$css .= " {$key}='{$value}'' ";
		}
		$css .= " />";
		$this->css[] = $css;	
	}	
	public function addJs($relative_path,array $options=array(),$asset=false){
		$relative_path = str_replace(".js","",$relative_path).".js";
		$type = $asset==false ? "js"  :"assets";
		$path = ($relative_path[0]=='/') ? "" : config::$BaseDomain."app/designs/{$this->design}/{$type}/";
		$js = "<script src='{$path}{$relative_path}' type='text/javascript' ";
		foreach($options as $key=>$value){
			$js.= " {$key}='{$value}' ";
		}
		$js .= " ></script>".$this->linebreak;
		$this->js[] = $js;
	}
	
	public function addCssArray(array $array,$assets=false){
		foreach($array as $k=>$css){
			if(is_array($css)) {
				$this->addCss($k,$css,$assets);
				continue;
			}
			$this->addCss($css,array(),$assets);
		}
	}
	
	public function addJsArray(array $array,$assets=false){
		foreach($array as $k=>$js){
			if(is_array($js)) {
				$this->addJs($k,$js,$assets);
				continue;
			}
			$this->addJs($js,array(),$assets);
		}
	}
	public function css(){
		$this->controller->default_css = is_array($this->controller->default_css) ? $this->controller->default_css : array();
		$this->addCssArray(config::$DefaultCSS);
		$this->addCssArray(config::$DefaultCSSAssets,true);
		$this->addCssArray($this->configuration->default_css);
		$this->addCssArray($this->configuration->default_css_assets,true);
		$this->addCssArray($this->controller->default_css);
		return implode("",$this->css);
	}
	public function js(){
		$this->controller->default_js = is_array($this->controller->default_js) ? $this->controller->default_js : array();
		$this->addJsArray(config::$DefaultJS);
		$this->addJsArray(config::$DefaultJSAssets,true);
		$this->addJsArray($this->configuration->default_js);
		$this->addJsArray($this->configuration->default_js_assets,true);
		$this->addJsArray($this->controller->default_js);
		return implode("",$this->js);
	}
    /**
     * Summary of $layout
     * @var String
	 * @property-read
	 * @property-write
     */
    private $layout;
    private $layoutPath;
	private function setLayout($layout){
		$extension = '.php';
		if(!is_file(config::$BasePath."/app/designs/".$this->design."/layouts/".$layout.$extension)){			
			$extension = '.html';
			if(!is_file(config::$BasePath."/app/designs/".$this->design."/layouts/".$layout.$extension)){			
				throw new Exception("Layout <b>{$layout}</b> not found on design path = <b>{$this->design}</b>");
			}
		}
		$this->layout = $layout;
		$this->layoutPath = config::$BasePath."/app/designs/".$this->design."/layouts/".$layout.$extension;
		return true;
	}
	
	/**
	 * Summary of $layout
	 * @var Collection(String)
	 * @property-read
	 * @property-write
	 */
    private $views;
	public function setViews($views){
		$views = is_array($views) ? $views : [$views];
		foreach($views as $view){
			if(is_file($view)){
				$this->views[] = $view;
				continue;
			}
			if(!is_file(config::$BasePath."/app/views/".$view.".php")){
				throw new Exception("View <b>{$view}</b> not found on views path");
			}
			$this->views[] = config::$BasePath."/app/views/".$view.".php";
		}		
	}
	/**
	 * Summary of $viewContent
	 * @var String
	 */
	private $viewContent;
	public function render()
	{
		include($this->layoutPath);
	}
	public function showViews(){
		if(!isset($this->controller->data)){
			$this->controller->data = new stdClass();
		}
		foreach((array)$this->controller->data as $k=>$data){
			$$k = $data;
		}
		foreach($this->views as $view){			
			require_once($view);
		}
		
	}
	public function withoutLineBreak(){
		$this->linebreak = "";
	}
	public function withLineBreak(){
		$this->linebreak = "\r\n";
	}
	
}