<?php

/**
 * Response short summary.
 *
 * Response description.
 *
 * https://www.w3.org/Protocols/HTTP/HTRESP.html
 */
abstract class Response
{
	public function __construct(){
	}

	public function __destruct(){
	}

	const Mode_100 = 100; //Continue
	const Mode_101 = 101; //Switching Protocols
	const Mode_200 = 200; //OK
	const Mode_201 = 201; //Created
	const Mode_202 = 202; //Accepted
	const Mode_203 = 203; //Non-Authoritative Information
	const Mode_204 = 204; //No Content
	const Mode_205 = 205; //Reset Content
	const Mode_206 = 206; //Partial Content
	const Mode_300 = 300; //Multiple Choices
	const Mode_301 = 301; //Moved Permanently
	const Mode_302 = 302; //Found
	const Mode_303 = 303; //See Other
	const Mode_304 = 304; //Not Modified
	const Mode_305 = 305; //Use Proxy
	const Mode_306 = 306; //(Unused)
	const Mode_307 = 307; //Temporary Redirect
	const Mode_400 = 400; //Bad Request
	const Mode_401 = 401; //Unauthorized
	const Mode_402 = 402; //Payment Required
	const Mode_403 = 403; //Forbidden
	const Mode_404 = 404; //Not Found
	const Mode_405 = 405; //Method Not Allowed
	const Mode_406 = 406; //Not Acceptable
	const Mode_407 = 407; //Proxy Authentication Required
	const Mode_408 = 408; //Request Timeout
	const Mode_409 = 409; //Conflict
	const Mode_410 = 410; //Gone
	const Mode_411 = 411; //Length Required
	const Mode_412 = 412; //Precondition Failed
	const Mode_413 = 413; //Request Entity Too Large
	const Mode_414 = 414; //Request-URI Too Long
	const Mode_415 = 415; //Unsupported Media Type
	const Mode_416 = 416; //Requested Range Not Satisfiable
	const Mode_417 = 417; //Expectation Failed
	const Mode_500 = 500; //Internal Server Error
	const Mode_501 = 501; //Not Implemented
	const Mode_502 = 502; //Bad Gateway
	const Mode_503 = 503; //Service Unavailable
	const Mode_504 = 504; //Gateway Timeout
	const Mode_505 = 505; //HTTP Version Not Supported
	/**
	 * Creates a response object for any controller's action return.
	 * @param int $mode This is the response mode (status code) to be returned as a response
	 * @param String $message This property contains the message to be displayed at the return
	 * @return array With the response object
	 */
	public static function mode($mode=Response::Mode_200, $message='All processing was OK',$clean_flush_exit=false,$custom_message='') {
		//Creates the response object
		$response = ['success' => false, 'message' => $message];

		//Sets the response header
		header("HTTP/1.1 {$mode} {$message}");
		if($clean_flush_exit){
			ob_start();
			ob_end_flush();
			ob_clean();
			echo $custom_message;
			exit;
		}
		return $response;
	}

	/**
	 * Creates a response object for when any controller's ERROR state should be returned.
	 * @param String $message The ERROR message to be displayed at the return
	 * @return array With the response object
	 */
	public static function error($clean_flush_exit=false,$custom_message='') {
		return Response::mode(Response::Mode_404, "Not found ",$clean_flush_exit,$custom_message);
	}

	/**
	 * Creates a response object for when any controller's OK state should be returned.
	 * @param String $message The SUCCESS message to be displayed at the return
	 * @return array With the response object
	 */
	public static function ok($clean_flush_exit=false,$custom_message='') {
		return Response::mode(Response::Mode_200, "Ok ",$clean_flush_exit,$custom_message);
	}

	/**
	 * Unauthorized should be thrown when the user is not authenticated and the route/file does require 
	 * authentication to be executed such as controller actions 
	 * that doesn't have @authorized mark on it's comments
	 */
	public static function unauthorized($clean_flush_exit=false,$custom_message=''){
		return Response::mode(Response::Mode_401, "Unauthorized ",$clean_flush_exit,$custom_message);
	}
	/**
	 * Forbidden should be thrown when the user is Authenticated but does not
	 * have the access to some resource on the server such as a directory listing, a file,
	 * or also if the user can not access an specific route
	 */
	public static function forbidden($clean_flush_exit=false,$custom_message=''){
		return Response::mode(Response::Mode_403, "Forbidden ",$clean_flush_exit,$custom_message);
	}
}
