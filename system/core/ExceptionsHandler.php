<?php

/**
 * ExceptionsHandler short summary.
 *
 * ExceptionsHandler description.
 *
 * @version 1.0
 * @author bruno.teixeira.silva
 */
class ExceptionsHandler
{
    
    public static function init(){
		set_exception_handler("ExceptionsHandler::handle");
	}
	public static function handle($e){
		try{
            Router::getInstance()->setErrorRouting($e);
        }
        catch(Exception $e){
			Router::getInstance()->castExceptionMessage($e);
        }
	}
}
